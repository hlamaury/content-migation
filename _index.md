+++ 
title = "Accueil FAQ ADULLACT"
description = "Page d'accueil de la FAQ"
date = "2014-11-06"
+++ 

# Bienvenue sur la FAQ

L'ADULLACT vous informe et répond à vos questions. Nous vous souhaitons une bonne lecture...

## Découvrir la FAQ

Depuis 2005, l'association a mis en place une FAQ pour vous aider à trouver des solutions à vos problèmes et questionnements informatiques, dans le domaine du logiciel libre.  

On le sait, le libre soulève de nombreuses questions. C'est pourquoi nous avons conçu ce blog comme un espace d'échange, de conseil, de pédagogie aussi bien vers le grand public que vers les usagers expérimentés.

À quoi sert l'[horodatage]() ? Comment faire une demande de [certificat électronique]() ? Comment libérer mon logiciel ? Comment fonctionnent les [licences libres]() ? Autant de questions auxquelles nous avons essayé de répondre, en espérant que vous prendrez le temps de nous lire...

---
#### Consulter les [mentions légales]({{< relref "./61-mentions-legales.md" >}})