+++
title = "Mentions légales"
description = "Mentions légales"
tags = [  ]
date = "2014-12-16"
+++
#### Informations légales

[faq.adullact.org](http://faq.adullact.org) est le blog officiel de l'association ADULLACT.

**Adresse** : 836, rue du mas de Verchant - Bât. Le Tucano - 34000 MONTPELLIER

**Éditeur** : ADULLACT

**Directeur de publication** : Pascal Feydel, Délégué Général de l'ADULLACT

**Hébergement** : ADULLACT

**Conception, réalisation, intégration et accessibilité** : ADULLACT

#### Informations générales

Nous mettons tout en oeuvre pour offrir à nos visiteurs des informations complètes et fiables ; cependant, et malgré tous les soins apportés à la vérification du contenu, le présent site peut comporter des erreurs ou inexactitudes. Si vous souhaitez signaler une éventuelle erreur, omission ou encore un problème d'affichage gênant votre utilisation du site, vous pouvez nous écrire à l'adresse suivante : [webmestre@adullact.org](mailto:webmestre@adullact.org). 

#### Droits de reproduction

Conformément au droit public de la propriété intellectuelle et selon l'article L122-5 du Code de la propriété intellectuelle, les document des communication de l'association (logo, dossier de presse, compte-rendu, déclaration publique...) sont librement réutilisables, à condition que la reprise de ces contenus mentionne de façon claire la source, ou propose un lien vers le document original en ligne sur le présent site.

Les reproductions, sur un support papier ou informatique du présent site sont autorisées sous réserve qu'elles soient strictement réservées à un usage personnel et familial, excluant tout usage à des fins publicitaires ou commerciales.

#### Liens hypertextes

Le présent site propose de nombreux liens vers de sites externes ; nous ne pouvons être tenus pour responsables du contenu des sites vers lesquels pointent ces liens. 

#### Clause de non responsabilité

L'utilisateur est informé que les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent faire l’objet de mises à jour.

La responsabilité d'ADULLACT ne peut, en aucune manière, être engagée quant au contenu des informations figurant sur ce site ou aux conséquences pouvant résulter de leur utilisation ou interprétation.

#### Utilisation des cookies

Lors de votre visite sur le site [faq.adullact.org](http://faq.adullact.org/), des cookies\* sont déposés sur votre ordinateur, votre mobile ou votre tablette. Les seuls cookies utilisés sont ceux destinés à la mesure d'audience du présent site. Ces données sont anonymes et nous permettent de mieux comprendre les habitudes et réactions de nos visiteurs.

Les données générées par les cookies sont transmises et stockées par Google Analytics. Les données collectées sont hébergées sur des serveurs situés aux Etats-Unis. Google utilisera cette information dans le but d'évaluer votre utilisation du site mais ne recoupera pas votre adresse IP avec toute autre donnée détenue par Google. Les prestataires de mesure d’audience sont susceptibles de communiquer ces données à des tiers en cas d'obligation légale ou lorsque ces tiers traitent ces données pour leur compte.

Vous pouvez désactiver l’utilisation de cookies en sélectionnant les paramètres appropriés de votre navigateur.

_\* Un cookie est un fichier texte déposé sur votre ordinateur lors de la visite d'un site ou de la consultation d'une publicité. Il a pour but de collecter des informations relatives à votre navigation et de vous adresser des services adaptés à votre terminal (ordinateur, mobile ou tablette). Les cookies sont gérés par votre navigateur internet._