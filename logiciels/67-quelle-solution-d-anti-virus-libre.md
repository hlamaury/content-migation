+++
title = "Quelle solution d\"anti-virus libre ?"
description = "<p>\"L\"antivirus est le degré zéro de la sécurité informatique...\"</p><p>Les virus informatiques (comme les virus biologiques) se renouvellent, changent régulièrement d\"aspect, et se propagent par différents moyens (réseaux, internet, DVD, clef USB, etc.). Il est donc essentiel d\"avoir un antivirus installé, et surtout, mis à jour.</p>"
tags = [  ]
type = "blog"
date = "2006-02-13"
author =  "beatrice"
menu = "logiciels"
+++
<p>"L'antivirus est le degré zéro de la sécurité informatique..."</p>
<p>Les virus informatiques (comme les virus biologiques) se renouvellent, changent régulièrement d'aspect, et se propagent par différents moyens (réseaux, internet, DVD, clef USB, etc.). Il est donc essentiel d'avoir un antivirus installé, et surtout, mis à jour.</p>


<p>L'antivirus est, en règle générale, constitué d'un programme toujours en mémoire de l'ordinateur, et vérifiant sans cesse la présence de "signatures virales" ou de séquences précises d'octets qui trahissent la présence d'un virus connu.</p>
<p> </p>
<h3>ClamAV</h3>
<p><a href="http://www.clamav.net/index.html">ClamAV</a> est disponible pour les systèmes Linux et MSWindows (ClamWIN), léger et performant, l’antivirus ClamAV est une solution libre antivirale (GPL). Vous trouverez toutes les informations sur ClamAV sur le site officiel. <a href="http://www.clamav.net">http://www.clamav.net </a></p>
<p>L'installation de clamAV se fait trés simplement sur Linux car il est prévu dans la plupart des ditributions.</p>
<ul>
<li><span style="text-decoration: underline;">Pour Mandriva</span> : <code># urpmi clamav </code></li>
<li><span style="text-decoration: underline;">Pour Fedora core</span> : <code># yum install clamav </code></li>
<li><span style="text-decoration: underline;">Pour Debian</span> :<code> # apt-get install clamav </code></li>
</ul>
<p>La commande clamscan permet de scanner les dossiers de la machine, la commande freshclam permet de réaliser les mises à jour , ces deux commandes peuvent facilement être automatisées avec cron. Nous alllons détailler le fonctionnement de ces deux commandes à travers quelque exemples.</p>
<ul>
<li>Scanner le disque dur entier = <code>clamscan -r /</code></li>
<li>Scanner tous les fichiers du dossier personnel = <code>clamscan -r /home/utilisateur</code></li>
<li>Scanner le disque dur entier et émettre un son une fois un fichier infecté trouvé = <code>clamscan -r –bell –mbox -i /  </code></li>
<li>Affiche uniquement les fichiers infectés = <code>–infected (-i) </code></li>
<li>Emet un son lors de la détection d’un virus = <code>–bell </code></li>
<li>Scanne les sous-dossiers récursivement = <code>–recursive (-r)</code></li>
</ul>
<p>La mise à jour des définitions anti-virus se fait à l'aide d'une autre commande : <code>freshclam</code> ; également disponible sous forme de démon: <code>freshclamd</code></p>
<p> </p>
<p><strong>En savoir plus :</strong></p>
<p>Voir l'<a href="http://wiki.debian-facile.org/doc:systeme:clamav">article wiki</a> sur le site Debian-Facile</p>
<p>Voir le <a href="http://linuxfr.org/news/tests-defficacit%C3%A9-des-antivirus-linux">test d'efficacité des anti-virus Linux</a> sur le site LinuxFR</p>