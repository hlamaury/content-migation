+++
title = "Accès à GitLab et aux fichiers"
description = "GITLAB donner accès aux fichiers binaires seulement sur inscription"
tags = [ "logiciels", "gitlab" ]
date = "2017-11-17"
+++

Besoin exprimé
--------------

Pour un projet libre donné, on souhaite savoir qui télécharge les binaires (i.e. pas le code source mais bien le résultat de compilation)

Solution - réponse courte
-------------------------

L'idée est de créer un second projet dédié aux binaires, et d'en restreindre l'accès.

Solution - mise en œuvre détaillée
----------------------------------

Soient:

*   `MonProjet` le nom du projet initial. Son code source est disponible, mais nous souhaitons savoir qui télécharge les binaires ;
*   `MonProjet-Telechargement` le nom du projet dédié au téléchargement des binaires de `MonProjet`.

Les étapes suivantes s'adressent au **propriétaire du projet**.

### Création et configuration du nouveau projet

1.  Créer un projet `MonProjet-Telechargement` à côté du projet initial `MonProjet`
2.  Une fois le projet créé, se rendre dans `Settings` > `General` puis déplier `Permissions`.
3.  Pour `Project Visibility`, choisir `Internal`
4.  Cocher la case `Allow users to request access`.
5.  Activer `Repository`, et choisir `Only Project  Members`.
6.  Activer `Git Large File Storage`
7.  Désactiver tous les autres choix.

Copie d'écran des permissions :  
![](/images/screenshot-2017-11-17_GITLAB_permissions_pour_savoir_qui_telecharge_kraken.png)

### Ajout des fichiers binaires - par l'interface web de Gitlab

En tant que propriétaire de `MonProjet-Telechargement`, se rendre sur le projet et [téléverser le fichier binaire](https://docs.gitlab.com/ce/user/project/repository/web_editor.html#upload-a-file).

### Ajout des fichiers binaires - depuis le poste du développeur

Avant d'ajouter un fichier :

1.  Il est **important** [d'installer GIT-LFS](https://git-lfs.github.com/) sur le poste client **avant** de poser le fichier dans le dépôt.
2.  Faire un `git clone` du projet
3.  Faire un `git lfs install`
4.  Considérant que les fichiers binaires sont au format .zip, faire un `git lfs track "*.zip"` (ajuster avec l'extension idoine si besoin)
5.  Faire un `git add gitattributes`

Lors de l'ajout du fichier :

git add fichier.zip  
git commit -m "Ajout fichier binaire"  
git push origin master

(Voir aussi la [Documentation Git-LFS](https://docs.gitlab.com/ce/workflow/lfs/manage_large_binaries_with_git_lfs.html))

Démarche à suivre pour l'utilisateur
------------------------------------

1.  L'utilisateur se rend sur le projet `MonProjet-Telechargement`
2.  Il demande à rejoindre le projet en [cliquant sur le bouton Request Access](https://gitlab.com/help/user/project/members/index.md#request-access-to-a-project) (sur la droite de l'écran, à la hauteur du logo du projet)
3.  Le propriétaire du projet reçoit un courriel pour valider (ou non) l'accès à l'utilisateur
4.  Une fois l'accès validé, l'utilisateur est notifié par courriel de son acceptation et peut accéder aux fichiers à télécharger

Contact
-------

Pour toute question, n'hésitez pas à nous écrire sur [support@adullact.org](mailto:support@adullact.org)