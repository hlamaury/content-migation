+++
title = "Quelle photothèque choisir ?"
description = "Quelle phototèque choisir"
tags = [ "phototèque" ]
date = "2005-10-04"
+++
On voir déjà fleurir de nombreux logiciels libres de gestions de photos en ligne. Lequel choisir ?

*   [**Gallery**](http://galleryproject.org/) : le logiciel de mise en ligne de photos le plus répandu dans le libre. Peut-être aussi (gallery version 2 uniquement) le plus respectueux des standards du WEB. Les fonctionnalités disponibles:
    *   imageMagick
    *   rotation
    *   taille et qualité réglables
    *   multi-langages
    *   possibilité Email
    *   diaporama
    *   commentaires par photo
    *   login & gestion des utilisateurs
    *   fil RSS
    *   multi albums
    *   customisation & thèmes
    *   CSS & relookage aisé
    *   Respect des standards du W3C

*   [**Coppermine**](http://coppermine-gallery.net/). A noter une [documentation en ligne](http://documentation.coppermine-gallery.net/en/) assez compète. À noter que "le pays de Brest" a choisi coppermine pour animer sa [Photothèque Collaborative](http://www.1zef2images.org/spip/index.php) du Pays de Brest. Liste des fonctionnalités:
    *   arrangement des images en catégories et albums
    *   utilisation d'une base de données
    *   upload des images via l'interface web ou via FTP
    *   support multimédia
    *   mode vignette
    *   outil de recherche
    *   lecture aléatoire
    *   login et gestion d'utilisateurs
    *   champs redéfinissables
    *   multi-langage
    *   commentaires par photo
    *   carte-postale
    *   diaporama
    *   customisation par squelettes
    *   rotation et redimensionnement des images
    *   réglage du poids maxi des photos

*   [**Phraseanet**](https://www.phraseanet.com/) est une solution web de gestion de photothèques/médiathèques destinée aux professionnels. Récemment, son éditeur a annoncé le passage du logiciel en Open Source : la solution est dorénavant gratuite (la licence d’utilisation du logiciel était auparavant de... 7500€). Alchemy existe depuis 1996 et la solution Phraseanet depuis 2005 (version 1). Aujourd’hui, Phraseanet est en version 3.0.2 (la V3 est sortie en mars 2010). Vous pouvez également trouver Phraseanet sur le site de sourceforge. Les sources de PHRASEANET sont disponibles sur [sourceforge](http://sourceforge.net/projects/phraseanet/).

On notera, dans tous les cas, l'importance de la bibliothèque de manipulation d'images ImageMagick, si l'installation est faite sous Linux ; ou encore, plus simple dans le cas des solutions PHP et quelque soit le système: la librairie GD. Ce sont ces utilitaires qui permettront la manipulation dynamique des images (retaillage, rotation...)