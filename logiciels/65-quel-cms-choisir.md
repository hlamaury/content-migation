+++
title = "Quel CMS choisir ?"
description = "<p style=\"text-align: justify;\">Il existe actuellement des centaines de CMS disponibles sur le marché (libre ou non), quelle solution choisir ?</p><p style=\"text-align: justify;\">Quelques exemples, et une méthode très simplifiée pour vous aider à choisir votre solution.</p>"
tags = [ "CMS", "Joomla", "Wordpress", "Spip", "Typo3" ]
type = "blog"
date = "2005-10-04"
author =  "pkuczynski"
menu = "logiciels"
+++
<p style="text-align: justify;">Il existe actuellement des centaines de CMS disponibles sur le marché (libre ou non), quelle solution choisir ?</p>
<p style="text-align: justify;">Quelques exemples, et une méthode très simplifiée pour vous aider à choisir votre solution.</p>


<p style="text-align: justify;"> </p>
<h3 style="text-align: justify;">1. Vous avez besoin d'un outil simple, offrant une prise en main rapide pour créer un site web orienté blog ou magazine :</h3>
<ul>
<li style="text-align: justify;"><a href="https://wordpress.org/">Wordpress</a>, sans doute le plus connu des CMS grand public. Lancé en 2003, Wordpress a rapidement acquis une belle notoriété ; simple d'accès, doté de nombreuses fonctionnalités et d'une interface claire, Wordpress bénéficie en outre d'une grande communauté active lui permettant de bénéficier de nombreux plugins. WordPress inclut la gestion des liens externes, des rétroliens (trackbacks), et un système de gestion fine des commentaires. À noter que Wordpress dispose d'un service de publication en ligne (gratuit), <a href="https://fr.wordpress.com/">wordpress.com</a>, qui limite certaines fonctionnalités mais permet de publier et de référencer rapidement son blog.</li>
<li style="text-align: justify;"><a href="http://www.spip.net/fr">Spip</a>, une valeur sûre</li>
<li style="text-align: justify;">Son dérivé, <a href="http://www.agora2spip.agora.gouv.fr/">Spip Agora</a>, qui offre des fonctionnalités supplémentaires comme une gestion plus fine des droits, un workflow sur les articles, archivage des articles, une gestion plus riche des mots-clés, une "galerie" de documents, des forums modifiés, moteur de recherche qui ira chercher dans les fichiers pdf...</li>
</ul>
<p style="text-align: justify;"> </p>
<h3 style="text-align: justify;">2. Vous voulez mettre en place une véritable infrastructure de développement, orientée gestion de contenu :</h3>
<ul>
<li style="text-align: justify;"><a href="http://www.mamboserver.com/">Mambo</a>, ou <a href="http://www.joomla.org/">Joomla</a>, tous deux créés par la même équipe. Mambo a été developpé par la société MIRO. Mais depuis l'annonce d'un projet de création d'une fondation supervisant le projet Mambo, et de peur que ce projet ne perde son aspect libre et opensource, de nombreux développeurs ont décidé de prendre du recul et de créer un fork de Mambo nommé Joomla! </li>
<li style="text-align: justify;"><a href="http://www.frxoops.org/">Xoops</a> est un CMS orienté objet dérivé de PHPNuke et écrit en PHP.</li>
<li style="text-align: justify;"><a href="http://typo3.org/">Typo3</a>, technologie PHP/MySQL. Typo3 (V.3.6) est multisites et présente une très forte modularité lui permettant de couvrir tous vos besoins ou presque. Il offre en outre la possibilité à chacun de le personnaliser à son goût... via les "extensions". Typo3 possède plus de 1200 extensions. </li>
<li style="text-align: justify;"><a href="http://share.ez.no">eZ publish</a>, technologie PHP/MySQL. Avec eZ Publish on peut réaliser un site de manière personnalisée : on peut tout faire ! Il y a par exemple des outils pour la création de menus (menu simple en haut ou à gauche, menu double en haut, ou à gauche et en haut...) et de toolbars. Un des principaux avantages de ezPublish est sa capacité à se programmer sur un mode objet. Les avantages :
<ul>
<li style="text-align: justify;">génère du code HTML standard W3C</li>
<li style="text-align: justify;">dispose d'une véritable communauté</li>
<li style="text-align: justify;">capacité d'import/export des contenus du site</li>
<li style="text-align: justify;">workflow simple et éditable ; possibilité de notification par mail ; archivage automatique d'anciens articles</li>
<li style="text-align: justify;">backoffice entièrement editable selon un modèle objet. Exemple: créer un nouveau type d'article, héritant de l'article standard mais où on aura spécifié que le titre est obligatoire; le chapo optionnel et le corps de l'article n'est pas multilangue. Il y a plusieurs dizaines de classes d'objets par défaut, toutes éditables.</li>
<li style="text-align: justify;">utilisation de gabarit pour mieux séparer le contenu de la forme</li>
<li style="text-align: justify;">facilité d'hébergement (PHP)</li>
</ul>
</li>
</ul>
<p style="padding-left: 30px;">Un exemple labellisé accessiweb réalisé avec ezPublish: le <a href="http://planetarium-galilee.montpellier-agglo.com/">planétarium de Montpellier.</a></p>
<p style="text-align: justify;"> </p>
<h3 style="text-align: justify;">3. Vous voulez mettre en place un portail orienté travail collaboratif :</h3>
<ul>
<li style="text-align: justify;"><a href="https://plone.org/">Plone</a>, technologie Python. Plone permet de construire facilement des portails collaboratifs, efficaces et très propres. Il est écrit en Python ce qui permet aux développeurs d'étendre facilement ses fonctionnalités ou d'en modifier le code. Pour en savoir plus...</li>
<li style="text-align: justify;"><a href="http://www.nuxeo.com/products/content-management-platform/">Nuxeo CPS</a> est un progiciel libre d'ECM (Enterprise Content Management). CPS Platform, qui fonctionne sur le serveur d'applications Zope, est actuellement, selon plusieurs études publiées par le Gartner Group, KnowledgeConsult ou Markess International, le progiciel libre le plus complet de son domaine, toutes technologies confondues. </li>
<li style="text-align: justify;"><a href="http://www.ovidentia.org/">Ovidentia</a> reprend les fonctions classiques d'un Gestionnaire de Contenu (CMS) telles que la publication d'informations, le workflow de validation, un moteur de recherche ... OVIDENTIA dispose également de puissantes fonctions collaboratives : gestionnaire de profils, agendas partagés, interface de messagerie, annuaires, partage de fichiers, forums, FAQs, workflow applicatif, gestion de demandes de congés, organigrammes ...</li>
</ul>
<p style="text-align: justify;"> </p>
<p style="text-align: justify;"><strong>Quelques sites pour vous permettre d'approfondir le sujet :</strong></p>
<p style="text-align: justify;">L'article <a href="http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu">Systèmes de Gestion de Contenu</a> sur Wikipedia.</p>
<p style="text-align: justify;">La rubrique <a href="http://www.framasoft.net/rubrique168.html">CMS</a> de Framasoft.</p>
<p style="text-align: justify;">Framasoft a également testé ces CMS pour vous :</p>
<ul>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article2166.html">Mambo</a></li>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article3088.html">eZ Publish</a></li>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article2198.html">Typo3</a></li>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article3083.html">Plone</a></li>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article2799.html">SPIP-AGORA</a></li>
<li style="text-align: justify;"><a href="http://www.framasoft.net/article1955.html">DotClear</a></li>
</ul>