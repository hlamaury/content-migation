+++
title = "MS-Word et Web-délib"
description = "Comment utiliser MS-Word avec web-delib"
tags = [ "Word", "web-delib", "MS-Word" ]
date = "2013-06-12"
+++

Web-delib impose l'utilisation exclusive du format ODT. MS-Word sait générer du ODT depuis sa version 2010... malheureusement certaines limitations nous interdisent d'exploiter complètement les fonctionnalités de Web-delib. Nous déconseillons l'utilisation de MS-Word avec Web-delib sans prendre contact avec nous au préalable afin d'étudier le contexte d'utilisation. MS-Word supporte assez bien le format ODT depuis sa version 2010, pour être utilisable dans Web-delib. Toutefois, nous souffrons encore des restrictions annoncées par Microsoft.

Certains cas de portabilités sont détaillés ci-après et certains contournements y sont explicités.

## Version ODT

OpenOffice et LibreOffice travaillent avec le format ODT v1.2

MS-Word 2010 travaille avec le format ODT v1.1

Sachant que les modèles continuent à être fabriqués par OOo/LO, il est donc nécessaire de considérer cette différence de versions. OOo/LO permet de jouer avec la version de ODT. Choisir la version 1.2 Etendu (mode compatibilité) ou 1.0/1.1

![](/images/sampledata/FAQ/Logiciels/MS-Word-webdelib1.png)

Remarque: avec MS-Word 2013, ce dernier supportant ODT1.2, le réglage énoncé ci-dessus devient obsolète. Il devient possible de travail en "full" ODT1.2.

Notre conseil : utiliser ODT1.2 étendu ou ODT1.1 pour toute version MS-Word inférieure à 2013

## Sauts de page intempestifs

### Pour la création d'un .odt avec MS-Word

Il arrive que la mécanique de fusion de Web-delib utilise la notion de "section". Or il apparait que la configuration par défaut de MS-Word provoque explicitement un saut de page à chaque nouvelle section! Il convient donc de modifier ce réglage par défaut sur tous les postes de travail des agents rédacteurs.

Pour ce faire, dans un nouveau document MS-Word, cliquer sur mise en page (1) puis sur le menu avancé (2), aller sur l'onglet disposition (3) et sélectionner "continu" (4), appliquer ce paramètre par défaut (5) et valider (6).

![](/images/sampledata/FAQ/Logiciels/MS-Word-webdelib2.png)

![](/images/sampledata/FAQ/Logiciels/MS-Word-webdelib3.png)

Valider ensuite la modification par défaut.

### Pour la modification d'un .odt avec MS-Word

Lors du passage d'un .odt (quelque soit la façon dont il a été créé : avec LibreOffice, OpenOffice ou MS-Word) dans MS-Word, la sauvegarde des paramètres de section ne s'effectue pas. Il faut donc à nouveau passer manuellement la disposition en "continu" (voir ci-dessus) dès l'ouverture d'un fichier .odt par MS-Word. Cette disposition est automatiquement reconnue dans LibreOffice, qui l'utilise par défaut.

Notre conseil : ne pas utiliser de section dans les documents insérés dans les champs texte de Web-delib (texte de projet, texte de synthèse, texte de l'acte) et créés avec MS-Word.

## Gestion des tableaux

Dû au mauvais encodage en ODT par MS-Word, les tableaux créés au format ODT sous MS-Word ne sont pas interprétés par GedOoo.

Notre conseil : déporter les tableaux dans des annexes (au format ODT ou PDF).

## Gestion des zones flottantes

Depuis MS Office 2010, les modèles préconçus par MS-Word fonctionnent sur une base de zones flottantes placées sur la page. L'encodage de ces zones par MS-Word n'est pas compatible avec le format ODT. Même si aucune de ces zones flottantes n'est supprimée, leur emplacement sur la page peut devenir très aléatoire lors de son passage dans Web-delib.

Notre conseil : ne pas utiliser les zones flottantes de MS-Word.

## Gestion des pages "paysage"

Les outils libres OpenOffice.org / LibreOffice gèrent l'orientation des pages à l'aide des styles de pages. En particulier le style Paysage fourni en standard avec ces outils est explicitement exploité par GedOOo pour gérer les pages en paysage (il suffit d'applique le style paysage à une zone texte pour la page en question s'affiche en paysage dans le document généré par GedOOo). Or MS-Word n'utilise pas le style paysage. De fait, il est impossible de générer un document intégrant des pages horizontales à partir de textes intégrés issus de MS-Word.

Remarque: cela évoluera avec une future évolution de GedOOo.

Notre conseil : ne pas utiliser le format paysage de MS-Word.

---
### Référence

Pour en savoir plus sur les situations ODT non traitées par MS-word, consulter la [page Microsoft](https://support.office.com/fr-be/article/Diff%C3%A9rences-entre-le-format-Texte-OpenDocument-odt-et-le-format-Word-docx-d9d51a92-56d1-4794-8b68-5efb57aebfdc) dédiée.