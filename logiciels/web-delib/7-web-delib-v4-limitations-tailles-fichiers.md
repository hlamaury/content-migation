+++
title = "Limitation de la taille des fichiers générés sur web-délib"
description = "Comment limiter la taille des fichiers générés sur Web-Délib"
tags = [ "web-delib" ]
date = "2013-08-13"
+++

Web-delib 4.x exploite les outils GedOOo et CloudOOo pour générer ses documents. Ces outils affichent certaines limites décrites ici.

## GedOOo

L'outil GedOOo permet de fusionner les informations et textes fournis via les formulaires Web-Delib avec les modèles déclarés dans Web-Delib. La mécanique GedOOo permet de fabriquer des documents de plusieurs milliers de pages, en quelques minutes, avec un serveur disposant d'1 CPU et 2Go RAM.

## CloudOOo

L'outil CloudOOo récupère le fichier au format ODT fabriqué par CloudOOo, et via un simple passage dans OpenOffice serveur, permet de finaliser ce fichier en numérotant les pages et en fabriquant la table des matières. C'est également avec CloudOOo que Web-Delib transforme le fichier ODT ainsi obtenu en fichier PDF que Web-Delib affichera à l'utilisateur.

On notera le cas particulier des annexes PDF fournies par l'utilisateur, qui sont transformées en images (1 image/page) pour être intégrées dans un format ODT. Cela permet d'intégrer sereinement les fichiers PDF au sein des modèles ODT de Web-Delib (voir section Annexes). Ces annexes PDF peuvent faire plusieurs centaines de pages et trop d'annexes PDF peuvent surcharger la machine CloudOOo. CloudOOo est en mesure de traiter des fichiers 100% texte de 3000 pages, ou des fichiers 100% images de 1500 pages (400Mo), en 10mn de traitement, sur un serveur d'un seul VCPU, avec 1Go RAM. CloudOOo traite classiquement des fichiers mixtes (texte + images) de 1000 pages en moins de 5mn. 

## Limitations connues

CloudOOo n'exploite qu'un seul VCPU (limitation OpenOffice.org)

CloudOOo ne gère pas plus de 1Go RAM (mais ce n'est jamais le maillon faible)

CloudOOo est réglé en standard avec un time out de 10mn de temps de traitement (cf valeurs limites ci-dessus)