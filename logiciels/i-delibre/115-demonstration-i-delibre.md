+++
title = "Démonstration i-delibRE"
description = "Démonstration d'i-délibRe"
tags = [ "Démo", "i-délibRe" ]
date = "2020-08-26"
+++

Informations utiles pour la démonstration i-delibRE
---------------------------------------------------

### Vue par le gestionnaire

Vue réservée aux agents de la collectivité, en charge de la gestion des assemblées et de la convocation des élus.

*   L'adresse à utiliser est [https://idelibre.demonstrations.adullact.org/](https://idelibre.demonstrations.adullact.org/)
*   Pour se connecter - identifiant : `secretaire@ville.libre` , mot de passe : `idelibre`

### Vue par l'administrateur

Vue réservée aux administrateurs de la collectivité, en charge de la gestion de idelibre

*   L'adresse à utiliser est [https://idelibre.demonstrations.adullact.org/](https://idelibre.demonstrations.adullact.org/)
*   Pour se connecter - identifiant : `admin@ville.libre` , mot de passe : `idelibre`

### Vue par les élus

Vue réservée aux élus via leur tablette, quelle que soit la tablette, mais aussi en mode WEB. Un élu peut aussi bien travailler de sa tablette (mode connecté ou non) que de son ordinateur avec un simple navigateur.

*   L'adresse à utiliser est [https://idelibre.demonstrations.adullact.org/idelibre\_client/](https://idelibre.demonstrations.adullact.org/idelibre_client/L'adresse)
*   Pour se connecter sur le compte d'un élu avec deux mandats en cours - Identifiant : `d.guillaume`, mot de passe : `idelibre`
*   Pour se connecter sur le compte d'un élu disposant d'un seul mandant - Identifiant : `r.dubourget` , mot de passe : `idelibre`

Mais la 1ere fois il faut remplir le formulaire d'authentification:

*   Mandat: **Ville Libre**
*   Login: **d.guillaume**
*   Password: **idelibre**
*   Suffixe: **ville.libre**

---
### Pour en savoir plus...

*   Voir les [pré-requis techniques](pre-requis/73-pre-requis-a-l-utilisation-d-i-delibre-tablette) pour utiliser I-DELIBRE.