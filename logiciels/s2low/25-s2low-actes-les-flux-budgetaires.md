+++
title = "S²LOW / ACTES, gestion des flux budgétaires"
description = "Gestion des flux budgétaires sur S²LOW"
tags = [ "S²LOW", "ACTES" ]

date = "2011-10-04"
+++

Un **acte budgétaire** se télétransmet comme un acte administratif standard sauf qu'il est au format XML (voir votre outil de Gestion Financière et _ToTEM_) à la place du classique PDF des actes administratifs. On sélectionne donc la bonne "nature d'acte": Documents budgétaires et financiers".

Pour information : _ToTEM_ sert à élaborer les documents budgétaires à partir des maquettes budgétaires issues de [ODM](https://adullact.net/projects/odm/) ; à visualiser les actes budgétaires; à sceller et valider les actes budgétaires avant de les envoyer en préfecture. N'hésitez pas à consulter notre article dédié à propos de cette [re-homologation](http://adullact.net/forum/forum.php?forum_id=2894).

Pour en savoir plus, n'hésitez pas à consulter les documents suivants, à disposition des collectivités utilisatrices de Tiers de Télétransmission et issus du Ministère de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration :

*   [Présentation du projet Actes Budgétaires - Dématérialisation des actes budgétaires](http://magasin.adullact.org/modules/wfdownloads/singlefile.php?cid=12&lid=506)
*   [Actes Budgétaires -Comment entrer dans la démarche de dématérialisation des documents budgétaires](http://magasin.adullact.org/modules/wfdownloads/singlefile.php?cid=12&lid=507)

**Pour information** : ToTEM sert à élaborer les documents budgétaires à partir des maquettes budgétaires issues de [ODM](https://adullact.net/projects/odm/) ; à visualiser les actes budgétaires; à sceller et valider les actes budgétaires avant de les envoyer en préfecture. N'hésitez pas à consulter notre article dédié à propos de cette [re-homologation](http://adullact.net/forum/forum.php?forum_id=2894).