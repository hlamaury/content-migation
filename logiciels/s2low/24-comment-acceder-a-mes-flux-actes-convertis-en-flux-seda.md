+++
title = "Comment accéder à mes flux ACTES convertis en flux SEDA"
description = "Comment accéder à mes flux ACTES convertis en flux SEDA"
tags = [ "S²LOW", "as@lae", "ACTES", "Flux SEDA" ]
type = "blog"
+++

Service de conversion SEDA et de stockage des flux ACTES acquittés en préfecture via S²LOW : accéder directement au [service](https://asalae.adullact.org/users/login).

Il s'agit de la conversion de flux ACTES issus du TDT mutualisé S²LOW ADULLACT, conformément au Standard d'Échange de Données pour l'Archivage (SEDA v0.2) et de leur stockage dans un environnement dédié à des fins de consultation et de versement ultérieur vers un SAE agréé. Pour mémoire le SEDA spécifie les transferts d'archives électroniques entre les services producteurs des collectivités et les services de gestion des archives publiques. Il est décrit dans le RGI, et préconisé par le SIAF.

### Préambule : inscription

Pour utiliser ce service, il convient de se faire connaître auprès de l'association ADULLACT. C'est ADULLACT qui configurera le compte de l'adhérent S²LOW, de manière à lui permettre de convertir et déposer ses flux ACTES dans l'espace de stockage dédié.

Les informations à fournir à ADULLACT sont:

*   Collectivité et n° SIREN.
*   Nom, prénom et adresse mail de l'agent responsable (login as@lae).
*   Nom du service producteur.

Tant que cette configuration n'est pas faite, le bouton "versement manuel" ne sera pas opérationnel. Ce bouton "versement manuel" apparaît sur S²LOW, dans le "détail d'un acte", en bas de page, 2 mois après acquittement de l'acte. En parallèle, il convient de configurer l'accès à l'espace de stockage et de pré-archivage , pour permettre aux agents d'accéder en consultation à leurs flux convertis conformément au SEDA.

ADULLACT crée le login (cf ci-dessus) pour l'agent désigné.

Ce login sera en mesure d'accéder aux flux ACTES convertis et stockés dans l'espace de pré-archivage dédié pour sa collectivité dans as@lae. Nous recommandons de faire en sorte que l'agent as@lae ait accès à tous les actes de la collectivité du côté S²LOW. Dans tous les cas, un login sera exigé pour accéder aux archives à partir de S²LOW. Certaines fonctionnalités du SAE as@lae sont utilisées et mises en œuvre dans le cadre de ce service de conversion et de pré-archivage, d'autres fonctionnalités fournies par le logiciel as@lae ne seront pas accessibles aux adhérents (ex: versement manuel, ...).

Sur demande de la collectivité, les flux SEDA stockés peuvent ensuite faire l'objet :

*   de transferts vers un SAE as@lae internalisé par la collectivité,
*   de transferts vers Un tiers-archiveur agrée SIAF exploitant [as@lae](mailto:as@lae)
*   de versements vers un autre système d'archivage électronique intermédiaire ou définitif basé sur le Standard d'Échanges des Données pour l'Archivage (SEDA) choisi par la collectivité.

### Utilisation : processus

Une fois la configuration initiale faite, le bouton "versement manuel" apparaît en bas de page S²LOW de "détail d'un acte", pour tous les actes acquittés depuis plus de 2 mois. Une série d'actions et d'effets se succèdent à l'occasion du dialogue entre les espaces de télé-transmission et de conversion/stockage :

*   L'agent souhaitant convertir et versé son flux ACTES dans l'espace de pré-archivage dédié clique sur "versement manuel".

*   L'acte est envoyé pour conversion et transfert vers l'espace de stockage; Le statut de l'acte est mis à jour du côté S²LOW: "envoyé au SAE".

![](/images/sampledata/FAQ/Logiciels/Flux-ACTES-SEDA1.png)

*   Le SAE (Système d'Archivage Électronique) accuse réception de l'acte: "reçu par le SAE".

*   Le flux est alors accessible dans l'espace dédié, sous la forme d'un fichier SEDA appelé "Entrée", conformément à la sémantique archivistique. Des fonctions de navigation et des filtres permettent de retrouver aisément le/les flux recherché(s)

![](/images/sampledata/FAQ/Logiciels/Flux-ACTES-SEDA2.jpg)

*   Une fois l'acte versé dans l'espace de stockage/pré-archivage, le statut est alors mise à jour du coté S²LOW: "archivé par le SAE".

*   Un lien (URL) est mis à jour du côté S²LOW pour retrouver l'acte versé dans l'espace de stockage/pré-archivage SEDA.

*   Dans la foulée, le fichier principal de l'acte et ses pièces jointes sont effacées du côté S²LOW. Le statut est mis à jour :

![](/images/sampledata/FAQ/Logiciels/Flux-ACTES-SEDA3.jpg)

*   Le lien installé du côté S²LOW permet de récupérer directement l'acte dans l'environnement de stockage SEDA.

![](/images/sampledata/FAQ/Logiciels/Flux-ACTES-SEDA4.jpg)

*   L'acte complet (document principal, pièces jointes, acquittement préfecture) est accessible dans l'espace sous forme d'une archive normée (SEDA).