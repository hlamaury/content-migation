+++
title = "Asqatasun Adullact : nouvelle version majeure et changements"
description = "Nouvelle version du service d'Asqatasun"
tags = [ "Accessibilité", "Migration", "Asqatasun" ]
date = "2021-01-22"
+++

Le **service Asqatasun** de l'Adullact propose la nouvelle **version 5** d'Asqatasun à compter du **27 janvier 2021**.

La nouvelle adresse est : **[asqatasun.adullact.org](https://asqatasun.adullact.org)**.

*   Pour y accéder, vous pourrez vous connecter avec votre compte utilisateur habituel.
*   Les audits réalisés sur l'ancienne version d'Asqatasun ne seront plus disponibles.

### Nouveautés de version 5 d'Asqatasun

*   Gestion du [**RGAA** version 4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/)
*   Une **API** pour intégrer Asqatasun dans des outils tiers
*   Sous le capot :
    *   Mises à jour techniques profondes pour faciliter les évolutions futures (_Spring Boot_, _Flyway_)
    *   Utilisation d'une version récente de **Firefox** (78 ESR), permettant d'auditer facilement les sites web utilisant des technologies modernes (_React_, _Angular_, _VueJs_...)

### Limitations

*   [L'ancienne URL](https://asqatasun.services.adullact.org/) et les audits qu'elle comporte seront supprimés le **15 février 2021**.
*   La fonctionnalité "audit de site" (crawler) est désactivée pour cette nouvelle version. Un travail est en cours pour réactiver cette fonctionnalité dans les prochaines versions d'Asqatasun.

### Questions

Nous serons très heureux de répondre à vos questions par mail sur [asqatasun@adullact.org](mailto:asqatasun@adullact.org).

### Ressources pour utiliser le service Asqatasun

*   [FAQ - Utiliser le service Asqatasun de l'Adullact pour mesurer l'accessibilité des sites web](logiciels/102-accessibilite-avec-asqatasun)
*   [FAQ - Premiers pas avec Asqatasun](logiciels/105-asqatasun-comment-demarrer)
*   [FAQ - Autres articles sur Asqatasun et l'accessibilité](asqatasun)