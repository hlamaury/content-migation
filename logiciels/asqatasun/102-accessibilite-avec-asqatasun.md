+++
title = "Utiliser le service Asqatasun de l\"Adullact pour mesurer l\"accessibilité des sites web"
description = "Utiliser le service d'Asqatasun"
tags = [ "Accessibilité", "Asqatasun" ]
date = "2016-09-07"
+++

**Asqatasun** est un [logiciel libre mesurant le niveau d'accessibilité d'un site web](https://asqatasun.org/), en particulier au regard du **RGAA** (Référentiel général d'amélioration de l'accessibilité).

L'Adullact a mis en place un service à destination de ses adhérents : [asqatasun.adullact.org](https://asqatasun.adullact.org/) leur offrant un accès immédiat au logiciel.

### Démonstration

La démonstration permet de découvrir le service (sans personnalisation)

*   Aller sur [asqatasun.adullact.org](https://asqatasun.adullact.org "Service Asqatasun de l'Adullact")
*   Identifiant : `demo-asqatasun@adullact.org`
*   Mot de passe : `demo4Adullact`

### Demander ses accès

Pour obtenir vos accès au service Asqatasun, il convient de renseigner le [formulaire de demande d'accès au service Asqatasun de l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-asqatasun-adullact).

### Liste de discussions

Quand une collectivité demande un accès au service Asqatasun de l'ADULLACT, nous l'inscrivons sur une liste de diffusion dédiée à Asqatasun et d'une manière plus générale à l'accessibilité. Il est possible d'y échanger entre pairs.

L'association ADULLACT utilise aussi cette liste pour informer les utilisateurs d'éventuels incidents et des évolutions fonctionnelles, ainsi que pour lancer des _Groupes de Travail Collaboratifs_ (GTC).

Si vous utilisez déjà ce service Asqatasun, vous pouvez écrire à [asqatasun@listes.adullact.org](mailto:asqatasun@listes.adullact.org).  
Si ce n'est pas le cas contactez nous pour corriger cet oubli.

### Questions

Nous serons très heureux de répondre à vos questions par mail sur [asqatasun@adullact.org](mailto:asqatasun@adullact.org).

### Ressources pour utiliser le service Asqatasun

*   [FAQ - Premiers pas avec Asqatasun](logiciels/105-asqatasun-comment-demarrer)
*   [FAQ - Autres articles sur Asqatasun et l'accessibilité](asqatasun)