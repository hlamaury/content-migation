+++
title = "Comment installer et configurer PmWiki ?"
description = "<p style=\"text-align: justify;\">1 est écrit en PHP et distribué sous licence GPL. Il est conçu pour être simple à installer, modifier, et entretenir pour différents usages.</p>"
tags = [ "pmWiki" ]
type = "blog"
date = "2006-07-05"
author =  "beatrice"
menu = "logiciels"
+++
<p style="text-align: justify;"><a href="http://www.pmwiki.org/">PmWiki</a> est écrit en PHP et distribué sous licence GPL. Il est conçu pour être simple à installer, modifier, et entretenir pour différents usages.</p>


<p style="text-align: justify;"> </p>
<h3 style="text-align: justify;">1. Installation de l'application</h3>
<p style="text-align: justify;">Télécharger la dernière version de <a href="http://www.pmwiki.org/wiki/PmWiki/Download">pmWiki</a></p>
<ol style="text-align: justify;">
<li>Dézipper l'archive dans votre DocumentRoot</li>
<li>Créer le répertoire 'wiki.d' et lui donner les droits d'écriture à votre server web.</li>
</ol>
<p style="padding-left: 30px; text-align: justify;">Sous unix :</p>
<p style="padding-left: 30px; text-align: justify;"><code>mkdir wiki.d</code></p>
<p style="padding-left: 30px; text-align: justify;"><code>chmod ugo+w wiki.d</code></p>
<p style="padding-left: 30px; text-align: justify;"> </p>
<h3 style="text-align: justify;">2. Configurer l'application pour le français</h3>
<p style="text-align: justify;">Télécharger le fichier i18.tgz</p>
<p style="text-align: justify;">Décompresser le fichier.</p>
<p style="text-align: justify;">Placer les fichiers obtenus dans les répertoires adéquats <code>wikilib.d/</code> et <code>scripts/</code></p>
<p style="text-align: justify;">Créer le fichier config.php dans le répertoire local/</p>
<p style="text-align: justify;">Ecrire dans ce fichier les lignes suivantes afin de le configurer en français :</p>
<p style="text-align: justify;"><code>&lt;?</code></p>
<p style="text-align: justify;"><code>XLPage('fr','PmWikiFr.XLPage');</code></p>
<p style="text-align: justify;"><code>&gt;</code></p>
<h3 style="text-align: justify;"> </h3>
<h3 style="text-align: justify;">3. Lutte anti-spam</h3>
<p style="text-align: justify;"><span style="text-decoration: underline;">Fixer un mot de passe à la page en cours</span> :</p>
<p style="text-align: justify;">Une des façons les plus efficace pour lutter contre le spam sur votre PmWiki est de protéger par un mot de passe votre page, nous allons voir à travers cet example comment rendre possible cette manipulation. Nous copions le lien de la page lorsqu'on clique sur le bouton "Edit":</p>
<p style="text-align: justify;"><em>http://test.adullact.net/wiki/pmwiki.php?n=PAGE.TEST0001?action=edit</em></p>
<p style="text-align: justify;">Nous remplaçons cette URL par:</p>
<p style="text-align: justify;"><em>http://test.adullact.net/wiki/pmwiki.php?n=PAGE.TEST0001?action=attr</em></p>
<p style="text-align: justify;">Une nouvelle page apparaît et vous permet de fixer un mot de passe. Lors des prochains clics sur le bouton "Edit" de cette page un mot de passe vous sera demandé, c'est bien ce que nous voulons faire, protéger par un mot de passe une page de notre PmWiki.</p>
<p style="text-align: justify;"><span style="text-decoration: underline;">Forcer le wiki à demander un nom d'auteur à la modification ou à la création d'une page</span> :</p>
<p style="text-align: justify;">PmWiki offre différents moyens de lutter contre la pollution de votre wiki par du spam.</p>
<p style="text-align: justify;">Depuis la version &gt; pmwiki-2.1.0, il existe un outil intégré forçant le rédacteur à saisir son login.</p>
<p style="text-align: justify;">Ce procédé bloque les robots qui insèrent des spams automatiquement sur les wikis. Il suffit d'ajouter la ligne suivante au fichier <em>local/config.php</em> : <code>$EnablePostAuthorRequired = 1;</code></p>
<p style="text-align: justify;"><span style="text-decoration: underline;">Filtrer les éditions par IP et par mots clefs</span> :</p>
<p style="text-align: justify;">Il existe d'autres outils comme <a href="http://www.pmwiki.org/wiki/Cookbook/Blocklist2">Blocklist2</a></p>
<p style="text-align: justify;">Pour installer ce module, il suffit de copier le ficher Blocklist2.php dans votre répertoire <code>cookbook/</code>.</p>
<p style="text-align: justify;">Compléter ensuite votre fichier de configuration <em>local/config.php</em> en y ajoutant la ligne suivante :</p>
<p style="text-align: justify;"><code>if ($action=='edit') include_once($FarmD.'/cookbook/blocklist2.php');</code></p>
<p style="text-align: justify;">Créer une page Site.Blocklist contenant les adresses IP à bannir, les mots à censurer.</p>
<p style="text-align: justify;">Vous trouverez une page d'exemple en cliquant ici que vous pouvez utiliser et que vous pourrez compléter par la suite.</p>
<h3 style="text-align: justify;"> </h3>
<h3 style="text-align: justify;">4. Supprimer l'historique</h3>
<p style="text-align: justify;">Si votre wiki a été spammé par des taggueurs, il existe un moyen de supprimer l'historique, et ainsi supprimer les traces de vandalisme. Copier le fichier expirediff.php dans le répertoire <em>cookbook</em></p>
<p style="text-align: justify;">Ajouter la ligne suivante dans votre fichier de configuration locale :</p>
<p style="text-align: justify;"><code>local/config.php <em>include_once('cookbook/expirediff.php');</em></code></p>
<p style="text-align: justify;">Dans l'url de la page où vous vous trouvez, et où vous voulez supprimer l'historique, ajoutez en fin d'url : <code>?action=expirediff</code></p>