+++
title = "Logiciels libres"
description = "Présentation des logiciels libres métiers"
tags = [ "Démo", "Comptoire du Libre", "logiciel libre métier" ]
date = "2018-10-02"
+++

Dans sa mission de promotion des logiciels libres pour les métiers des collectivités territoriales nous avons deux axes :

*   les démonstrations : l'objectif est de pouvoir tester un logiciel concrètement, sans pour autant disposer de moyens comparables à un service de production.
*   le comptoir du libre : une plateforme collaborative pour trouver un logiciel, des utilisateurs et des prestataires.

Les démonstrations
------------------

L'[ADULLACT](https://adullact.org "ssociation des Développeurs et Utilisateurs de Logiciels Libres pour les Administrations et les Collectivités Territoriales") maintient en ligne des démonstrations de logiciels libres pour les métiers des collectivités territoriales.

Vous pouvez accéder aux démonstrations de :

*   [i-delibRE](https://www.comptoir-du-libre.org/softwares/15 "Fiche du logiciel i-delibRE sur le Comptoir du libre") : [démonstration](https://idelibre.demonstrations.adullact.org/), nous proposons ce service à nos adhérents sur une instance de production.
*   ecollectivites : [démonstration](https://ecollectivites.demonstrations.adullact.org), nous avons développé le logiciel avec un fond européen FEDER.

Vous pouvez utiliser ces logiciels en démonstration pour les essayer. **Chaque weekend, les données de démonstration sont remises à zéro** à leurs valeurs d'origine.

Le comptoir du libre
--------------------

Si vous recherchez un logiciel libre, utile dans les métiers des collectivités, vous pouvez aussi consulter notre [Comptoir du Libre](https://www.comptoir-du-libre.org/). Identifier des logiciels libres métier n'est que la première des fonctionnalités du comptoir.

Une fois votre(vos) logiciel(s) identifié(s), via la plateforme, vous pouvez entrer en contact avec les collectivités qui se sont déclarées utilisatrices. Cela permet de discuter entre collègues de différentes collectivités. Ceci pour, par exemple, disposer d'informations de terrain quant au comportement d'un logiciel identifié.

Et enfin, une fois que vous avez trouvez le logiciel qui répond à vos besoins, le comptoir permet de connaître les prestataires de services pour accompagner la collectivité à la mise en œuvre, au support ou à la formation.

Vous pourrez, vous aussi, participer à cette chaîne collaborative en vous déclarant collectivité utilisatrice ou rédiger des avis à propos des logiciels libre que vous utilisez dans votre collectivité.

Autres articles intéressants :

*   [Acheter un logiciel libre ?](usages/84-acheter-un-logiciel-libre)
*   [FAQ ADULLACT](https://faq.adullact.org/ "faq")