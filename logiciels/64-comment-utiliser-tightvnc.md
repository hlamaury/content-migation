+++
title = "Comment utiliser TightVNC ?"
description = "Comment utiliser TightVNC"
tags = [ "TightVNC" ]
date = "2005-09-16"
+++

Comment prendre le contrôle d'une machine à distance ? Ou voir l'écran d'une machine distante ?

Pour répondre au besoin de prise de contrôle à distance, on évoque spontanément pcAnywhere, produit commercial de la société Symantec, ou encore VNC, de la société britannique RealVNC, qui propose une version "free" de son logiciel en plus des version "personnal" et "enterprise". Mais il est possible de trouver son bonheur avec des solutions open source telles que TightVNC.

**TightVNC** est une suite logicielle de prise de contrôle à distance dérivée de VNC. Le logiciel possède les caractéristiques suivantes :

*   disponible gratuitement sous licence GPL,
*   utile pour l'administration distante, le support distant aux utilisateurs, l'enseignement et dans beaucoup d'autres applications,
*   logiciel multi plate-forme, disponible pour Windows et Unix, et compatible avec d'autres logiciels VNC,
*   bien maintenu et développé de manière active.

Vous pouvez télécharger le produit à l'adresse suivante : [www.tightvnc.com/download](http://www.tightvnc.com/download.html).

### Installation TightVNC

Linux Installer le package au format "rpm" à l'aide du gestionnaire de paquetages de votre distribution ou installer TightVNC à partir des sources. Il est aussi possible d'installer la version fournie dans la distribution. Vérifier ce qui est disponible à l'aide du gestionnaire de paquetages. MS-Windows Installer le logiciel à partir du fichier .exe auto-installable.

### Démarrage du serveur

*   **Linux**

Sous Linux, après installation, le serveur TightVNC se démarre en tant que root avec la commande suivante :

vncserver

Lors du tout premier lancement du serveur, il vous sera demandé un mot de passe. Celui sera nécessaire pour toute accès distant. Après avoir saisi ce mot de passe, un second mot de passe est demandé, celui-ci pour un accès en "view-only" (pas de contrôle possible).

Par la suite, il sera possible de modifier ce mot de passe avec la commande suivante : vncpasswd

Sur une machine Linux dont le nom d'hôte est myserver, l'affichage X principal est habituellement myserver:0. Il est possible d'y lancer plusieurs serveurs VNC qui apparaitront eux en tant que myserver:1, myserver:2 etc...

Il est aussi possible de démarrer le serveur avec quelques options bien pratiques :

`vncserver -geometry 800x600 -httpd /chemin/des/dossiers/java/classes/ -depth 16:1`

Cette commande démarre le serveur TightVNC avec une résolution de 800x600, 65536 couleurs sur le display 1 et donne le path des classes java. Pour éteindre le serveur TightVNC proprement, tapez la commande vncserver -kill :1

*   **MS-Windows**

Aller dans Démarrer/Programmes/TightVNC et cliquer sur Lauch TightVNC server. Une icône TightVNC apparait dans la barre de tâches. Cliquer avec le bouton droit sur cette icône pour obtenir la boite des propriété. Dans la zone Incoming connexions, cliquer sur le bouton radio port. Les requêtes http seront reçues sur le port n° 5800.

### Vérification du bon fonctionnement du serveur

*   **Connexion "simple" au serveur TightVNC**

Une connexion simple est une connexion directe et non cryptée entre le client et le serveur TightVNC. Pour accéder à distance à la machine myserver, Il est possible d'utiliser un client tel que TightVNC viewer. Le serveur myserver écoute sur le port 590? où "?" correspond au display du serveur X, dans cette exemple nous associons le display à la valeur 1. Bien sur si le serveur TightVNC est derrière un firewall, le port TCP sur le serveur VNC doit être ouvert, dans notre exemple il s'agit du port 5901 en TCP. Nous lançons la commande vncviewer depuis le poste client, une fenêtre de connexion au serveur VNC s'affiche. Nous remplissons ce champ avec myserver::5901, pour valider cette action seule la touche entrée centrale du clavier le permet, celle du carré numérique ne le permet pas. Saisir le password et la connexion est ainsi établie.

On peut aussi se connecter au serveur selon une autre méthode, le serveur VNC contient aussi un petit serveur web. Il est alors possible d'utiliser simplement un navigateur, à condition de l'autoriser à exécuter du code Java. Il téléchargera alors une version java du viewer. Ce serveur écoute sur un numéro de port égal à 5800 + le numéro d'affichage X. Ainsi, le serveur TightVNC, lancé une première fois sur la machine myserver est visible en tant que myserver:1 et est accessible avec un navigateur à l'adresse suivante : [http://myserver:5801/](http://myserver:5801/) L'applet Java demande un login et un mot de passe, puis affiche le bureau de la machine distante.

*   **Connexion "sécurisée" au serveur TightVNC**

Nous allons sécuriser l'échange d'information entre le client et le serveur VNC, nous devons en plus de notre stucture initiale ajouter un serveur SSH, s'il se trouve sur la même station que le serveur VNC, la sécuritée est optimale. Nous créons un tunnel SSH entre le client et le serveur, nous attribuons un port localement sur le client qui serra forwarder sur le serveur , une fois cette connexion établie, nous nous connecterons en localhost sur le port choisi.

Tapons la commande ssh -g -L 5555:myserver:5901 myserver, une fois connecté au serveur SSH nous ouvrons un autre shell et lançons la commande vncviewer. Nous remplissons le champ de connexion au serveur VNC, cette fois si avec localhost::5555, nous validons, donnons le mots de passe et l'échange entre le client et le serveur est établi.

Les datas sont encapsulées dans un flux réseau SSH qui sécurise l'échange sur le medium.

Pour ce qui est de créer un tunnel ssh avec le type de connexion faisant appel à l'applet java, nous rencontrons des problèmes qui sont dus à la redirection de port, l'applet java semble mal fonctionner dés lors que l'on utilise un proxy ou dés que nous manipulons la translation de ports. Pour cette raison nous ne pouvons pas sécuriser ce type de transaction. Si vous souhaitez plus d'informations sur ce sujet rendez-vous à ladresse suivante : [www.csd.uwo.ca/staff/magi/doc/vnc/faq.html](http://www.csd.uwo.ca/staff/magi/doc/vnc/faq.html)

Note: L'un des points forts de la méthode de connexion via un tunnel SSH réside dans le fait qu'il n'est pas nécéssaire d'ouvrir spécialement le port TCP sur le firewall pour se connecter au serveur TightVNC (5901/tcp). Seul le port ssh (22/tcp) doit être ouvert.

Par contre nous devons disposer d'un un compte UNIX (login/password) sur le serveur SSH.