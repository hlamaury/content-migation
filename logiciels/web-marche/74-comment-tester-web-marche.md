+++
title = "Comment tester web-marché ?"
description = "Tester Web-Marché"
tags = [ "web-marché", "Marchés publics" ]
date = "2011-08-18"
+++
Comment tester la plateforme de marché public de l'ADULLACT, WEB-marché, offerte à ses adhérents.  
  
Accéder directement au [service](http://webmarche.test.adullact.org/collectivite/).

### Tester web-marché

Pour faciliter la prise en main de notre plate-forme de dématérialisation des marchés publics, nous mettons à disposition de nos adhérents une plate-forme école ainsi que des accès pour s'y connecter. Ces accès de test permettent de se connecter en tant qu'agent, ou élu (ou entreprise...) de la collectivité école ADULLACT34

*   Le chemin d'accès de notre plate-forme école en tant que **collectivité**, est : [webmarche.test.adullact.org/collectivite/](http://webmarche.test.adullact.org/collectivite/)
*   Le chemin d'accès de notre plate-forme école en tant qu'**entreprise**, est : [webmarche.test.adullact.org/](http://webmarche.test.adullact.org/). Les entreprises (ainsi que ceux qui souhaitent _jouer_ le rôle d'une entreprise) s'auto-inscrivent sur la plateforme à l'aide de leur n° de SIREN.
*   Le login de test, pour jouer le rôle d'un **agent** du service des marchés publics est : `agent_ADULLACT34`; mot de passe: `Adullact2011`
*   Le login de test, pour jouer le rôle d'un **élu** habilité à ouvrir les enveloppes est : `elu_ADULLACT34`; mot de passe: `Adullact2011`

Une fois connecté, vous avez accès à toutes les fonctionnalités de notre logiciel WEBMARCHE en tant qu'acheteur de la collectivité. De l'aide et des documentations sont accessibles en ligne, une fois connecté, via le menu "aide".

> Remarque: les documentations fournies sont sous la banière "région aquitaine" car la **Région Aquitaine** a gracieusement créé et fourni ces documentations à la communauté des utilisateurs WEBMARCHE. Ces documents sont sous licence libre _creatives-commons_.

### Cycle de test complet

Pour tester un cycle complet, il conviendra de jouer aussi le rôle d'une entreprise qui répond au marché public. Sa réponse sera chiffrée. Pour cela, un certificat a été stocké sur la plateforme pour le compte de la collectivité de test ADULLACT34. C'est ce certificat (son nom: elu) que l'entreprise qui répond devra choisir.  
Pour jouer un cycle de test complet, nous vous invitons à simuler une inscription d'entreprise sur la [plateforme de test](http://webmarche.test.adullact.org/), afin de proposer votre propre email côté entreprise et ainsi tester la mécanique des échanges entre l'entreprise et la collectivité.

> Remarque: l'email du côté collectivité ne reçoit que des notifications correspondant à ce qui est affiché dans la rubrique "registre" du marché concerné par ces échanges.

  
La gestion des notifications et questions/réponses se fait via l'interface dédiée:

![](/images/sampledata/FAQ/Logiciels/Test_web-marche1.png)  
Exemple de mail:

*   Exemple de notification de question posée par une entreprise, envoyé à tous les agents concernés:

> `Sujet: Réception d'une question électronique - Réf : xyz123  
> De : "MPE - Portail ATEXO" <testwebmarche@adullact.org>  
> Date : 28/10/2011 14:58  
> Pour : webmarche_elu@adullact.org  
> 
> Bonjour,  
> Une question a été posée par un Opérateur économique pour la consultation définie ci-après :  
> Entité publique : ADULLACT34 - ADULLACT  
> Service : ADULLACT34 - ADULLACT  
> Intitulé de la consultation : testMarchePublic  
> Objet de la consultation : ceci est mon test de MarchePublic  
> Référence consultation :xyz123  
> Type de procédure : Procédure adaptée  
> Date de mise en ligne : 27/10/2011 16:51  
> Date et heure limite de remise des plis : 02/11/2011 17:30  
> Pour accéder à cette consultation, veuillez cliquez sur le lien suivant :  
> http://webmarche.test.adullact.org?page=agent.TableauDeBord&AS=0&ref=6  
>   
> Identification de la personne ayant posé la question :  
> Nom : Kuczynski  
> Prénom : Pascal  
> Adresse électronique : entreprise@entreprise.fr  
> Contenu de la question :  
> _ceci est ma question à propos du marché testMarchePublic_  
> 
> Pièce jointe à la question disponible sur la plate-forme : Non  
> Date et heure du dépôt de la question : 28/10/2011 14:58  
>  
> Cordialement,  
> Le Portail de Dématérialisation des Marchés publics de L'ADULLACT`

*   Exemple de notification de réponse déposée par une entreprise, envoyé à tous les agents/élus concernés:

> Sujet: Réception d'une réponse électronique - Réf : xyz123  
> De : "MPE - Portail ATEXO" <testwebmarche@adullact.org>  
> Date : 28/10/2011 14:52  
> Pour : webmarche_agent@adullact.org  
>  
> Bonjour,  
> Une réponse électronique a été déposée pour la consultation définie ci-après :  
>  
> Entité publique : ADULLACT34 - ADULLACT  
> Service : ADULLACT34 - ADULLACT  
> Intitulé de la consultation : testMarchePublic  
> Objet de la consultation : ceci est mon test de MarchePublic  
> Référence consultation : xyz123  
> Type de procédure : Procédure adaptée  
> Date de mise en ligne : 27/10/2011 16:51  
> Date et heure limite de remise des plis : 02/11/2011 17:30  
> Pour accéder à cette consultation, veuillez cliquez sur le lien suivant :  
> http://webmarche.test.adullact.org?page=agent.TableauDeBord&AS=0&ref=6  
>   
> Horodatage du dépôt : 28/10/2011 14:52  
>   
> Cordialement,  
> Le Portail de Dématérialisation des Marchés publics de L'ADULLACT  

Après avoir simulé une réponse en tant qu'entreprise, vous pourrez déchiffrer la(les) réponse(s) en question directement en ligne :

![](/images/sampledata/FAQ/Logiciels/Test_web-marche2.png)  
  
Puis décider à quelle entreprise attribuer ce marché:

### ![](/images/sampledata/FAQ/Logiciels/Test_web-marche3.png)

### Mise en production

Pour démarrer WEBMARCHE en production dans sa collectivité, lire [le billet](index.php?2011/08/18/74-comment-demarer-en-production-avec-webmarche). 

**Pour en savoir plus :** Se rendre sur le [projet webmarche](https://adullact.net/projects/webmarche/) sur la forge ADULLACT.