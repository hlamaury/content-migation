+++
title = "Démarches Simplifiées"
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour objet de faciliter la dématérialisation des démarches administratives par toute administration.

*   [Conditions Générales pour les Administrations]({{< relref "./123-cgu-pour-les-administrations-du-service-demarches-simplifiees-adullact" >}})
*   [Conditions Générales d'Utilisation pour les Usagers]({{< relref "./121-cgu-pour-les-usagers-du-service-demarches-simplifiees-adullact" >}})
*   [Mentions légales]({{< relref "./119-mentions-legales-demarches-simplifiees-adullact" >}})