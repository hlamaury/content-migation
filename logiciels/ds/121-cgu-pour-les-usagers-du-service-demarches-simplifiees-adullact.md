+++
title = "CGU pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
description = "Condition générales d'utilisation de Démarches Simplifiées"
tags = [ "CGU", "Démarches Simplifiées (DS)", "utilisateurs" ]
date = "2021-02-05"
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour objet de faciliter la dématérialisation des démarches administratives par toute administration.

*   [Conditions Générales pour les Administrations]({{< relref "./123-cgu-pour-les-administrations-du-service-demarches-simplifiees-adullact" >}})
*   **Conditions Générales d'Utilisation pour les Usagers**
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
*   [Mentions légales](logiciels/119-mentions-legales-demarches-simplifiees-adullact)

* * *

CONDITIONS GÉNÉRALES POUR LES USAGERS
=====================================

Présentation
------------

Le site **demarches.adullact.org**, ci-après « le Service », est un service édité et hébergé par l’ADULLACT qui a pour objet de faciliter la dématérialisation des Démarches Administratives par toute Administration.

Le présent document a pour objet de régler les modalités d’utilisation du Service par les usagers.

Toute utilisation du service est subordonnée à l'acceptation préalable et au respect intégral des présentes conditions générales d’utilisation (CGU).

* * *

Objet
-----

Le Service permet :

*   la dématérialisation de Démarches Administratives par les administrations ;
*   la réalisation en ligne de ces Démarches Administratives par l’administration qui le souhaite et la faculté pour elle de partager la construction du dossier avec les personnes de son choix ;
*   l’instruction partagée et le recueil d’avis par pour traiter les dossiers déposés ;
*   le suivi des dossiers à traiter et l’accès à un tableau de bord récapitulatif pour les administrations ;
*   le dialogue entre l’ensemble des utilisateurs du Service ;
*   l’accès à un tableau de bord des démarches engagées.

Ces conditions d’utilisation s’appliquent à l’ensemble des formulaires proposés par le service et s’imposent à tout usager, sauf conditions générales particulières directement attachées à la démarche.

L’utilisation du service est facultative et gratuite. Les usagers choisissent librement les formulaires hébergés auxquels ils souhaitent accéder.

* * *

Définitions
-----------

*   **Démarche Administrative** ou **Démarche** : demande présentée par le Public ou déclaration transmise par celui-ci, par l’intermédiaire du Service, à une Administration ;
*   **Administration** : personne publique ou privée chargée d'une mission de service public administratif au sens du 1° de l’article L.100-3 du code des relations entre le public et l’administration (ci-après « le CRPA ») ;
*   **Administrateur** : personne physique, agissant pour le compte d’une Administration, responsable de la création de la Démarche Administrative ainsi que de l’organisation de son instruction ;
*   **Instructeur** : personne physique ayant obtenu délégation par l’Administrateur pour intervenir sur le traitement et l’instruction d’une Démarche Administrative ;
*   **Expert** : personne physique qui a obtenu une délégation d’un Instructeur pour formuler un avis sur un Dossier ;
*   **Public** : personne physique ou personne morale usager du Service ;
*   **Invité** : personne physique, invitée par le Public pour intervenir sur son Dossier ;
*   **Dossier** : ensemble des éléments fournis par le Public lorsqu’il réalise une Démarche.

* * *

1\. Conditions générales relatives aux Usagers
----------------------------------------------

 Le présent service permet aux usagers des services publics de :

*   s’inscrire pour créer un compte usager ;
*   compléter et soumettre les formulaires accessibles depuis le service ;
*   accéder à une vision consolidée de l’ensemble des démarches qu’ils ont réalisées.

L’utilisation du service requiert une connexion internet et un navigateur récent.

L'application affichera un message si le navigateur utilisé est trop ancien pour être compatible.

Pour utiliser les formulaires hébergés, l’usager :

*   doit être majeur ;
*   doit réaliser la démarche en son nom propre. S’il intervient pour le compte d’une autre personne, il est mandaté ; chacune des parties concernées conserve les preuves matérielles de la réalisation du mandat.

Les données transmises au service par ses soins restent de la responsabilité de l’usager

* * *

2\. Fonctionnalités
-------------------

### 2.1. Inscription sur la plateforme

Lors de l’inscription au service, l’usager choisit un identifiant (une adresse électronique valide) et un mot de passe.

L’usager doit sécuriser et conserver son identifiant et son mot de passe qui sont utilisés pour tout accès à son compte personnel.

Le mot de passe doit être choisi par l’usager de façon qu’il ne puisse pas être deviné par un tiers.

L’usager s’engage à en préserver la confidentialité.

L’usager s’engage à avertir immédiatement l’ADULLACT du service de toute utilisation non autorisée de ces informations.

L’ADULLACT ne pouvant être tenu pour responsable des dommages éventuellement causés par l’utilisation du mot de passe par une personne non autorisée.

L’ADULLACT se réserve le droit de résilier, sans préavis ni indemnité d’aucune sorte, tout compte faisant l’objet d’une utilisation illicite, frauduleuse ou contraire aux présentes CGU.

L’usager est libre de créer un compte en bénéficiant du dispositif FranceConnect.

En l’utilisant, il consent aux conditions générales d’utilisation de ce service.

La création d’un compte usager permet dès validation de l’inscription de :

*   Remplir un formulaire et donc réaliser une démarche en ligne ;
*   Enregistrer un brouillon de démarche en ligne, en vue de compléter sa démarche ;
*   Discuter en ligne avec un instructeur ou un invité ;
*   Inviter un autre usager à accéder à son dossier.

### 2.2. Réalisation d’une Démarche en ligne

L’usager remplit en ligne le formulaire et valide celui-ci en y joignant éventuellement les pièces nécessaires au traitement de son dossier. La confirmation et la transmission du formulaire par l’usager vaut signature de celui-ci.

En utilisant le service, l’usager s’engage sur la véracité des informations transmises lors du dépôt de son dossier.

Pour des raisons techniques, l’usager ne peut joindre des documents dépassant la somme de 20 méga-octets par envoi.

En cas de pièce justificative volumineuse, l’usager peut réitérer l’envoi de documents.

Pour des raisons de sécurité, l’ensemble des actions relatives à un dossier est enregistré en vue notamment d’horodater toute action et envoi.

Ces éléments techniques recueillis par l’ADULLACT font office de preuve.

L’usager est informé par voie électronique, à l’adresse utilisée pour créer son compte, des différentes étapes de son dossier et notamment : l’accusé réception par le service instructeur, la demande d’information complémentaire et l’issue donnée à la demande.

L’usager s’engage à ne fournir, dans le cadre de l’utilisation du service, que des informations exactes, à jour et complètes. Dans l’hypothèse où l’usager ne s’acquitterait pas de cet engagement, l’ADULLACT ou l’Administration utilisatrice se réservent le droit de suspendre ou supprimer sa demande, sans préjudice d’éventuelles poursuites judiciaires.

Il est rappelé que toute personne procédant à une fausse déclaration pour elle-même ou pour autrui s’expose, notamment, aux sanctions prévues à l’article 441-1 du code pénal, prévoyant des peines pouvant aller jusqu’à trois ans d’emprisonnement et 45 000 euros d’amende.

### 2.3. Invitation d’autres usagers à intervenir sur un dossier

L’usager a la faculté de déléguer un accès à un tiers en vue de l’accompagner dans la complétion de sa formalité. Cette invitation se fait sous l’entière responsabilité de l’usager qui consent à donner accès à l’ensemble du dossier, y compris les données à caractère personnel le concernant qui y figurent. L’usager utilise cette fonctionnalité à son initiative uniquement. Cette invitation permet à l’invité de compléter la demande (ajout de pièce justificative, d’éléments écrits etc.). En aucun cas, l’invité ne pourra soumettre une demande au nom et pour le compte de l’usager.

Cette invitation est personnelle et ouverte sur la durée de vie du dossier (temps de dépôt et d’instruction). Elle est irrévocable.

Pour accéder au dossier, l’invité doit créer un compte usager, il est donc soumis aux présentes conditions générales d’utilisation.

Sur signalement, l’ADULLACT peut révoquer une invitation et se donne le droit de supprimer tout compte contrevenant aux présentes conditions générales d’utilisation.

### 2.4. Échanges avec les utilisateurs

 Les discussions avec les autres usages, les instructeurs et les invités ont vocation à porter sur la démarche administrative proposée dans le cadre du Service.

Les usagers ne publient pas de messages de nature publicitaire ou promotionnelle, à caractère raciste ou diffamatoire, grossier ou injurieux, agressif ou violent.

Les représentants de l’administration partenaire, comme l’ADULLACT, se réservent notamment le droit de supprimer les contributions sans lien avec la démarche entamée, publiées aux fins d’entraver le bon fonctionnement de la plateforme, de publicité ou de promotion, de propagande ou de prosélytisme et toute contribution contrevenant au cadre juridique en vigueur.

### 2.5. Consultation du tableau de bord

 L’usager obtient une référence de dossier unique qui lui permettra de suivre le traitement de son dossier. C’est l’objet du tableau de bord qui permet d’avoir une vision consolidée des démarches entamées (enregistrées comme des brouillons), des démarches déposées (en cours d’instruction) et des démarches réalisées (acceptées, refusées ou classées sans suite).

### 2.6. Accessibilité

Le service n'a pas encore fait l'objet d'une déclaration de conformité RGAA (Référentiel général d'accessibilité pour les administrations).

* * *

 3. Engagements de l’ADULLACT
-----------------------------

Le service permet la mise en relation d’une autorité administrative partenaire et d’un usager.

En aucune manière, l’envoi d’un dossier via le service ne garantit l’acceptation d’une demande. Toutefois, l’ADULLACT se réserve la liberté de suspendre, moyennant un préavis de 48h, le Service pour des raisons de maintenance.

En cas d’urgence, cette suspension pourra intervenir sans préavis ou pour tout autre motif jugé nécessaire. Dans ces situations, les temps de suspension ne seront pas comptabilisés dans la disponibilité.

L’indisponibilité du Service ne saurait ouvrir droit à aucune compensation quelle qu’en soit sa nature.

L’ADULLACT assure un support exclusivement sur les parties techniques de l’outil.

L’ADULLACT s’engage à assurer le suivi et l’évaluation de l’utilisation du Service, et à communiquer les résultats obtenus aux différents partenaires

L’ADULLACT ne saurait être tenue responsable des contenus publiés par les administrations partenaires qu’il héberge ou les usagers. Dès qu’elle a connaissance de contenus illicites, l’ADULLACT agit rapidement pour retirer ces données ou en rendre l'accès impossible. Tout usager peut signaler tout contenu non conforme aux présentes conditions d’utilisation. Ainsi, l’ADULLACT ne modère pas de contenus a priori, mais uniquement a posteriori.

* * *

4\. Responsabilité
------------------

L’ADULLACT ne saurait être responsable des conséquences, directes ou indirectes, pouvant découler des erreurs de créations de formulaires, de saisie de coordonnées ou de toute autre information incomplète ou erronée transmise via le Service.

Aucune des parties ne sera responsable vis-à-vis de l'autre partie d'un retard d'exécution ou d'une inexécution en raison de survenance d'un événement en dehors du contrôle des parties qui ne pouvait être raisonnablement prévu lors de l'acceptation des CGU et dont les effets ne peuvent pas être évités par des mesures appropriées.

* * *

5\. Force majeure
-----------------

Le cas de force majeure suspend les obligations de la partie concernée pendant le temps où jouera la force majeure si cet évènement est temporaire. Néanmoins, les parties s'efforceront d'en minimiser dans toute la mesure du possible les conséquences. A défaut, si l'empêchement est définitif, les parties seront libérées de leurs obligations dans les conditions prévues aux articles 1351 et 1351-1 du Code civil.

* * *

6\. Protection des données personnelles
---------------------------------------

### 6.1. Cookie

Le Service dépose des cookies de mesure d’audience (nombre de visites, pages consultées), respectant les conditions d’exemption du consentement de l’internaute définies par la recommandation « Cookies » de la Commission nationale informatique et libertés (CNIL).

Cela signifie, notamment, que ces cookies ne servent qu’à la production de statistiques anonymes et ne permettent pas de suivre la navigation de l’internaute sur d’autres sites.

Le site dépose également des cookies de navigation, aux fins strictement techniques, qui ne sont pas conservés. La consultation de la plateforme n’est pas affectée lorsque les utilisateurs utilisent des navigateurs désactivant les cookies.

### 6.2. Service tiers

Le service utilise Mailjet, un service français d’envoi d’emails. Ce service tiers est hébergé en France et dispose de ses propres modalités d’utilisation.

### 6.3. Données à caractère personnel

Pour réaliser les formalités nécessaires proposées par les Administrations utilisatrices, l’usager transmet des données à caractère personnel que le service conserve. Par ailleurs, l’usager consent à l’accès de son dossier, et donc à ses données à caractère personnel, par les personnes habilitées, c’est-à-dire les instructeurs et les personnes qu’ils peuvent inviter en vue de statuer sur sa demande ; l’usager peut également donner accès à son dossier à un autre usager.  
  

#### 6.3.1. Responsabilité du traitement

L’Administration utilisatrice est considérée comme étant responsable des traitements opérés sur les données reçues dans le cadre du Service et, à ce titre, respecte les obligations inhérentes à ce traitement, notamment celles prévues par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés et par le règlement européen nᵒ 2016/679 dit règlement général sur la protection des données.

Elle s’engage à ne pas commercialiser les données reçues, à ne pas les communiquer à des tiers en dehors des cas prévus par la loi et à effectuer toutes formalités préalables obligatoires auprès de la Commission Nationale de l’Informatique et des Libertés.

L’Administration utilisatrice s’engage à mettre en œuvre les mesures de sécurité techniques et organisationnelles nécessaires dans le cadre de sa démarche.

En cas de non-respect, l’ADULLACT se garde le droit de suspendre ou de supprimer la démarche, et tous les dossiers associés.

En utilisant le Service, l’Administration utilisatrice s’engage en outre à respecter un niveau de qualité, et notamment à assurer un service d’aide au Public pour ses Démarches et à traiter les Dossiers dans les meilleurs délais.

Il appartient au responsable de traitement de fournir l’information aux personnes concernées par les opérations de traitement au moment de la collecte des données.  
  

#### 6.3.2. Sous-traitant

De son côté l’ADULLACT est sous-traitant des traitements opérés.

L’ADULLACT s’engage à mettre en œuvre toutes mesures appropriées, afin de protéger les données traitées dans le cadre du Service et notamment celles prévues par la loi n°78-17 du 6 janvier 1978 modifiée, l’ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l'application des articles 9, 10 et 12 de l'ordonnance n° 2005-1516 du 8 décembre 2005.

La responsabilité de l’ADULLACT ne s’entend que sur la durée pendant laquelle les données sont effectivement présentes sur le Service.

La ou les finalité(s) du traitement, les données à caractère personnel traitées et les catégories de personnes concernées sont définies et documentées par l’Administration utilisatrice en fonction de la Démarche utilisée.

Dans ce cadre, l’ADULLACT s’engage à ne traiter les données **que pour la ou les seule(s) finalité(s)** qui fait/font l’objet de la sous-traitance, à **garantir la confidentialité** des données à caractère personnel traitées dans le cadre du présent contrat.

L’ADULLACT veille à ce que les **personnes autorisées à traiter les données à caractère personnel** en vertu du présent contrat :

*   s’engagent à respecter la confidentialité ou soient soumises à une obligation légale appropriée de confidentialité ;
*   reçoivent la formation nécessaire en matière de protection des données à caractère personnel.

L’ADULLACT s’engage à prendre en compte pour chaque outil permettant le fonctionnement du Service les principes de **protection des données dès la conception** et de **protection des données par défaut**.

Toute sous-traitance ultérieure fera l’objet d’une information préalable indiquant clairement les activités de traitement sous-traitées, l’identité et les coordonnées du sous-traitant et les dates du contrat de sous-traitance. Le responsable de traitement disposera d’un délai de 8 jours à compter de la date de réception de cette information pour présenter d’éventuelles objections, lesquelles seront alors suspensives de la sous-traitance.

Tout sous-traitant ultérieur sera tenu de respecter les obligations du présent contrat pour le compte et selon les instructions du responsable de traitement. Il appartiendra à l’ADULLACT de s’assurer que le sous-traitant ultérieur présente les mêmes garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière à ce que le traitement réponde aux exigences du règlement européen sur la protection des données. Si le sous-traitant ultérieur ne remplit pas ses obligations en matière de protection des données, le sous-traitant initial demeure pleinement responsable devant le responsable de traitement de l’exécution par l’autre sous-traitant de ses obligations.

Lorsque les personnes concernées exerceront auprès de l’ADULLACT des demandes d’exercice de leurs droits au regard de la protection de leurs données personnelles, l’ADULLACT adressera ces demandes dès réception à l’Administrateur concerné.

L'ADULLACT est autorisée à faire appel à un autre sous-traitant pour mener les activités de traitement, dès lors qu’elle s’est assurée du respect par le sous-traitant des obligations et mesures de sécurité auxquelles l'ADULLACT est elle-même tenue en vertu de ce contrat.

L’ADULLACT aidera le responsable de traitement pour la réalisation d’analyses d’impact relative à la protection des données, ainsi que pour la réalisation de toute consultation préalable de l’autorité de contrôle. Elle mettra à la disposition du responsable de traitement **la documentation nécessaire pour démontrer le respect de toutes ses obligations** et pour permettre la réalisation d'audits, y compris des inspections, par le responsable du traitement ou un autre auditeur qu'il aurait mandaté, et contribuer à ces audits.

L’ADULLACT tient un registre des traitements, et a désigné un délégué à la protection des données conformément à l’article 37 du règlement européen sur la protection des données. Il est joignable :

*   par voie postale à l’adresse de l’association.
*   par voie électronique à l’adresse [contact-rgpd@adullact.org](mailto:contact-rgpd@adullact.org)

Toute demande d’exercice des droits des usagers, notamment d’information ou de suppression de compte peut être faite directement auprès de l’ADULLACT.  
  

#### 6.3.3. Durée de conservation des données

L’Administrateur détermine la durée de conservation des données de sa Démarche.

Le Service conserve les données uniquement le temps nécessaire à l'instruction.

Une fois la décision rendue, le Dossier est mis à disposition de l’Instructeur et du Public pour téléchargement.

A la date de fin de conservation du dossier fixé par l'Administration, l’ADULLACT supprime le Dossier des bases du Service.

Seule une trace du Dossier est conservée. Celle-ci indique l’adresse électronique de l’usager, le nom de l’Administration chargée de la Démarche, la date de dépôt de la Démarche, la date de décision, le nom de la Démarche. Cette trace est accessible au Public et à l’Instructeur dans leur tableau de bord respectif.

Conformément aux dispositions des articles L. 114-8 et suivants du code des relations entre le public et l’administration, les administrations échangent entre elles les informations strictement nécessaires à l’accomplissement de la démarche administrative.

* * *

7\. Propriété intellectuelle
----------------------------

Il est rappelé que certains éléments présents sur le site sont couverts par des droits de propriété intellectuelle. C’est le cas des marques verbales, figuratives ou semi-figuratives (logos des administrations notamment) contenues sur ce site. Toute représentation desdits éléments est strictement interdite par les titulaires des droits.

L'atteinte portée au droit du propriétaire de la marque constitue une contrefaçon engageant la responsabilité civile de son auteur.

Le code source de la plateforme est libre et disponible sur [GitHub](https://github.com/betagouv/demarches-simplifiees.fr).

* * *

8\. Droit applicable
--------------------

 Conformément à l’article 17 de la Loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique (LCEN), l’Application est soumise au droit français, ainsi que les présentes CGU.

Toutes difficultés relatives à la validité, l'application ou à l'interprétation des CGU seront soumises, à défaut d'accord amiable, au Tribunal Judiciaire de Paris (France), auquel les Parties attribuent compétence territoriale exclusive, quel que soit le lieu d'exécution ou le domicile du défendeur.

* * *

9\. Évolution des conditions d’utilisation
------------------------------------------

 Les termes des présentes CGU peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au Service, de l’évolution de la législation ou pour tout autre motif jugé nécessaire.

L’ADULLACT s’engage à avertir les usagers de l’entrée en vigueur de nouvelles conditions d’utilisation.

* * *

*   [Conditions Générales pour les Administrations]({{< relref "./123-cgu-pour-les-administrations-du-service-demarches-simplifiees-adullact" >}})
*   **Conditions Générales d'Utilisation pour les Usagers**
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
*   [Mentions légales](logiciels/119-mentions-legales-demarches-simplifiees-adullact)