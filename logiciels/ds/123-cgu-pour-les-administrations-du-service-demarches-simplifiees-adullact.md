+++
title = "CGU pour les administrations du service \"Démarches Simplifiées\" ADULLACT"
description = "Conditions Générales d'utilisateurs pour les administrations"
tags = [ "CGU", "administrations", "Démarches Simplifiées (DS)" ]
date = "2021-02-05"
+++
Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour objet de faciliter la dématérialisation des démarches administratives par toute administration.

*   **Conditions Générales pour les administrations**
*   [Conditions Générales d'Utilisation pour les usagers]({{< relref "./121-cgu-pour-les-usagers-du-service-demarches-simplifiees-adullact" >}})
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
*   [Mentions légales](logiciels/119-mentions-legales-demarches-simplifiees-adullact)

* * *

CONDITIONS GÉNÉRALES POUR LES ADMINISTRATIONS CRÉATRICES DE FORMULAIRES
=======================================================================

Présentation
------------

Le site **demarches.adullact.org**, ci-après « le Service », est un service édité et hébergé par l’ADULLACT qui a pour objet de faciliter la dématérialisation des Démarches Administratives par toute Administration.

Le présent document constitue les modalités d’utilisation du Service par les Administrations créatrices de formulaires.

Toute utilisation du Service est subordonnée à l'acceptation et au respect intégral des présentes conditions générales d’utilisation (ci-après « les CGU »).

* * *

Objet
-----

Le Service permet :

*   la dématérialisation de Démarches Administratives par les administrations ;
*   la réalisation en ligne de ces Démarches Administratives par l’administration qui le souhaite et la faculté pour elle de partager la construction du dossier avec les personnes de son choix ;
*   l’instruction partagée et le recueil d’avis par pour traiter les dossiers déposés ;
*   le suivi des dossiers à traiter et l’accès à un tableau de bord récapitulatif pour les administrations ;
*   le dialogue entre l’ensemble des utilisateurs du Service ;
*   l’accès à un tableau de bord des démarches engagées.

L’utilisation du Service est libre et facultative.

* * *

Définitions
-----------

Au sens des CGU, sont considérés comme :

*   **Démarche Administrative** ou **Démarche** : demande présentée par le Public ou déclaration transmise par celui-ci, par l’intermédiaire du Service, à une Administration ;
*   **Administration** : personne publique ou privée chargée d'une mission de service public administratif au sens du 1° de l’article L.100-3 du code des relations entre le public et l’administration (ci-après « le CRPA ») ;
*   **Administrateur** : personne physique, agissant pour le compte d’une Administration, responsable de la création de la **Démarche Administrative** ainsi que de l’organisation de son instruction ;
*   **Instructeur** : personne physique ayant obtenu délégation par l’Administrateur pour intervenir sur le traitement et l’instruction d’une **Démarche Administrative** ;
*   **Expert** : personne physique qui a obtenu une délégation d’un Instructeur pour formuler un avis sur un Dossier ;
*   **Public** : personne physique ou personne morale usager du Service ;
*   **Invité** : personne physique, invitée par le Public pour intervenir sur son Dossier ;
*   **Dossier** : ensemble des éléments fournis par le Public lorsqu’il réalise une Démarche.

* * *

1\. Conditions générales relatives aux Administrations partenaires
------------------------------------------------------------------

Cette section organise les relations entre les Administrations souhaitant bénéficier du Service et l’ADULLACT. L’inscription de l’Administrateur emporte l’acceptation sans réserve, des CGU.

### 1.1. Inscription sur la plateforme et ouverture de l’accès au Service

L’Administration souhaitant devenir utilisatrice du Service adresse une demande d’inscription d’un Administrateur.

Celle-ci est réalisée en ligne sur le site internet: [demarches.adullact.org](https://demarches.adullact.org)

Suite à la demande d’inscription d’un Administrateur, l’Administration reçoit une réponse après vérification par l’ADULLACT de l’éligibilité de son Administration.

Seules peuvent être inscrites en tant qu’Administrateur, les personnes physiques ayant capacité à agir pour le compte de leur Administration.

Les Administrations utilisatrices du Service mettent en œuvre les Démarches conformément aux dispositions législatives et réglementaires en vigueur et notamment à celles du CRPA relatives à la saisine par voie électronique, l’accusé réception électronique, l’échange d’informations entre administrations et la motivation des actes administratifs.

### 1.2. Fonctionnalités

L’Administrateur enregistré par son Administration bénéficie des fonctionnalités suivantes :

*   créer des formulaires en ligne à destination du Public ;
*   créer des formulaires en ligne à destination des Instructeurs ;
*   créer des emails informant automatiquement le Public de l’état d’avancement de sa Démarche ;
*   créer une attestation ;
*   déléguer et le cas échéant révoquer un Instructeur ;
*   renseigner les informations concernant son service telles que l’adresse ou le numéro de téléphone, les coordonnées du délégué à la protection des données personnelles et les informations concernant la Démarche telles que son cadre juridique ou la durée de conservation des données.  
      
    

#### 1.2.1. Création d’une Démarche

L’Administrateur est responsable des Démarches créées. Il veille notamment à ce que la Démarche soit prévue par une disposition législative ou réglementaire, soit instruite et traitée dans ce cadre, ou s’inscrive dans la gestion de processus internes à l’Administration.

Le Service n’est pas conçu pour les démarches nécessitant la collecte de données relevant du I de l’article 6 de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, c’est-à-dire toutes données qui révèlent la prétendue origine raciale ou l'origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale d'une personne physique ou de traiter des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique.

L’ADULLACT se réserve le droit d’amender, suspendre ou supprimer toute production qui méconnaîtrait les CGU ou nuirait à l’image du Service.  
  

#### 1.2.2. Échanges de données entre administrations

Le Service met en œuvre, le cas échéant, un échange d’informations entre Administrations par traitements automatisés.

En créant une Démarche nécessitant ces échanges, l’Administration utilisatrice consent à l’ensemble des conditions générales d’utilisation spécifiques à l’Administration fournissant ces informations.

La liste des traitements automatisés mis en œuvre est disponible sur le Service.  
  

#### 1.2.3. Instructions des Démarches

##### Administrateur

L’administrateur :

*   est la seule personne susceptible de créer, archiver ou supprimer une Démarche ;
*   veille à ne fournir des accès qu’aux Instructeurs habilités à connaître des informations traitées ;
*   est la seule personne ayant accès au jeton permettant de récupérer par traitement automatisé l’ensemble des informations d’une Démarche, d’un Dossier ainsi que leurs métadonnées et les données liées à leur instruction.

##### Instructeur

L'instructeur accède au Service uniquement à l’initiative de l’Administrateur. Il s’agit uniquement de personnes ayant le droit de connaître les éléments constitutifs d’un Dossier.

Il est invité uniquement pour une Démarche donnée, il s’agit généralement du service instructeur compétent de l’Administration concernée.

L'Instructeur rend la décision sur la Démarche du Public et en est responsable.

L'instructeur peut :

*   Être notifié des nouveaux Dossiers déposés via le Service ainsi que leurs modifications ;
*   Visualiser et télécharger les Dossiers dans leur intégralité ;
*   Prendre en charge et instruire l’ensemble des Dossiers soumis par le Public ;
*   Échanger avec le Public, dans le cadre de l’instruction de sa Démarche ;
*   Faire intervenir des « Experts » sur un Dossier ;
*   Suivre l’évolution du dossier avec des indicateurs de statut ;
*   Télécharger un récapitulatif de l’ensemble des dossiers déposés, sous un format tableur.

L’instructeur s’engage à respecter la confidentialité des données et à ne demander que les informations nécessaires au traitement de la Démarche.

L'instructeur est responsable des personnes invitées en tant qu’Expert et de leurs actions.

##### Expert

L'instructeur peut inviter des Experts afin qu’ils fournissent un avis technique sur un Dossier spécifique. Ils doivent être légalement habilités à connaître des informations auxquelles ils ont accès. Cette invitation est renouvelée à chaque dossier pour lequel un avis est sollicité.

##### Invité

Le Public peut faire intervenir un Invité pour compléter son Dossier. Celui-ci peut :

*   Visualiser le Dossier du Public ;
*   Échanger avec l’ensemble des intervenants par le biais de la messagerie intégrée au Service.

* * *

2\. Traitement des données personnelles
---------------------------------------

### 2.1. Responsabilité du traitement

L’Administration utilisatrice est considérée comme étant responsable des traitements opérés sur les données reçues dans le cadre du Service et, à ce titre, respecte les obligations inhérentes à ce traitement, notamment celles prévues par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés et par le règlement européen nᵒ 2016/679 dit règlement général sur la protection des données.

Elle s’engage à ne pas commercialiser les données reçues, à ne pas les communiquer à des tiers en dehors des cas prévus par la loi et à effectuer toutes formalités préalables obligatoires auprès de la Commission Nationale de l’Informatique et des Libertés.

L’Administration utilisatrice s’engage à mettre en œuvre les mesures de sécurité techniques et organisationnelles nécessaires dans le cadre de sa démarche.

En cas de non-respect, l’ADULLACT se garde le droit de suspendre ou de supprimer la démarche, et tous les dossiers associés.

En utilisant le Service, l’Administration utilisatrice s’engage en outre à respecter un niveau de qualité, et notamment à assurer un service d’aide au Public pour ses Démarches et à traiter les Dossiers dans les meilleurs délais.

Il appartient au responsable de traitement de fournir l’information aux personnes concernées par les opérations de traitement au moment de la collecte des données.

### 2.2. Sous-traitant

De son côté l’ADULLACT est sous-traitant des traitements opérés.

L’ADULLACT s’engage à mettre en œuvre toutes mesures appropriées, afin de protéger les données traitées dans le cadre du Service et notamment celles prévues par la loi n°78-17 du 6 janvier 1978 modifiée, l’ordonnance n° 2005-1516 du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l'application des articles 9, 10 et 12 de l'ordonnance n° 2005-1516 du 8 décembre 2005.

La responsabilité de l’ADULLACT ne s’entend que sur la durée pendant laquelle les données sont effectivement présentes sur le Service.

La ou les finalité(s) du traitement, les données à caractère personnel traitées et les catégories de personnes concernées sont définies et documentées par l’Administration utilisatrice en fonction de la Démarche utilisée.

 Dans ce cadre, l’ADULLACT s’engage à ne traiter les données **que pour la ou les seule(s) finalité(s)** qui fait/font l’objet de la sous-traitance, à **garantir la confidentialité** des données à caractère personnel traitées dans le cadre du présent contrat.

L’ADULLACT veille à ce que les **personnes autorisées à traiter les données à caractère personnel** en vertu du présent contrat :

*   s’engagent à respecter la confidentialité ou soient soumises à une obligation légale appropriée de confidentialité ;
*   reçoivent la formation nécessaire en matière de protection des données à caractère personnel.

L’ADULLACT s’engage à prendre en compte pour chaque outil permettant le fonctionnement du Service les principes de **protection des données dès la conception** et de **protection des données par défaut**.

L'ADULLACT est autorisée à faire appel à un autre sous-traitant pour mener les activités de traitement, dès lors qu’elle s’est assurée du respect par le sous-traitant des obligations et mesures de sécurité auxquelles l'ADULLACT est elle-même tenue en vertu de ce contrat.

Il appartiendra à l’ADULLACT de s’assurer que le sous-traitant ultérieur présente les mêmes garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de manière à ce que le traitement réponde aux exigences du règlement européen sur la protection des données. Si le sous-traitant ultérieur ne remplit pas ses obligations en matière de protection des données, le sous-traitant initial demeure pleinement responsable devant le responsable de traitement de l’exécution par l’autre sous-traitant de ses obligations.

Lorsque les personnes concernées exerceront auprès de l’ADULLACT des demandes d’exercice de leurs droits au regard de la protection de leurs données personnelles, l’ADULLACT adressera ces demandes dès réception à l’Administrateur concerné.

L’ADULLACT notifiera au responsable de traitement toute violation de données à caractère personnel dans un délai maximum de 48 heures après en avoir pris connaissance et ce par tout moyen utile permettant d’assurer une traçabilité raisonnable. Cette notification est accompagnée de toute documentation utile afin de permettre au responsable de traitement, si nécessaire, de notifier cette violation à l’autorité de contrôle compétente.

L’ADULLACT aidera le responsable de traitement pour la réalisation d’analyses d’impact relative à la protection des données, ainsi que pour la réalisation de toute consultation préalable de l’autorité de contrôle. Elle mettra à la disposition du responsable de traitement **la documentation nécessaire pour démontrer le respect de toutes ses obligations** et pour permettre la réalisation d'audits, y compris des inspections, par le responsable du traitement ou un autre auditeur qu'il aurait mandaté, et contribuer à ces audits.

L’ADULLACT tient un registre des traitements, et a désigné un délégué à la protection des données conformément à l’article 37 du règlement européen sur la protection des données. Il est joignable :

*   par voie postale à l’adresse de l’association.
*   par voie électronique à l’adresse [contact-rgpd@adullact.org](mailto:contact-rgpd@adullact.org)

### 2.3. Durée de conservation des données

L’Administrateur détermine la durée de conservation des données de sa Démarche.

Le Service conserve les données uniquement le temps nécessaire à l'instruction.

Une fois la décision rendue, le Dossier est mis à disposition de l’Instructeur et du Public pour téléchargement.

A la date de fin de conservation du dossier fixé par l'Administration, l’ADULLACT supprime le Dossier des bases du Service.

Seule une trace du Dossier est conservée. Celle-ci indique l’adresse électronique de l’usager, le nom de l’Administration chargée de la Démarche, la date de dépôt de la Démarche, la date de décision, le nom de la Démarche. Cette trace est accessible au Public et à l’Instructeur dans leur tableau de bord respectif.

* * *

3\. Engagements de l’ADULLACT envers les Administrations utilisatrices
----------------------------------------------------------------------

L’ADULLACT s’engage à :

*   Vérifier l’éligibilité des Administrations pour lesquelles les Administrateurs agissent ;
*   Assurer la sécurité du Service, notamment en prenant toutes les mesures nécessaires permettant de garantir l’intégrité et la confidentialité des informations traitées ;
*   Mettre en œuvre et opérer le Service conformément aux dispositions légales et réglementaires en vigueur.

### 3.1. Qualité de service

L’ADULLACT se réserve la liberté de suspendre, moyennant un préavis de 48h, le Service pour des raisons de maintenance. En cas d’urgence, cette suspension pourra intervenir sans préavis ou pour tout autre motif jugé nécessaire.

L’indisponibilité du Service ne saurait ouvrir droit à aucune compensation quelle qu’en soit sa nature.

L’ADULLACT assure un support exclusivement sur les parties techniques de l’outil.

L’ADULLACT s’engage à assurer le suivi et l’évaluation de l’utilisation du Service, et à communiquer les résultats obtenus aux différents partenaires.

### 3.2. Responsabilité

L’ADULLACT ne saurait être responsable des conséquences, directes ou indirectes, pouvant découler des erreurs de créations de formulaires, de saisie de coordonnées ou de toute autre information incomplète ou erronée transmise via le Service.

Aucune des parties ne sera responsable vis-à-vis de l'autre partie d'un retard d'exécution ou d'une inexécution en raison de survenance d'un événement en dehors du contrôle des parties qui ne pouvait être raisonnablement prévu lors de l'acceptation des CGU et dont les effets ne peuvent pas être évités par des mesures appropriées.

### 3.3. Force majeure

Le cas de force majeure suspend les obligations de la partie concernée pendant le temps où jouera la force majeure si cet évènement est temporaire. Néanmoins, les parties s'efforceront d'en minimiser dans toute la mesure du possible les conséquences. A défaut, si l'empêchement est définitif, les parties seront libérées de leurs obligations dans les conditions prévues aux articles 1351 et 1351-1 du Code civil.

### 3.4. Suppression de compte

L’ADULLACT s’autorise à suspendre ou révoquer un Administrateur ou un Instructeur et toutes les actions réalisées par eux, si elle estime que leur usage du Service contrevient aux dispositions légales en vigueur ou aux dispositions des CGU, ou si cet usage ne respecte pas les exigences de sécurité ou porte préjudice à son image.

* * *

4\. Droit applicable
--------------------

Conformément à l’[article 17 de la Loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique (LCEN)](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000006421561), l’application est soumise au droit français, ainsi que les présentes CGU.

Toutes difficultés relatives à la validité, l'application ou à l'interprétation des CGU seront soumises, à défaut d'accord amiable, au Tribunal Judiciaire de Paris (France), auquel les Parties attribuent compétence territoriale exclusive, quel que soit le lieu d'exécution ou le domicile du défendeur.

* * *

5\. Évolution des conditions d’utilisation
------------------------------------------

Les termes des présentes CGU peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au Service, de l’évolution de la législation ou pour tout autre motif jugé nécessaire. L’ADULLACT s’engage à avertir les Administrations partenaires de l’entrée en vigueur de nouvelles conditions d’utilisation.

* * *

*   **Conditions Générales pour les administrations**
*   [Conditions Générales d'Utilisation pour les usagers]({{< relref "./121-cgu-pour-les-usagers-du-service-demarches-simplifiees-adullact" >}})
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
*   [Mentions légales](logiciels/119-mentions-legales-demarches-simplifiees-adullact)