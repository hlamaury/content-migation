+++
title = "Mentions légales \"Démarches Simplifiées\" ADULLACT"
description = "Mentions légales de DS"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour objet de faciliter la dématérialisation des démarches administratives par toute administration.

*   [Conditions Générales pour les Administrations]({{< relref "./123-cgu-pour-les-administrations-du-service-demarches-simplifiees-adullact" >}})
*   [Conditions Générales d'Utilisation pour les Usagers]({{< relref "./121-cgu-pour-les-usagers-du-service-demarches-simplifiees-adullact" >}})
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
*   **Mentions légales**

* * *

### Mentions légales du service _demarches.adullact.org_

#### Éditeur

**ADULLACT**  
5 rue du plan du palais  
34000 Montpellier

*   Tel : 04 67 65 05 88
*   Mail : [contact@adullact.org](mailto:contact@adullact.org)
*   RCS : Montpellier 443783170

#### Directeur de la publication

Pascal Kuczynski, Délégué Général de l'**ADULLACT**

#### Prestataire d'hébergement

**SAS OVH**, [ovh.com](https://www.ovh.com)  
2 rue Kellermann  
BP 80157  
59100 Roubaix