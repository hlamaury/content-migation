+++
title = "Présentation de la licence AGPL v3"
description = "Présentation de la licence AGPL v3"
tags = [ "Licence AGPL", "Juridique", "Licence libre" ]
date = "2014-03-31"
+++

Cette licence AGPL v3 publiée le 19 novembre 2007 a été conçue sur la base de la licence GPLv3, dont elle reprend toutes les dispositions, mais avec une modification essentielle. Dans son article 13, l'AGPL v3 complète là où la GPL v3 reste silencieuse, elle oblige la mise à disposition du code source du logiciel modifié lorsque ce dernier est mis à disposition du public par le biais d’un réseau à distance.

### 1\. La licence AGPL v3 est venue combler une lacune de la GPL v3

Elle applique l'obligation de mise à disposition du code source modifié pour les programmes distribués via un réseau en ligne:

La parade de la licence GNU AGPL v3 consiste donc simplement à ajouter à la GPL classique une clause pour les « Interactions à distance » indiquant que « … si vous modifiez le Programme, votre version modifiée doit offrir de manière visible à tous les utilisateurs interagissant à distance grâce à un réseau informatique… la possibilité de recevoir la source correspondante de votre version… gratuitement, grâce aux méthodes standard ou habituelles de copies de logiciel . Les règles établies par la GPLv3 sont ainsi applicables dans le nouveau monde de la fourniture de services d’applications en réseau.

### 2\. Compatibilité de l'AGPL v3 :

Compatible avec la GPL v3 :

La licence GNU AGPL v3 est compatible avec la licence GNU GPL v3, en matière de composition : la licence GNU GPL v3 permet en effet de combiner ou lier un logiciel sous licence GNU GPL v3 avec un second sous licence GNU AGPL v3.

Dans ce cas, l’ensemble du logiciel modifié sera soumis à la licence GNU GPL v3, mais en cas de mise à disposition par le biais d’un réseau à distance, l’obligation de mise à disposition du code source de la licence GNU AGPL v3 devra être respectée.

Autre compatibilité expresse :

Dans son contrat, la licence CeCILL v2.1 dispose clairement être compatible avec la licence AGPL v3. Si du code sous licence AGPL v3 est combiné à du code sous licence CeCILL v2.1, alors le logiciel modifié sera distribué sous licence AGPL v3.

### 3\. Le contenu de la licence AGPL v3 en quelques mots clés :

*   Clause de survie
*   Fort copyleft
*   Langue officielle : anglais
*   Juridiction compétente et droit applicable : non définis, il faut se référer aux règles de droit international privé.

En l'absence de clause attributives de compétence, les règles de droit international privé s'appliquent. Donc en application de la convention de Rome de 1980 sur la loi applicable aux obligations contractuelles, il faut en premier lieu se référer à la loi choisit par les parties (article 3 de la convention de Rome), si aucun choix n'a été effectué par les parties, alors c'est la loi du pays avec lequel le contrat présente les liens les plus étroits (article 4). Cependant si le contrat est conclu avec un consommateur (c'est à dire un étranger à l'activité professionnelle), alors c'est la loi du pays de résidence de celui-ci qui s'applique (article 5). Donc le choix du droit applicable ce fera au cas par cas.

{well}

Pour des explications détaillées sur le contenu de cette licence voir la présentation de la [licence GPL v3]({{< relref "../presentation-licences/31-presentation-de-la-licence-gpl-v3" >}}) qui est identique, sauf différence vu précédemment. 

{/well}