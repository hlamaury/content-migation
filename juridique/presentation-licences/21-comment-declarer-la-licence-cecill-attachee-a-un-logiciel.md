+++
title = "Comment déclarer la licence CeCILL attachée à un logiciel ?"
description = "Comment déclarer la licence CeCILL attachée à un logiciel ?"
tags = [ "Licence CeCILL", "juridique", "licence" ]
date = "2005-10-10"
+++

Une fois la décision prise de quelle licence libre (CeCILL dans cet exemple) sera attachée à un logiciel, que convient-il de faire pour entériner ce choix ?

Le présent texte fournit les informations concernant la [licence CeCILL](component/tags/tag/17-licence-cecill). Le principe reste le même pour les autres licences libres.

L'idée principale consiste à faire le nécessaire pour avertir tous les utilisateurs du logiciel ainsi que ceux qui utiliseront les sources du logiciel de la licence attachée à ce logiciel. Il convient de fournir le texte complet de la licence dans un fichier de type "licence.txt" ou "license.txt" systématiquement livré avec le logiciel. Quel que soit les divers packaging existant (sources, binaires, sources+binaires...), il convient de fournir ce fichier.

On trouvera une copie de l'intégralité du texte à fournir sur le site de la licence CeCILL Il est également préférable de rappeler la licence dans chacun des fichiers sources constituant le logiciel. En effet, la licence s'applique à tous les fichiers sources constituant le logiciel. Quiconque reprenant tout ou partie de ces fichiers doit être informé de la licence attachée à ces fichiers. Les textes insérés sont insérés dans le fichiers sources sous forme de commentaires (selon le langage de programmation).

On notera en particulier les noms des personnes ayant participé à la rédaction des fichiers sources. Cette liste de personnes est importante car chacune de ces personnes devra donner son accord avant de permettre une modification de la licence.

Parmi les information à insérer dans les fichiers sources, on trouve :

*   L'information sur la licence, ainsi que les années indiquant la portée de cette licence dans le temps
*   Le nom du ou des auteurs du fichier source. Il s'agit classiquement de personne(s) physique(s) ; il peut également s'agir de personne(s) morale(s)
*   Le nom du ou des personnes ayant participé à l'évolution ou à la correction de bugs du code contenu dans le fichier source.
*   Éventuellement d'autres informations concernant le projet (site web, @ mail, lieux physiques...)

**Modèle d'insertion :**

_"Version 3.12.8a - 2005/10/08_

_CeCILL Copyright (C) 2003-2005 by François Elie_

_Initiated by François Elie & Pascal Kuczynski_

_followed by ADULLACT technical team_

_Web Site = http://www.adullact.net"_