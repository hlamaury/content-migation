+++
title = "Présentation de la licence GPL v3"
description = "Présentation de la licence GPL v3"
tags = [ "Juridique", "Licence GPL", "Licence libre" ]
date = "2014-03-24"
+++

Cette licence reconnue par la FSF et validée par l'OSI, est une licence à fort copyleft, c'est à dire contaminante. Elle incarne la licence libre depuis sa publication le 29 juin 2007, puisqu'elle garantie dès son préambule la liberté de l'utilisateur quant à l'exécution, le partage, la modification et la distribution des copies modifiées ou non soumis à cette licence. Il est à noter, cependant que seule la version anglaise de cette licence à une valeur juridique, les traductions de cette licence ne sont pas officiellement reconnues.

### 1\. Avant les effets de la licence, l'acceptation :

A partir de l'acceptation de la licence, la durée de protection est de 70 ans post mortem.

Pour certaines utilisations d'un logiciel sous GPLv3 l'acceptation n'est pas nécessaire : l'utilisateur qui reçoit ou exécute le logiciel n'a pas l'obligation d'accepter les termes de la licence. Cependant, s'il modifie et/ou distribue le logiciel alors l'utilisateur reconnaît les termes de la licence et devient licencié, sans quoi il est vu comme un contrefacteur.

### 2\. Loi applicable et juridiction compétente :

Ces deux points ne sont pas précisés, certainement par souci d'exportation au niveau mondial de cette licence. Les auteurs de cette licence n'ont pas souhaité contraindre les utilisateurs à une loi précise ainsi qu'à une juridiction prédéfinie, afin d'éviter les réticences que cela pourrait engendrer et donc fatalement aboutir à une non utilisation de cette licence. Ce souhait d'utilisation au niveau mondial est clairement palpable au travers de l'écriture du texte de la licence en des termes qui se veulent larges.

En pareilles circonstances, il est à se demander comment vont se résoudre les éventuels litiges :

En l'absence de clause attributives de compétence, les règles de droit international privé s'appliquent. Donc en application de la convention de Rome de 1980 sur la loi applicable aux obligations contractuelles, il faut en premier lieu se référer à la loi choisit par les parties (article 3 de la convention de Rome), si aucun choix n'a été effectué par les parties, alors c'est la loi du pays avec lequel le contrat présente les liens les plus étroits (article 4). Cependant si le contrat est conclu avec un consommateur (c'est à dire un étranger à l'activité professionnelle), alors c'est la loi du pays de résidence de celui-ci (article 5). Donc le choix du droit applicable ce fera au cas par cas.

### 3\. Droits et obligations des parties

Obligation de citation : afin de respecter les droits de propriété intellectuelle des différents auteurs, le licencié qui distribue des copies du logiciel modifié ou non doit obligatoirement copier toutes les mentions, il ne doit en aucun cas supprimer une mention des droits de propriété sur le logiciel.

Obligation de traçabilité des contributions : chaque contributeur doit mentionner les modifications qu'il a effectué ainsi que la date et son nom. Cela par protection des auteurs et développeurs successifs, afin que les problèmes ne soient pas attribués de façon erronée aux auteurs des versions précédentes. De plus aucune garantie n'accompagne le logiciel libre.

Afin d'assouplir la licence GPLv3, il est possible d'insérer des « permissions additionnelles » ce sont des termes qui complètent les termes de la licence en stipulant des exceptions à l'une ou plusieurs de ses conditions. Ces termes additionnels doivent être traités comme les termes de la licence et, doivent être respectées sauf s'ils sont contraires à la loi applicable. De plus, lors de la distribution des exemplaires du logiciel, il est possible de supprimer toute permission additionnelle sur cet exemplaire ou sur une partie de celui-ci. Au contraire, des permissions additionnelles peuvent être stipulées, sur une contribution ajoutée par un contributeur qui dispose des droits pour le faire. Les termes additionnels peuvent porter sur :

*   Le refus de toute garantie ou limiter la responsabilité différemment des termes (exonération de garantie et de responsabilité) déjà contenu dans la licence.
*   L'exigence du maintient de certaines mentions spécifiques.
*   L'interdiction d'indication d'origine erronée des contributions.
*   La limitation à des fins publicitaires des noms des concédants ou des auteurs de la contributions.
*   Refuser d'accorder des droits aux termes du droit des marques pour l'utilisation des noms commerciaux, marques commerciales ou marques de services.
*   Exiger que l’indemnisation des concédants et des auteurs de cette contribution par quiconque transmettant la contribution (ou des versions modifiées de celle-ci) avec des acceptations contractuelles de responsabilité au bénéfice du récipiendaire (bénéficiaire), pour toute responsabilité que ces acceptations contractuelles imposent directement à ces concédants et auteurs .

Tous autres termes additionnels non permissifs sont considérés comme des « restrictions supplémentaires », mais ces stipulations peuvent être supprimées par celui qui reçoit de la sorte le programme.

Lors de la transmission d'un exemplaire du code source du logiciel : le concédant a l'obligation de citation, il doit la préciser l'absence de garantie et fournir un exemplaire de licence GPLv3.

Le concédant dispose de diverses méthodes de mise à disposition du code source :

1-Le code source peut être distribué avec le code objet sur le même support physique (durable et habituellement utilisé pour les logiciels).

2-Si seul le code objet est distribué, il faut:

*   L'accompagner d'un engagement écrit qui stipule que le concédant s'engage durant 3 ans à fournir une copie intégrale du code source sur un support physique durable, habituellement utilisé pour l'échange de logiciels ou depuis un serveur réseau sans frais.
*   Offrir un accès équivalent au code source que celui d'accès au code objet via le même emplacement.
*   Il est également possible de distribuer le seul code objet en P2P si un accès simple et effectif au code source est indiqué.

Obligation de non abandon de la liberté des autres : cela passe par le fait que si des obligations sont imposées au licencié et que ces obligations sont en contradiction avec les termes de la licence, ce n'est pas une cause libératoire de la licence. si vous ne pouvez pas transmettre une création régie de façon à satisfaire simultanément vos obligations issues de cette licence et toutes autres obligations pertinentes, alors en conséquence vous ne pouvez pas du toute la transmettre.

Par exemple : si vous acceptez des stipulations qui vous obligent à collecter une redevance pour la transmission subséquente auprès de ceux vers qui vous transmettez le Programme, la seule façon qui puisse vous permettre de satisfaire tant ces stipulations que celles de la licence serait de vous abstenir entièrement de transmettre le Programme.

### 4\. Les compatibilités de la GPLv3 :

Compatibilité particulière entre la GPLv3 et AGPLv3 :

La GPLv3 est expressément compatible par les termes de son contrat de licence avec l'AGPLv3 : compatibilité supérieure de la GPLv3 sur l'AGPLv3 mais sous condition. Dans son article 13 le contrat de licence de la GPLv3 dispose « qu'il est permis de lier ou combiner tout logiciel sous cette licence avec une création licenciée en application d'AGPLv3 en une seule création modifiée qui sera sous licence GPLv3 mais avec la particularité que l'obligation spéciale de l'article 13 de l'AGPLv3 concernant l'interaction au travers d'un réseau s’appliquant à la combinaison en tant que telle. Lorsqu'un autre composant est sous GNU AGPL, les termes de cette dernière s'appliqueront en cas de mise à disposition du logiciel par le biais d'un réseau à distance.

Il est obligatoire d'appliquer la clause n°13 de la licence AGPLv3 (celle rendant la communication du code source d'un logiciel distribué par réseau obligatoire) à un logiciel sous licence GPLv3 qui est issu de la combinaison de code sous AGPLv3 et GPLv3. Ceci afin de pérenniser le logiciel libre, car si la distribution du code n'était pas obligatoire, alors les droits du licencié se verraient restreints et on ne parlerait plus de logiciel libre, il y aurait une privatisation.

Les compatibilités avec les autres licences :

Il n'a pas semblé nécessaire de préciser les autres compatibilités avec les autres licences, car en cas de compatibilité avec d'autres licences, il s'agit d'une compatibilité supérieure de la GPLv3 qui est précisée par les contrats de ces licences lorsque cette compatibilité ne découle pas de la logique.

Actuellement, la GPLv3 est compatible avec certaines licences à fort copyleft comme :

*   la [CeCILL v2]({{< relref "../presentation-licences/29-presentation-de-la-licence-cecill-v2" >}})
*   la [CeCILL v2.1]({{< relref "../presentation-licences/28-presentation-de-la-licence-cecill-v2-1" >}})

En clair : cela signifie qu'un logiciel dérivé issu de composants sous licence GPLv3 et d'autres sous licence CeCILL v2, alors le logiciel modifié devra être sous licence GPLv3. La solution est la même avec la licence CeCILL v2.1.

Fonctionnement avec la GPLv2 :

Si le logiciel sous licence GPLv2 contient la clause « GPL version 2 ou toute version ultérieure » (" version 2 of the GPL or any later version "), alors la version 3 de la GPL est disponible pour tous les utilisateurs, qui pourront se soumettre aux termes de la licence GPLv3.

Cependant, il ne faut pas confondre la mise à jour avec la compatibilité des deux versions. En effet, la version 2 et la version 3 de la GPL ne sont pas compatibles, cela signifie qu'il n'est pas possible de combiner du code sous GPLv2 avec du code sous GPLv3 dans le même programme.

### 5\. La clause spécifique DRM et tivoisation :

C'est une clause spécifique intégrée à la licence GPLv3, c'est une nouveauté vis à vis de l'ancienne version GPLv2. Ceci dans le but de préserver les droits des utilisateurs de logiciels libres. En effet, les systèmes de DRM/MTP (= digital rights management/ mesures techniques de protection) et la tivoïsation sont des dispositifs qui réduisent les possibilités d’utilisation d'un logiciel. Ces systèmes de DRM/MTP ainsi que la tivoisation, faisaient déjà polémique au sein des logiciels propriétaires, puisque ces dispositifs entravaient et entravent toujours la liberté des utilisateurs de pouvoir faire des copies privées. La polémique est d'autant plus vive dans la communauté du libre.

De ce fait la licence GPLv3 a voulu clarifier la situation :

Certains dispositifs sont conçus pour empêcher l’accès des utilisateurs aux fins d’installation ou d’exécution de versions modifiées du logiciel qu’ils contiennent bien que le fabricant le puisse. Ceci est fondamentalement incompatible avec l’objectif de protéger la liberté des utilisateurs de modifier le logiciel.

Par ces termes, la licence GPLv3 interdit fermement la tivoïsation, qui est le fait de restreindre la liberté accordée par la licence du logiciel au travers du matériel nécessaire à l'utilisation du logiciel et qui par la tivoïsation ne permet pas de mettre en œuvre une version modifiée.

Si vous transmettez une création régie, vous renoncez à tout droit d’interdire le contournement des mesures techniques dès lors qu’un tel contournement serait effectué en exerçant des droits issus de cette Licence concernant la création régie, et vous déclarez n’avoir aucune intention de limiter le fonctionnement ou la modification de la création en opposant, à l’encontre des utilisateurs de la création, vos droits ou ceux de tiers d’interdire le contournement de mesures techniques.

Quant aux dispositifs DRM, ce sont des systèmes qui restreignent les libertés de l'utilisateur, en ne lui donnant pas librement accès au code source, et ainsi il ne peut copier ou modifier le code source comme il l'entend et comme lui permet normalement la licence. Si on s'en tient stricto sensu aux termes de la licence GPLv3, on s'aperçoit que ces systèmes DRM ne sont pas littéralement interdits, donc, le distributeur peut en ajouter comme bon lui semble, mais vice versa l'utilisateur doit pouvoir les enlever librement, ce qui garantit ses libertés.

### 6\. Garantie et responsabilité :

Il est prévu dans le contrat de licence de la GPLv3 une exonération totale de garantie et de responsabilité. Mais afin de ne pas être contradictoire avec certaines législations, une souplesse est acceptée, en effet il est prévu que si ces exonérations totales ne peuvent prendre effet dans certaines juridictions en raison de leur incompatibilité avec la législation nationale, les cours de justice qui les examinent doivent appliquer la législation locale qui s'approche le plus d'une exclusion absolue de toute responsabilité, à moins qu'une garantie ou hypothèse de responsabilité supplémentaire accompagne la licence comme il est possible de le faire à titre personnel.

De plus, avec les « permissions additionnelles », il est possible de stipuler des exceptions à l'une ou plusieurs des conditions de la licence. Ces termes additionnels doivent être traités comme les termes de la licence, elles doivent être respectées sauf si elles sont contraires à la loi applicable. Concernant les conditions de garanties et de responsabilité, il est permis de décliner toute garantie ou limiter la responsabilité différemment des termes des articles traitant les conditions d'exonération de garantie et de responsabilité déjà contenu dans la licence.

Enfin par la clause de survie, cela permet à la licence de rester valide même si une de ses clauses est invalide vis à vis du droit applicable. Cette clause sera par la suite interprétée ou modifiée en conformité du droit qui lui est applicable.

### 7\. Articulation avec les brevets :

Le Concédant s'engage à ne pas opposer ses droits conférés par des brevets , ce qui permet au licencié une utilisation de ses droits sur ce logiciel sans risque de poursuites judiciaires dans le cas où il contreviendrait par une utilisation normale du logiciel aux droits d'un breveté. Cette obligation s'impose en cas de cession des brevets en cause.

### 8\. Résiliation du contrat de licence GPLv3 :

Une clause prévoit qu'en cas de manquement à l'une de ses obligations par le licencié, le concédant à la possibilité de résilier le contrat. Cependant, s'il s'agit du premier manquement du licencié à une de ses obligations et que ce dernier cesse ses agissements durant la période de 30 jours suivant la notification qui lui a été faite par le concédant, alors le licencié est automatiquement réinstauré dans ses droits. Cette résiliation n'aura aucun impact sur les licences qui auront été concédées par le licencié démis de ses droits.