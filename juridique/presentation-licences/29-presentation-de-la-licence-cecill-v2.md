+++
title = "Présentation de la licence CeCILL v2"
description = "Présentation de la licence CeCILL v2"
tags = [ "Licence CeCILL", "Juridique", "Licence libre" ]
date = "2014-03-31"
+++

C'est une licence dite à fort copyleft soit non permissive, elle s'est voulue l'homologue français de la GNU GPL s'appliquant même lors de la distribution via un réseau. La CeCILL v2 est donc analogue à l'AGPL v3, elle-même basée sur la GPL v3.

### 1\. Loi applicable et juridiction compétente :

La CeCILL v2 est soumise et conforme au droit français. En cas de conflit, la juridiction compétente est le Tribunal de Grande Instance de Paris qui a compétence exclusive. Ce contrat de licence peut être rédigé en français ainsi qu'en anglais, les deux versions linguistiques ayant même valeur juridique. La rédaction anglaise, ouvre la voie vers une possible internationalisation de la licence et la rédaction française de cette licence la met en conformité avec la loi Toubon qui impose que ce genre de contrat soit rédigé en langue française pour la bonne compréhension et donc pour un consentement non vicié des utilisateurs francophones.

Rappel : Le français est obligatoire pour les contrats auxquels sont partis des personnes morales de droit public ou des personnes privées exécutant une mission de service public sous peine que le contrat soit déclaré nul et également sous peine d'une contravention de 4e classe. Cependant, à ce jour, les licences libres en langue anglaise utilisées sur le territoire français n’ont pas eu (pour des raisons évidentes, dans la pratique) à souffrir d’actions sur le fondement de la contradiction avec la loi Toubon.

### 2\. Compatibilité avec d'autres licences :

Cette licence non permissive impose donc que toute redistribution du logiciel modifié ou non soit faite sous cette même licence CeCILLv2, elle est contaminante, sauf lorsque du code sous licence CeCILL v2 est inclus dans un logiciel GNU GPL et inversement, alors dans ce cas uniquement l'ensemble sera redistribué sous licence GNU GPL. La CeCILL v2 est expressément compatible avec la GPLv3 et la GPLv2.

### 3\. Garantie et responsabilité :

Responsabilité :

L'exonération prévue par la licence CeCILL v2 se veut conforme au droit français et donc on parle de responsabilité limitée. En effet, l'article 8 du contrat de la licence limite la responsabilité du concédant, puisqu'il faut que le licencié subisse un préjudice direct du fait du logiciel, qu'il rapporte la preuve de ce qu'il avance et enfin, que cela soit dans les limites des engagements pris en considération du contrat.

De plus vis à vis du droit des consommateurs, l'article L.132-1 du Code de la consommation déclare nulles les clauses d'exonération totale de responsabilité lorsque le licencié est un utilisateur "consommateur" ou non professionnel et l'article 1386-15 du Code civil qui interdit ces clauses en cas de dommages causés par la défectuosité d'un logiciel à un consommateur ou non professionnel. Donc par cette responsabilité limitée et non totale, cela permet la mise en balance des intérêts des développeurs et des utilisateurs et, la conformité avec le droit français. Enfin, quoi qu'il arrive dans tous les cas de force majeure si une des parties n'a pas pu de ce fait répondre de son obligation, elle ne pourra être tenue pour responsable même si l'autre partie de ce fait a subi un préjudice.

Garantie :

Exonération totale de garantie : le concédant ne s'engage qu'à être de bonne foi et à apporter son aide technique dans le cas où le licencié venait à être assigné en contrefaçon.

Choix d'une plus grande responsabilité ou garantie :

Lors de la distribution du logiciel aucune obligation de maintenance ou d'assistance n'est à la charge du concédant sauf s'il le décide et ceci doit être constaté dans un document séparé du contrat de licence. De plus cette obligation supplémentaire est strictement circonscrite, elle ne concerne que celui qui s'est engagé et non les prochains concédants.

### 4\. Reconnaissance par la FSF mais non validation par l'OSI :

La licence CeCILL v2 est reconnue par la FSF, ce qui signifie qu'elle respecte les libertés fondamentales du logiciel libre qui sont :

1.  La liberté d'exécuter le programme, pour tous les usages.
2.  La liberté d'étudier le fonctionnement du programme, et de l'adapter à ses besoins. Pour ceci l'accès au code source est une condition requise.
3.  La liberté de redistribuer des copies, donc de partager le logiciel avec la communauté, mais sous licence CeCILLv2 !
4.  La liberté d'améliorer le programme et de publier des améliorations, pour en faire profiter toute la communauté. L'accès au code source est donc indispensable pour faciliter l'exercice de cette liberté.

De plus elle respecte les 10 critères de l'OSI mais absence de reconnaissance pour de sa part pour l'instant. Cela n'a pas fondamentalement d'impact la validation de l'OSI tout comme la reconnaissance par la FSF sont un repère dans le monde du logiciel libre, une sorte de label confirmant qu'il s'agit véritablement d'un logiciel libre, mais il peut arriver qu'une licence réponde aux critères du libre, mais sans être labellisée comme en l'espèce par l'OSI.

### 5\. Droits et obligations des parties :

La phase pré contractuelle :

Avant d'accéder au rang de cocontractant, il faut que le futur licencié ait pu avoir connaissance des termes de la licence avant de les accepter, cette charge repose sur le concédant comme nous allons le voir par la suite.

L'utilisateur est considéré comme un cocontractant et par conséquent avoir accepté les termes de la licence, à l'instant où il télécharge le logiciel par tout moyen ou bien lorsqu'il exerce un quelconque droit concédé par le contrat de licence donc qu'il se comporte comme un licencié. Par la suite le contrat produira ses effets pendant la durée légale de protection des droit patrimoniaux qui est de 70 ans post mortem comme en dispose l'article L123-1 Code de la propriété intellectuelle.

Remarque : Il faut que l'utilisateur sache qu'il s'engage et qu'il puisse lire le contenu de la licence. Contraindre l'utilisateur est compliqué, mais un système qui permette de prévenir l'utilisateur qu'il s'apprête à s'engager et l'oblige à accepter les termes de la licence de façon claire et certaine semble obligatoire pour répondre correctement à l'obligation d'information de l'utilisateur. Concrètement lorsque vous mettez à disposition un code source sous licence CeCILL, il faut que celui qui télécharge puisse auparavant avoir accès au contenu du contrat de licence et être averti avant de télécharger de l'engagement que cela implique. En effet, le fait de télécharger signifie que l'utilisateur devient licencié d'après les terme de la licence, son acceptation est considérée acquise (article 3 du contrat de licence).

Les droits :

Le titulaire initial des droits patrimoniaux sur le logiciel initial a droit au respect de ses droits, donc toute utilisation du logiciel initial est soumise au respect des conditions dans lesquelles le titulaire a choisi de diffuser son œuvre et nul autre a la faculté de modifier les conditions de diffusion du logiciel initial.

Les utilisateurs d'un logiciel sous licence CeCILLv2 disposent de la liberté de modifier le code source. La contrepartie à cette liberté, est l'obligation de traçabilité des contributions, ce qui signifie que chaque licencié qui apporte une modification au logiciel a l'obligation d'indiquer son nom sur la portion modifiée. De plus, cette traçabilité est accompagnée de l'interdiction pour les licenciés successifs de modifier ces mentions, en d'autres termes ils ont une obligation de conservation lors des distributions successives.

Enfin une clause résolutoire est comprise dans le contrat de licence, qui permet au concédant en cas de manquement de l'autre partie de pouvoir résilier le contrat mais une notification préalable aura du être faite et restée sans effet, et alors seulement 30 jours après la résiliation pourra être effectuée. (Concrètement cela signifie par exemple que si un utilisateur a modifié le code source et a distribué la version modifiée mais n'a pas donné accès au code source comme il en avait l'obligation d'après les termes de la licence, alors celui qui lui avait distribué le code source non modifié (le concédant) avant de mettre fin au contrat de licence doit lui notifier son manquement/ l'irrespect de son obligation et si 30 jours après cette notification rien n'a changé alors à ce moment là le concédant pourra mettre fin au contrat de plein droit).

Les obligations :

Lors de toute distribution, le licencié est soumis à des obligations, que l'objet de la distribution soit un logiciel modifié ou non. Dans les deux cas, le licencié peut distribuer sous forme de code source ou de code objet, sous réserve d'accompagner d'un exemplaire du contrat et d'un avertissement quant à la restriction de garantie et également de responsabilité du concédant (article 5.3 du contrat de licence CeCILLv2). Cependant, si seul le code objet est redistribué, le concédant doit clairement informer le licencié de la façon dont il peut avoir accès au code source complet. Quand le licencié distribue le logiciel non modifié il doit le faire sous la même licence CeCILLv2. Lorsqu'il s'agit d'un logiciel modifier tout dépendant des modifications soit le logiciel sera sous licence CeCILLv2 soit sous licence GNU GPL (« compatibilité avec d'autres licences »).

Une autre obligation est à la charge du licencié, il s'agit d'une obligation de ne pas faire. En effet, il lui est interdit de porter atteinte aux droits de propriété intellectuelle apposées sur le logiciel, il devra donc les reproduire à l'identique sur les copies du logiciels modifiées ou non.

### 6\. Mise à jour CeCILL v2 pour CeCILLv2.1 :

C'est une clause comprise dans le contrat de CeCILL v2.0, il donc possible de passer à la version CeCILL v2.1 :

Article 12.2 de la CeCILLv2 dispose : « Afin d'en préserver la cohérence, le texte du Contrat est protégé et ne peut être modifié que par les auteurs de la licence, lesquels se réservent le droit de publier périodiquement des mises à jour ou de nouvelles versions du Contrat, qui posséderont chacune un numéro distinct. Ces versions ultérieures seront susceptibles de prendre en compte de nouvelles problématiques rencontrées par les logiciels libres.

Article 12.3 de la CeCILLv2 dispose : "Tout Logiciel diffusé sous une version donnée du Contrat ne pourra faire l'objet d'une diffusion ultérieure que sous la même version du Contrat ou une version postérieure »

Contrat de la licence : [www.cecill.info/licences/Licence\_CeCILL\_V2-fr.html](http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)