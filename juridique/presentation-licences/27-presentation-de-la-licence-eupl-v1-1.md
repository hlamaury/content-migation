+++
title = "Présentation de la licence EUPL v1.1"
description = "Présentation de la licence EUPL v1.1"
tags = [ "Licence EUPL", "Juridique", "Licence libre" ]
date = "2014-03-31"
+++

C'est une licence dite à fort copyleft soit non permissive, elle a été rédigé par la Commission Européenne pour répondre aux difficultés rencontrées dans la mise en œuvre des licences copyleft au niveau européen ( conformité avec les différentes législations nationales) . Cette licence est reconnue par la FSF et approuvée par l'OSI.

### 1\. Acceptation et durée de protection :

Comment accepter ?

La licence peut être acceptée de deux manières :

1.  soit de façon expresse par exemple en cliquant sur une icône « j'accepte » au bas d'une page faisant apparaître le texte de la licence ou de toute autre manière ;
2.  soit l'acception est considérée donnée lorsque l'utilisateur se comporte comme un licencié en exerçant effectivement un droit concédé par la licence.

Le consentement éclairé de l'utilisateur est facilité, puisque le texte de licence est disponible en 22 versions linguistiques différentes ayant toutes la même valeur juridique.

A partir de l'acceptation :

La durée de protection de la licence à partir de l'acceptation par l'utilisateur est la durée légale de protection des droits patrimoniaux de l'auteur, c'est à dire en France toute la vie de l'auteur du logiciel + 70 ans après son décès.

### 2\. Droits et obligations des parties

Obligation de mise à disposition du code source :

Qu'il s'agisse de l'auteur initial ou des contributeurs successifs, ils ont tous l'obligation de mise à disposition du code source du logiciel qu'il soit ou non modifié. Cette obligation vaut pour toutes communications ou distributions. Le code peut être communiqué sous forme de code source ou bien de code objet, mais dans ce dernier cas le concédant (celui qui distribue le logiciel sous la licence) doit fournir en plus une « copie numérique lisible par ordinateur du code source de l’œuvre avec chacune des copies de l’œuvre que le donneur de licences distribue, ou indique dans un avis qui suit la notice des droits d'auteur apposés à l’œuvre, l'endroit où le code source peut être facilement et gratuitement accessible aussi longtemps que le donneur de licence distribue ou communique l’œuvre » article 3 du contrat de licence EUPL.

Les obligations du licencié :

Le respect du contrat : Par la clause résolutoire, si le licencié ne respecte pas l'une des conditions de la licence, cela entraîne la résolution (= l'extinction) de la licence. Cependant, la résolution de cette licence ne met pas fin aux autres licences concédées à d'autres personnes en vertu de cette licence.

L'obligation de citation et de traçabilité des contributions : 1. Le licencié a donc l'obligation de notifier les modifications qu'il effectue ; 2. Le licencié à l'interdiction de modifier, supprimer les mentions des droits de propriété intellectuelle des auteurs successifs, ce qui veut dire qu'il doit faire toutes ses copies avec ces mentions.

### 3\. Choix de distribution du logiciel modifié ou non :

La licence EUPL, est dite à fort copyleft contaminante, ce qui signifie que chaque copie non modifiée sera obligatoirement distribuée sous cette même licence. En ce qui concerne les logiciels dérivés, l'EUPL est également contaminante, donc le logiciel modifié sera également distribué sous cette licence, sauf compatibilité expressément mentionnée dans le contrat.

Concrètement cette licence mentionne être compatible avec :

GPL v2 Open Software License (OSL) 2.1 et 3.0 la Common Public License 1.0 la Eclipse Public License (EPL) 1.0. CeCILL v2

Cette liste de compatibilités expresses permet de guider l'utilisateur dans le choix d'une licence pour son logiciel dérivé. En effet, lorsqu'une personne créée un logiciel modifié à partir de code sous licence EUPL qui est ensuite inclus dans un logiciel sous une des licences citées dans le contrat de licence de l'EUPL ou inversement, alors le logiciel modifié en résultant sera distribué sous la licence mentionnée dans la licence de l'EUPL. Cela permet de résoudre les problèmes de contamination qui pourraient se rencontrer entres différentes licences à fort copyleft.

Pour exemple : si du code d'un logiciel sous EUPL est inséré à logiciel sous GPLv2 ; le résultat sera distribué sous licence GPLv2, parce que d'après la licence de l'EUPL cela est expressément accepté.

### **4\. Exclusion de responsabilité et de garantie :**

Garantie : En sachant qu'il s'agit d'un travail «inachevé, à améliorer », destiné à être poursuivit par des contributeurs successifs, le logiciel est fournit tel quel sans garantie.

Responsabilité : Exonération de responsabilité quasi-totale pour le concédant : « Sauf dans le cas de dommages causés avec intention de nuire ou dans le cas de dommages directement causés à des personnes physiques, le Donneur de licence ne sera en aucun cas responsable d’aucun dommage, quelle qu’en soit la nature, direct ou indirect, matériel ou moral, qui surviendrait de la Licence ou de l’utilisation de l’Œuvre » article 8 du contrat de licence de l'EUPL ».

Contrats additionnels : Malgré l'exonération de garantie et de responsabilité, il n'est pas interdit au donneur de licence de proposer au licencié en contrepartie d'une rémunération distincte, un contrat n'engageant que lui et non les concédants successifs, constituant une garantie ou responsabilité supplémentaire, des services de maintenance technique.

### **5\. Lois et juridictions :**

Lois applicables :

La licence est interprétée selon la loi de l’État membre de l'Union européenne où le donneur de licence réside ou a établi son siège social. Et pour facilité la compatibilité avec toutes les législations de l'Union, il a fallu rendre adaptable/ souple la licence. C'est pour cela que l'EUPL contient une clause de survie, ce qui signifie que si une clause du contrat est invalide, cela n’entraîne pas la nullité de toute la licence et la clause invalide sera interprétée ou modifiée afin de devenir valide au vu du droit applicable.

Juridictions compétentes :

Si le litige a lieu entre la Commission européenne (en tant que donneur de licence) et un licencié, la juridiction compétente est la Cour de Justice des Communautés européennes (CJCE) désormais dénommée Cour de Justice de l'Union européenne (CJUE) et la loi belge sera applicable. Cette loi s'applique aussi au cas où le concédant n'a pas de siège social ou de résidence sur un territoire d'un État membre de l'Union européenne.

Si le litige à lieu entre Parties (en dehors des cas de figure précédents), alors la juridiction compétente est celle du lieu où le donneur de licence réside ou exerce son activité principale.

Voir le [contrat de licence EUPL](https://joinup.ec.europa.eu/system/files/FR/EUPL%20v.1.1%20-%20Licence.pdf) (pdf).