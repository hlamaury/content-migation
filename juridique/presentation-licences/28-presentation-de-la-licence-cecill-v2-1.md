+++
title = "Présentation de la licence CeCILL v2.1"
description = "Présentation de la licence CeCILL v2.1"
tags = [ "Licence CeCILL", "Juridique", "Licence libre" ]
date = "2014-03-31"
+++

La licence CeCILL v2.1 (en date du 21 juin 2013) est la dernière version de la licence CeCILL v2, elles ont le même esprit. Cependant, la version v2.1 est améliorée vis-à-vis de son ancienne version v2. En effet, ont été modifié une condition de distribution ainsi que le champ des compatibilités. La licence CeCILL v2.1 est également reconnue par la FSF et validée par L'OSI à contrario de la licence CeCILL v2.

**Les deux différences majeures par rapport à l'ancienne version sont :**

*   Les conditions de distribution : celui qui distribue le logiciel a l'obligation d'informer l'utilisateur de la manière d'avoir accès au code source complet et cela pendant 3 ans à compter de l'acceptation (consentement au contrat de licence).
*   Le champ des compatibilités, qui a été élargi, puisque la licence CeCILL v2.1 est compatible avec les licences GNU GPL v2 et v3, GNU AGPL v3 ainsi que l'EUPL.

Pour les autres caractéristiques de cette licence se référer aux explication de la licence CeCILL v2, puisqu'en dehors des différences étudiées ci-dessus la CeCILL v2.1 et la CeCILL v2 sont similaires. Le contrat de licence CeCILL v2.1