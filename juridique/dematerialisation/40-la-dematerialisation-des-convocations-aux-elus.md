+++
title = "La dématérialisation des convocations aux élus"
description = "La dématérialisation des convocations aux élus"
tags = [ "Dématérialisation", "i-delibRE" ]
date = "2014-04-03"
+++

Par dématérialisation de la convocation des élus, il est fait référence au format électronique. Cela est désormais prévu par la loi, au niveau communal, départemental et régional. Toutes les questions soulevées précédemment sont désormais résolues par le programme i-délibRE.

Il est conseillé de lire préalablement Initialement des problèmes vis à vis du mode de transmission des documents aux élus étaient rencontrés.

La dématérialisation des convocations aux élus
----------------------------------------------

### Niveau communal :

La loi du 1er janvier 2005 a permis la dématérialisation de la convocation des élus en modifiant l'[article L.2121-10 du Code général des collectivités territoriales](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=1BAE383F37F13104EC7F92476EE8CB46.tpdila07v_2?idArticle=LEGIARTI000031038655&cidTexte=LEGITEXT000006070633&categorieLien=id&dateTexte=) (=CGCT), relatif aux modalités de convocation des conseillers municipaux. Cet article énonce : "Toute convocation est faite par le maire.... Elle est adressée par écrit, sous quelque forme que ce soit, au domicile des conseillers municipaux, sauf s'ils font le choix d'une autre adresse".

Il ressort de ces dispositions que la transmission des convocations des élues peut se faire non seulement sur des supports papiers mais aussi sous forme dématérialisée pour bénéficier des avancées technologiques.

La légalité de la transmission des convocations par voie dématérialisée est ainsi consacrée, toutefois rien de vient encadrer les modalités de ces envois électroniques.

En l'absence de précision de la loi, une réponse ministérielle du 21 mai 2009 est venue rappeler qu'Il est essentiel de permettre à tous les élues communaux d'être convoqués dans les formes qui leur sont accessibles pour assurer leur information. Il revient donc aux maires en accord avec les conseillers municipaux de définir les modalités de convocations. Toutefois il faut bien bien garder à l'esprit que les modalités de la convocation reposent sur le choix du conseiller lui même. La convocation dématérialisée des élues avec leur accord peut donc aussi bien ce faire via l'envoie d'un courrier électronique qu'avec l'aide d'une plateforme de convocation électronique des élues " ex i-délibRE". Dans le cas contraire l'envoie papier reste de rigueur.

A noter que pour se prémunir contre d'éventuelles protestations relatives à la convocation des élues, les logiciels " de convocation électronique des élues ex i-délibRE " apportent une sécurité juridique accrue et garantissent le respect des exigences réglementaires ( garantie de la date d'envoi "horodatage" , traçabilité des convocations).

Selon l'[article L.2121-12 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031069083?isSuggest=true), simultanément à la convocation, doit être adressée aux conseillers municipaux des communes de 3500 habitants et plus, une note explicative de synthèse sur les affaires soumises à délibération. Il ne s'agit pas d'un rapport complet sur ces affaires, mais d'une simple note. L'absence de celle-ci lors de l'envoi de la convocation est un motif d'annulation de la délibération qui serait irrégulière, de même pour les documents annexes que le conseiller doit obligatoirement recevoir avant toute délibération sous peine que celle-ci soit entachée d'illégalité et qu'elle soit annulée.

Cela est source de problème, car il est prévu clairement que la convocation peut être envoyée par tout moyen, mais rien n'est spécifié pour les documents annexes qui devraient également être envoyés au conseiller en même temps que sa convocation et nous l'avons vu si ces documents ne sont pas communiqués correctement alors cela peut être source de nullité de la délibération (Cependant aujourd'hui en pratique, les élus récupèrent les documents annexes dans la salle du conseil ou dans leur casier au sein de la mairie, mais non à leur adresse personnelle comme il est prévu par la loi, donc actuellement par le biais de cette communication des documents annexes en mairie, la délibération pourrait être remise en question). Si on en suit les termes de l'[article L.2121-12 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031069083?isSuggest=true) « note explicative de synthèse sur les affaires soumises à délibération doit être adressée avec la convocation aux membres du conseil municipal... », la note devant être communiquée avec la convocation, elle doit suivre le même sort que cette dernière et donc par conséquent être communiquée par voie électronique.

Une proposition de loi pourrait résoudre le problème si elle est adoptée, puisque son article 13ter prévoit que l'[article L.2121-10 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041410274?isSuggest=true) serait complété par « cette convocation ainsi que les projets de délibération et les pièces annexes peuvent être adressées aux conseillers municipaux par voie électronique avec leur accord ».

En attendant, nous pouvons supposer que la communication des pièces annexes à la convocation, de la même façon que la convocation elle-même, peut être possible avec l'accord des intéressés. Cela peut devenir d'usage en pratique, même si ce n'est pas légalement prévu, le risque est qu'un jour une contestation se soulève et s'appuie sur la décision du Conseil d’État n°290687 du 9 mars 2007 qui a considéré « que la méconnaissance de ces règles est de nature a entaché d'illégalité les délibération prises par le conseil municipal alors même que les conseillers municipaux auraient été présents ou représentés lors de la séance, il ne pourrait en aller différemment que dans le cas où il serait établi que les convocations irrégulières adressées ou distribuées sont effectivement parvenues à leurs destinataires dans le délai légal ». En conséquence via une transmission électronique des documents, à l'appui de cette décision du Conseil d’État, il pourrait y avoir annulation de la délibération, sauf peut-être si le défendeur arrive à rapporter la preuve que les documents sont parvenus au bon destinataire dans les délais.

Là est tout l'apport du programme i-délibRE, puisqu'il garantit la transmission de toutes les informations ainsi que la convocation simultanément et de manière identique (donc plus de problème d'adresse, ni de communication des pièces annexes par pièce jointe ou lien hypertexte). De plus i-délibRE apporte la certitude de la date et de l'heure d'envoi des documents grâce à son système d'horodatage répondant à tous les critères de fiabilité.

EPCI (établissements public de coopération intercommunale) :

D'après les dispositions de l'[article L.5211-1 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031038607/), la solution est identique à celle des communes.

### Niveau départemental :

L'[article L.3121-19 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000027572150?isSuggest=true) contenu dans la partie information au niveau du département dispose : « Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises. Les rapports peuvent être mis à la disposition des conseillers qui le souhaitent par voie électronique de manière sécurisée ; cette mise à disposition fait l'objet d'un avis adressé à chacun de ces conseillers dans les conditions prévues au premier alinéa... »

Concernant les convocations aux conseils généraux, toutes les informations peuvent être communiquées par voie électronique comme le confirme l'[article L.3121-19 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000027572150?isSuggest=true), donc les documents annexes à la convocation, les notes explicative de l'ordre du jour etc., mais concernant le sort de la convocation cet article reste silencieux, cependant étant dans la section « information », en prenant ce terme au sens large, la convocation peut être considérée comme une information sur la tenue d'une délibération et sur l'invitation a y participer pour le conseiller.

La seule contrainte imposée par cet article, est celle du délai de douze jours pour de communiquer les informations avant la délibération, dans l'optique où il faudrait en rapporter la preuve, un système d'horodatage est indispensable.

### Niveau régional :

La solution est identique à celle vue au niveau départemental. Ici on se référera à l'[article L.4132-18 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000022496357?isSuggest=true) qui dispose « Douze jours au moins avant la réunion du conseil régional, le président adresse aux conseillers régionaux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises... » L'alinéa 1er des articles L.3121-19 et L.4132-18 CGCT ont été rédigés dans le même esprit, dans des termes identiques transposés pour l'un aux conseillers généraux et pour l'autre aux conseillers régionaux, ici se situe la seule nuance.

Ce qui signifie comme en témoigne les termes « sous quelque forme que ce soit » que les informations peuvent être transmises aux conseillers par voie électronique. La seule contrainte imposée est celle du délai de douze jours comme au niveau départemental. Afin de justifier être dans les temps, un marquage temporel est nécessaire d'où l'utilité de l'horodatage.

Concernant le sort de la convocation aux conseils régionaux, on se situe dans le même cas identique que vu au niveau départemental, c'est-à-dire, que rien n'est explicitement prévu pour la convocation des conseillers régionaux, mais l'article concerne les informations au sens large qui doivent être communiquées aux conseillers, la convocation peut être vue comme une information et également être dématérialisée.

Dans le cadre départemental ainsi que régional, le programme i-délibRE permet de répondre à toutes les conditions énoncées par les articles [L.3121-19](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000027572150?isSuggest=true) et [L.4132-18 CGCT](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000022496357?isSuggest=true), c'est à dire un moyen de communication des informations et dans le délais imparti qui est de douze jours avant la tenue d'un conseil. En effet, par le biais d'i-delibRE, toutes les informations sont communiquées simultanément de manière sécurisée, datée et nominative au conseiller.