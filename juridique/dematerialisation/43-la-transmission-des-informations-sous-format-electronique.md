+++
title = "La transmission des informations sous format électronique"
description = "La transmission des informations sous format électronique"
tags = [ "Juridique" ]
date = "2014-04-02"
+++

**Généralités sur les liens hypertextes et les pièces jointes**

Une pièce jointe est un fichier informatique qui est encapsulé dans un courrier électronique. Si elle contient une œuvre, des informations etc., protégées par un droit de propriété intellectuelle, alors il peut y avoir atteinte à ces droits, si l'autorisation du titulaire n'a pas été obtenue préalablement. Un lien hypertexte quant à lui est un mot ou ensemble de mots qui par un simple clic permet sur une page web ou un e-mail d’accéder à une autre partie de la page, à une autre page ou à un autre site.

Certains problèmes peuvent se rencontrer suite à ce mode de transmission de l'information, tel par exemple l'accès à un contenu protégé qui ne serait pas autorisé à des personnes extérieures au site d'origine de l'information qui ne serait donc pas en libre accès sur la toile.


Avant la lecture de ce billet, il est conseillé de prendre connaissance de [La question de la preuve de la date pour les convocations des élus](index.php/juridique/42-la-question-de-la-preuve-de-la-date-pour-les-convocations-des-elus).


**1\. Transmission des informations par pièce jointe :**

Une pièce jointe par définition est un fichier informatique qui est encapsulé dans un courrier électronique. Si elle contient une œuvre, des informations, etc, protégées par un droit de la propriété intellectuelle, alors il peut y avoir atteinte à ces droits, si l'autorisation du titulaire n'a pas été obtenue préalablement. Mais dans le cas des pièces jointes contrairement aux liens hypertextes, il est peut probable de rencontrer des problèmes liés à la communication au public des informations, puisqu'en général, le courriel contenant la pièce jointe est destiné à un nombre restreint/limité de destinataires.

**2\. Transmission des informations par un lien hypertexte :**

Les différents types de liens hypertextes :

Lien interne : la page source et la page cible appartiennent au même site. (pas de problème dans ce cas)

Lien externe : la page source et la page cible n'appartiennent pas au même site.

Dans cette catégorie (lien externe) différents problèmes peuvent se rencontrer :

*   la responsabilité de l'éditeur du site peut être engagée si un des liens donne accès à un contenu illicite;
*   la responsabilité de l'éditeur du site source peut être engagée par l'éditeur du site cible s'il y a violation de son droit de paternité

Dans ce cas il est préférable d'éviter :

les « liens profonds » qui dirigent vers des pages secondaires (et non vers la page d'accueil du site cible, comme avec les « liens simples »), encore que... ((la JP a considérée que de tels liens hypertextes profonds étaient parfaitement licites : il n'existe aucune obligation légale de ne proposer que des liens hypertextes dirigeant l'internaute vers la page d'accueil.\[...\] Précisons toutefois qu'il semble nécessaire que le droit de paternité éventuel de l'auteur de la page web vers laquelle le lien hypertexte redirige l'internaute soit respecté)). les framing, qui font apparaître le contenu du site cible à l'intérieur d'une fenêtre du site source. Les liens automatiques, qui se déclenchent automatiquement à l'affichage d'une page web, indépendamment d'une quelconque action de l'utilisateur.

Lien hypertexte nouvelle communication au public ? (analyse de l'arrêt de la CJUE du 13 février 2014)

Dans son arrêt du 13 février 2014, la CJUE (Cour de justice de l'union européenne) a jugé que l'insertion sur un site web d'un lien hypertexte menant à une œuvre protégée ne nécessite pas d'autorisation préalable de la part de l'auteur de cette œuvre. Mais attention cette réponse n'est pas aussi simple qu'elle en a l'air, pour bien la comprendre il faut décortiquer le raisonnement de la CJUE.

Selon l'article 3§1 de la Directive 2001/29/CE, tout acte de communication au public d'une œuvre doit être préalablement autorisée par l'auteur de celle-ci. Pour la CJUE : le fait de fournir des liens cliquables vers des œuvres protégées doit être qualifié de « mise à disposition » et, par conséquent, d' « acte de communication ». Si l'on s'arrêtait à ce stade, il faudrait donc recueillir le consentement de l'auteur d'une œuvre pour y donner accès via un lien hypertexte. La Cour ajoute ensuite que l'autorisation de l'auteur est nécessaire uniquement dans le cas ou l'acte de communication vise un public nouveau par rapport à celui qui était initialement visé.

Mais la CJUE considère que si une œuvre est en libre accès sur internet, alors son public potentiel est l'ensemble des internautes, donc les personnes ayant accès à l’œuvre via un lien hypertexte ne sont pas considérées comme un nouveau public. Donc les internautes sont libres de créer des liens sur la toile (internet) à condition tout de même que le lien qui renvoie vers une œuvre hébergée par un site, ne le soit pas par un site dont l'accès est restreint (par exemple uniquement accessible aux abonnés de ce site), car dans ce cas il n'y a pas de dispense d'autorisation qui fonctionne, celui qui créer le lien vers l’œuvre doit demander l'autorisation à l'éditeur du site cible.

Conséquences :

Du coté des « lieurs », la décision de la CJUE est une bonne nouvelle puisque synonyme de liberté quasi-totale. Attention tout de même à ne pas renvoyer vers un site limité d'accès ou vers un site avec des contenus violant les droits de propriété intellectuelle d'un tiers. (Mais ce cas de figure n'a pas encore été tranchée par la justice, donc l'incertitude plane).

La notion de communication au public :

A l'occasion de deux arrêts en date du 15 mars 2012, la CJUE a définit la notion de ''communication au public'' en dégageant les critères à remplir. La première affaire concerne un dentiste, assigné devant la juridiction italienne pour avoir diffusé de la musique d'ambiance protégée par le droit d'auteur. La Cour italienne a demandé, par voie de question préjudicielle, si la diffusion dans un cabinet dentaire devait être considérée comme une communication au public. Non répond la CJUE, eu égard au nombre de personnes destinataires potentielles, au fait qu'il s'agit d'un ensemble déterminé (la clientèle du cabinet) et à l'absence de caractère lucratif.

Donc pour qu'il y ait communication au public, un nombre substantiel de personnes doit potentiellement avoir accès à l'information/ à l’œuvre et il faut que cette communication ait un but lucratif. Même si en l'espèce, le litige n'était pas en version numérique, les critères relevés peuvent être appliqués au format électronique.


**Conclusion :**On peut considérer qu'il y a communication au public d'une œuvre par le biais d'un lien hypertexte si un grand nombre de personne (l'ensemble des internautes) peut y avoir accès et cela dans un but lucratif (exemple apporter une plus-value au site source).

