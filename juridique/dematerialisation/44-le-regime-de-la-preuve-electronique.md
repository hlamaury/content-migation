+++
title = "Le régime de la preuve électronique"
description = "Le régime de la preuve électronique"
tags = [ "juridique", "preuve électronique"]
date = "2014-03-28"
+++

Avant de se pencher sur la valeur probatoire des preuves électroniques, il est important de connaître les bases de droit commun du régime de la preuve. Il faut savoir que les preuves sont généralement sous-divisées en deux catégories : les preuves parfaites et les preuves imparfaites, ces dernières n'étant pas recevables dans toutes les situations. De cette distinction, il faut tirer les conséquences et faire le parallèle avec les preuves électroniques.

Une introduction générale sur le régime de la preuve est nécessaire afin de bien comprendre le fonctionnement et la transposition au niveau électronique.

### 1\. Généralités de droit commun sur la preuve :

Le régime de la preuve est très encadrée. On subdivise souvent entre preuve parfaite et preuve imparfaite, les deux n'aillant pas la même force probante devant le juge judiciaire et n'étant pas recevables dans tous les cas.

En quelques mots, les **faits juridiques** (= événements voulus ou non, susceptible de produire des effets juridiques ; exemple la naissance) peuvent être prouvés par tout moyen, c'est à dire soit par l'apport d'une preuve parfaite qui s'imposera au juge qui n'aura aucun pouvoir d'appréciation et/ou par une preuve imparfaite et dans ce cas le juge a un large pouvoir d'appréciation, il peut rejeter la preuve.

Concernant les **actes juridiques** (= qui sont la manifestation de volontés d'une ou plusieurs personnes destinée à produire des effets juridiques, exemple lorsque l'on achète quelque chose, un contrat de vente se forme, c'est un acte juridique), au niveau de la preuve, si la matière du litige est inférieure à 1500€ alors la preuve peut être rapportée par tout moyen (sachant que la preuve parfaite est plus « forte » que la preuve imparfaite, comme vu dans le cas des faits juridiques). Si par contre la matière du litige est supérieure à 1500€, alors seule des preuves parfaites sont acceptées. Exception pour les litiges commerciaux dont la preuve peut être apportée par tout moyen et ce même si le montant de la matière litigieuse dépasse 1500€.

La règle de la preuve écrite ne s'applique qu'aux parties au contrat et non aux tiers qui voudraient prouver l'existence, le contenu de l'acte ou faire la preuve contraire. Cette limitation du champ d'application de la règle s'explique car on ne pourrait si ce n'est injustement et de façon déséquilibrée imposer à un tiers qui n'a pas eu l'occasion de se ménager des preuves de fournir une preuve écrite de ce qu'il avance. C'est pour cela que le tiers à l'acte peut rapporter des preuves par tout moyen, bien sûr le juge aura la liberté de les retenir ou non en fonction de leur pertinence.

Cependant cette obligation des parties de recourir à des preuves parfaites connaît des exceptions (articles 1347 et 1348 du Code civil) : la loi autorise la preuve des contrats par témoins, présomptions ou indices pour compléter un commencement de preuve par écrit ; pour palier à une impossibilité de fournir une preuve écrite (impossibilité matérielle ou morale) ; pour palier à une impossibilité de fournir le document original (exemple cas de force majeure), alors une « copie fidèle et durable » est acceptée.

### 2\. Valeur probatoire d'un document électronique :

Concernant la preuve sous forme électronique, la loi du 13 mars 2000 reconnaît la même **valeur probatoire** à l'écrit électronique qu'à l'écrit sur support papier sous les mêmes conditions, c'est-à-dire que l'écrit électronique peut avoir valeur d'acte authentique s'il est dresser par un officier d’État civil (par exemple un notaire), il peut être reconnu comme un acte sous seing privé s'il est signé par les parties via une signature électronique fiable.

D'autres éléments tels que la date et la lettre recommandée ont leurs homologues électroniques, nécessaires dans certaines situations pour leur valeur probatoire.


**Conclusion** : L'ère actuelle tend à la numérisation de toutes les situations et le droits se doit d'évoluer en ce sens. À chaque problème une réponse adéquate doit être apportée.
