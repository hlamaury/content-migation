+++
title = "La lettre recommandée électronique"
description = "<p style=\"text-align: justify;\">D\"après l\"article 1369-8 du Code civil, la lettre recommandée électronique à la même valeur qu\"une lettre recommandée avec avis de réception classique mais uniquement dans le cadre de la conclusion et l\"exécution des contrats, c\"est à dire pour l\"envoi d\"un contrat signé par les parties et pour les courriers notifiant des manquements à l\"exécution du contrat ou des modifications de ce dernier. Selon que le destinataire de cette lettre est ou non un professionnel des conditions spécifiques sont à respectées, comme par exemple l\"accord préalable du destinataire particulier, sans quoi une lettre recommandée classique devra lui être envoyée à la place.</p>"
tags = [  ]
type = "blog"
date = "2014-04-07"
author =  "beatrice"
menu = "juridique"
+++

D'après l'article 1369-8 du Code civil, la lettre recommandée électronique à la même valeur qu'une lettre recommandée avec avis de réception classique mais uniquement dans le cadre de la conclusion et l'exécution des contrats, c'est à dire pour l'envoi d'un contrat signé par les parties et pour les courriers notifiant des manquements à l'exécution du contrat ou des modifications de ce dernier. Selon que le destinataire de cette lettre est ou non un professionnel des conditions spécifiques sont à respectées, comme par exemple l'accord préalable du destinataire particulier, sans quoi une lettre recommandée classique devra lui être envoyée à la place.


 Avant la lecture de ce billet, il est conseillé d'avoir pris connaissance au préalable du [Régime de la preuve version électronique]({{< relref "./44-le-regime-de-la-preuve-electronique" >}}).


**La lettre recommandée électronique**

L'article 1369-8 du Code civil dispose : « Une lettre recommandée relative à la conclusion ou à l'exécution d'un contrat peut être envoyée par courrier électronique à condition que ce courrier soit acheminé par un tiers selon un procédé permettant d'identifier le tiers, de désigner l'expéditeur, de garantir l'identité du destinataire et d'établir si la lettre a été remise ou non au destinataire.

Le contenu de cette lettre, au choix de l'expéditeur, peut être imprimé par le tiers sur papier pour être distribué au destinataire ou peut être adressé à celui-ci par voie électronique. Dans ce dernier cas, si le destinataire n'est pas un professionnel, il doit avoir demandé l'envoi par ce moyen ou en avoir accepté l'usage au cours d'échanges antérieurs. Lorsque l'apposition de la date d'expédition ou de réception résulte d'un procédé électronique, la fiabilité de celui-ci est présumée, jusqu'à preuve contraire, s'il satisfait à des exigences fixées par un décret en Conseil d’État.

Un avis de réception peut être adressé à l'expéditeur par voie électronique ou par tout autre dispositif lui permettant de le conserver. Les modalités d'application du présent article sont fixées par décret en Conseil d’État. 

L'article 1369-8 du Code civil est clair, la lettre recommandée électronique à **la même valeur** qu'une lettre recommandée avec avis de réception classique mais uniquement dans le cadre de la conclusion et l'exécution des contrats, c'est à dire pour l'envoi d'un contrat signé par les parties et pour les courriers notifiant des manquements à l'exécution du contrat ou des modifications de ce dernier.

Cette équivalence juridique entre lettre recommandée avec accusé de réception classique sous format papier et lettre recommandée électronique ne vaut que pour la conclusion et l'exécution d'un contrat et **sous certaines conditions** :

*   le tiers chargé de l'acheminement doit garantir l'identité du destinataire et de l'expéditeur ;
*   si le destinataire n'est pas un professionnel, son accord préalable doit être obtenu pour recevoir un recommandé sous forme électronique ;
*   les dates d'expédition et de réception doivent être garanties et vérifiables ;
*   un avis de réception doit être adressé à l'expéditeur par voie électronique ou par tout autre dispositif lui permettant de le conserver

Le tiers chargé de l'acheminement de la lettre recommandée électronique doit conserver ces informations **pendant 1 an**, ainsi que :

*   le document original électronique
*   et son empreinte informatique

L'expéditeur doit pouvoir y accéder pendant 1 an.

Concernant l'accord préalable du destinataire non professionnel, le tiers qui est chargé de l'acheminement doit lui demander par courrier électronique en l'informant qu'un recommandé électronique va lui être envoyé, au lendemain de ce mail informatif, le destinataire dispose de 15 jours pour accepter ou refuser. En cas de refus, le tiers doit alors convertir le document électronique en version papier et faire remettre la lettre imprimée à l'adresse du destinataire sous forme d'un courrier recommandé classique.

D'après le décret du 2 février 2011 de nombreuses zones d'ombres subsistent, donc si rien n'est précisé explicitement la règle reste le recommandé classique. Donc la lettre recommandée électronique ne peut être valablement utilisée pour mettre fin à un contrat. De même elle ne peut être valablement utilisée pour les documents administratifs.

De plus, le décret ne précise pas comment traduire l'absence de réponse du destinataire concernant son accord pour recevoir une lettre recommandée électronique, cela vaut-il acceptation ou refus ? Sans réponse explicite, il est plus judicieux de considérer ce silence comme un refus et non comme une acceptation tacite ; d'un point de vu simplement logique il est possible que l'adresse électronique soit fausse ou que le destinataire du courrier ne consulte pas régulièrement ses mails, dans le doute il est préférable d'envoyer une lettre recommandée sous format papier.

Il est a préciser qu'en dehors des cas prévus pour l'envoi d'une lettre recommandée électronique, c'est à dire dans le cadre des contrats, cela ne signifie pas que la lettre recommandée électronique est inutile, simplement elle n'aura pas la valeur d'un recommandé à part entière comme son analogue papier. On ne va pas la considérer en son entier comme preuve mais sera détaillée pour chacune des informations qu'elle apporte, on regardera indépendamment la date et l'heure, l'identité de l'expéditeur et du destinataire, donc la lettre recommandée électronique est très utile dans tous les cas pour les informations/les preuves qu'elle contient, en fonction de la sécurité des informations qu'elle contient bien entendu.