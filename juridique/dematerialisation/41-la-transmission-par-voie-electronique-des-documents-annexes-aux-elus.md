+++
title = "La transmission par voie électronique des documents annexes aux élus"
description = "La transmission par voie électronique des documents annexes aux élus"
tags = [ "i-delibRE", "dématérialisation", "juridique" ]
date = "2014-04-02"
+++

L'utilisation du programme [i-delibRE](index.php/i-delibre) résout le problème de la communication des informations aux élus préalablement aux délibérations. En effet, ce programme permet d'envoyer simultanément tous les documents à l'élu, qui par définition en installant puis en utilisant ce programme accepte de recevoir les informations préalables à une délibération par ce biais. Mais initialement, en dehors de l'utilisation d'i-delibRE, le problème du mode de transmission des informations à l'élu était récurrent. Fréquemment, la question de ''la communication par pièce jointe ou lien hypertexte est-elle légalement possible ?'' revenait.

 Effectivement, la transmission par pièces jointes des documents préalables à la délibération ne pose pas de grands problèmes puisque par définition, la pièce jointe est incluse dans le courriel destiné à l'élu donc par le même mode de transmission et à la même adresse. Le problème se pose pour le lien hypertexte, qui lui renvoie vers un site donc une autre adresse et donc est un autre moyen de communication.

Préalablement à la lecture de ce billet, il est conseillé de lire [La transmission des informations sous format électronique](index.php/juridique/43-la-transmission-des-informations-sous-format-electronique) (généralités) et [La question de la preuve de la date pour les convocations des élus](index.php/juridique/42-la-question-de-la-preuve-de-la-date-pour-les-convocations-des-elus).


### La transmission des informations appliquée aux convocations

#### Au niveau communal pour les convocations des élus :

Concernant le mode de transmission des informations, il est acquis que la convocation peut être communiquée par voie électronique aux conseillers avec leur accord, quant aux documents supplémentaires qui sont à communiquer avec la convocation, ils devraient logiquement suivre le sort de la convocation. Donc si la convocation est transmise par voie électronique, il en sera de même pour les documents annexes, mais une question peut alors être soulevée : La transmission des informations doit-elle être faite par pièce jointe ou lien hypertexte ?

Le problème du format pièce jointe ou lien hypertexte ressort :

Une pièce jointe par définition est un fichier informatique qui est encapsulé dans un courrier électronique, donc ici pas de problème majeur si les documents annexes sont envoyées sous forme de pièces jointes simultanément à la convocation, c'est-à-dire via le même courrier pas de problème au niveau de l'adresse choisie par le conseiller. Le problème intervient pour les liens hypertextes, la question à se poser est : Est-ce un moyen de communication s'apparentant à la même adresse malgré le fait que ce lien renvoie à une page extérieure ?

**Lorsque les conseillers doivent prendre connaissance des documents annexes en cliquant sur un lien hypertexte contenu dans le même envoi électronique que la convocation, cela devrait pouvoir être considéré comme envoyé à la même adresse que la convocation sous certaines conditions :**

*   que l'accès au lien hypertexte soit restreint et sécurisé au seul conseiller auquel il est destiné, le lien pour accéder aux documents est porté à la connaissance du conseiller simultanément sur le même support que la convocation.
*   que l'accès aux documents annexes via le lien hypertexte ne soit pas plus contraignant qu'un envoi par pièce jointe, ce qui implique que les mesures nécessaires pour garantir d'une part la facilité d'utilisation par les conseillers (l'accès aux pièces extranet ne doit pas être plus complexe que l'ouverture de pièces jointes au courriel) et, d'autre part, le bon fonctionnement technique du dispositif et la disponibilité des documents
*   en cas de contestation, la preuve de l'envoi des documents sera facilitée par les moyens de sécurité appliqués à l'envoi de la convocation, car étant sur le même support il sera aisé de prouver l'envoi, la date, le contenu et l'identité du destinataire puisqu'un seul envoi aura suffit à communiquer toutes les pièces en même temps, même si pour prendre connaissance des pièces annexes il aura fallu cliquer sur un lien intégré au courriel. Cependant, il sera plus difficile de prouver que le conseiller ait véritablement pris connaissance du contenu vers lequel renvoi le lien, mais il en est de même pour tout autre support, personne ne peut être certain que le conseiller ait eu la conscience professionnelle de prendre connaissance des documents annexes, cela reste un cas de conscience personnel dont un tiers n'a pas à rapporter la preuve, ce qui est impossible.

#### Aux niveaux départemental et régional pour les convocations des élus :

La transmission des informations peut être faite par tout moyen comme le précise les articles L.3121-19 et L.4132-18 CGCT. Sans autre précision il n'y a pas lieu de distinguer la où la loi ne le fait pas, donc pas de précision sur l'accord préalable des conseillers, encore moins sur celle d'une adresse précise où envoyer les documents, donc dans ce cas il n'y a pas lieu de distinguer envoi via pièce jointe ou lien hypertexte puisque tout moyen de communication est possible !