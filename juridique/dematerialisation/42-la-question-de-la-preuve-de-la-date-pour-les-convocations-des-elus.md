+++
title = "La question de la preuve de la date pour les convocations des élus"
description = "La question de la preuve de la date pour les convocations des élus"
tags = [ "Juridique", "Horodatage" ]
date = "2014-03-27"
+++

La communication de la convocation ainsi que des pièces annexes aux élus préalablement à toute délibération doit se faire dans un délai prédéfini par les textes de lois. Si ce délai n'est pas respecté, la délibération encoure la nullité pour irrégularité. Afin de palier à ce risque, un moyen efficace permettant d'apporter la preuve du respect des délais a été mis en place, grâce à l'horodatage qui rend possible l'ajout d'un indicateur temporel lors de chaque communication de documents aux conseillers. Cela est plausible grâce au programme i-delibre comprenant ce marquage lors de la transmission des documents.

Avant la lecture de ce billet, la lecture préalable de [L'horodatage (généralités)]({{< relref "./46-l-horodatage" >}}) est nécessaire.

#### L'horodatage appliqué au cas des convocations des élus

Que ce soit au niveau communal, départementale ou régional, un indicateur temporel est indispensable pour la computation des délais qui est essentielle afin que les délibérations soient régulières. Afin d'apporter la preuve de la date et de l'heure, un système d'horodatage est nécessaire. Par ce mécanisme, le document sur lequel le jeton d'horodatage est apposé, permet de certifier qu'il n'a pas été modifié et de dater le document pour garantir son existence et son intégrité.

L'horodatage est souvent utile pour prouver : la non altération du document, c'est à dire qu'il n'a pas été modifié depuis qu'il a été horodaté; le respect des délais légaux ou preuve d'antériorité : date de l'horodatage faisant foi; la traçabilité des actions.

**Problème commun aux trois niveaux**

Le problème commun qui ressort, est celui de la preuve du respect des délais qui a été résolu par le logiciel i-delibRE qui est le porte-document nomade des élus pour le suivi des séances délibérantes de la collectivité.

En l'espèce, même si l’horodatage ne peut pas bénéficier directement de la présomption de fiabilité, car le module d'horodatage n'a pas été certifié conforme par le Premier ministre (article 5 Décret du 20 avril 2011) donc en découle logiquement que le prestataire de service d'horodatage électronique ne peut être certifié conforme sans que le soit préalablement le module.

En cas de litige la preuve de la fiabilité de l'horodatage pourra être rapportée, car tous les critères demandés pour cela sont réunis, ils n'ont simplement pas été vérifiés par une autorité de certification, mais cela ne remet pas en cause leur validité, qui lors de l'instruction de l'affaire sera établie sans équivoque. De plus en cas de contestation, l'affaire sera portée au juge administratif. Devant cette juridiction comme nous allons l'aborder ci-après, la procédure est moins réglementée et en quelque sorte plus flexible que devant les juridictions civiles.

**S'il y a mise en cause d'une délibération, il appartiendra au juge administratif de statuer** 

A la différence de la procédure civile, la procédure administrative permet la liberté de la preuve, c'est-à-dire que toutes les preuves sont recevables, il n'y a pas de hiérarchie, le but est d'emporter la conviction du juge. C'est une procédure inquisitoire, ce qui signifie que la maîtrise du procès est confiée au juge qui joue un rôle actif. En plus des éléments que les parties vont lui apporter, le juge pourra rechercher des éléments de preuve lui-même afin de fonder sa propre opinion. Dans ce système de procédure inquisitoire, c'est le juge qui dirige le procès, qui recherche les preuves et qui apprécie, en dernier lieu, souverainement, la force probante des éléments de preuve.

#### Des exemples de jurisprudences

« Le défaut de mentions de l'ordre du jour entraîne la nullité des délibérations prises au cours de la séance consécutives. » Tribunal administratif de Dijon, 29 janvier 1991, Mathus. « La convocation doit être accompagnée d'une note explicative de synthèse sur les affaires soumises à délibération. En l'absence d'une telle note (ou tout documents équivalent permettant aux élus de disposer d'une information répondant aux exigences de la loi \_ Conseil d’État, 12 juillet 1995, Commune de Fontenay-le-Fleury), les délibérations adoptées suite à la convocation seront irrégulières. « Contestation des délibérations du conseil municipal \[...\]

1/ Antérieurement à la loi du 2 mars 1982, une délibération même très ancienne et remontant éventuellement à plusieurs dizaines d'années, pouvait être attaquée à tout moment. La loi de décentralisation a mis fin à cette pratique. Une délibération doit être attaquée dans le délai de 2 mois à compter de son affichage en mairie, sous réserve que cet affichage puisse être attesté par le maire si son existence est contestée et à condition que celui qui l'attaque n'en ait pas eu connaissance par une autre voie que l'affichage \[…\]

2/ Un conseiller municipal est toujours réputé avoir eu connaissance de la délibération à la date à laquelle s'est tenue la séance du conseil municipal :

*   s'il a participé à cette séance et même s'il allègue n'avoir pas eu d'information suffisante sur les questions traitées. » Conseil d’État, 27 octobre 1989, de Peretti.
*   s'il a été régulièrement convoqué, le délai dont dispose un membre du conseil municipal pour attaquer une délibération de ce conseil court de la date de la séance même s'il n'y a pas assisté . Conseil d’État, 24 mai 1995, ville de Meudon. »