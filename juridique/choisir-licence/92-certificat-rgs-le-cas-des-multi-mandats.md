+++
title = "Certificat RGS** : le cas des multi-mandats"
description = "Certificat RGS** : le cas des multi-mandats"
tags = [ "Certificat RGS" ]
date = "2015-03-23"
+++

Doit-on avoir autant de certificats RGS que de mandats ou d'employeurs, ou peut-on utiliser une seule et même signature ?

Le document ci-dessous précise :

"En outre, le caractère « multi-rôles » ou « multi-qualités » des certificats d’authentification et/ou de signature, par nature nominatifs, est accepté pour autant que l’**entité émettrice est toujours clairement identifiée** : par exemple, un maire peut signer avec le même certificat en tant que maire, président du centre communal d’action sociale de sa commune et président d’un établissement public de coopération intercommunale. De même, il est possible à un secrétaire de mairie employé par plusieurs communes en temps partagé d’utiliser **un seul certificat nominatif** pour adresser les actes de ses différents employeurs sur le système d’information @ctes."

Un seul certificat RGS\*\* est donc admis lorsque l'émetteur cumule plusieurs fonctions ou employeurs.

**Voir le document :** [Authentification des émetteurs signature actes](images/sampledata/FAQ/Juridique/20140806_authentification_emetteurs_signature_actes.pdf) (pdf)