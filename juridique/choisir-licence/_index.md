+++
title = "Choisir sa ou ses licence.s"
tags = [ "Juridique", "Licence libre", "Free Software Foundation", "OSI" ]
date = "2014-03-24"
+++

Dans le monde du libre, il existe d’innombrable licences libres. Une licence permet de gérer les droits patrimoniaux du titulaire et de définir l'étendue des droits des licenciés. Afin de rassurer les utilisateurs de ces licences, ce qu'on appellera des « labels » ont été mis en place pour certifier que ces licences répondent aux critères du libre, mais cela n'est qu'indicatif, puisque certaines licences bien que répondant aux critères du libre ne dispose pas de ces « labels ».

### Critères de l'OSI (Open Source Initiative) :

Ci-dessous une traduction des conditions pour qu'un logiciel soit considéré comme Open Source par l'OSI : 

*   **La redistribution libre**

La licence ne doit empêcher quiconque de vendre ou de donner le logiciel en tant que composant d'une distribution de logiciels constitués de programmes provenant de différentes sources. La licence ne doit pas exiger de droits d'auteur ou d'autres commissions sur une telle vente.

*   **Le code-source**

Le programme doit inclure le code source, et autoriser sa distribution sous forme compilée aussi bien que sous forme de code source. Lorsqu'un produit n'est pas distribué avec son code source, il doit exister un moyen bien indiqué pour l'obtenir sans autres frais qu'un coût raisonnable de reproduction, avec une préférence pour le téléchargement gratuit depuis l'Internet. Le code source doit être la forme privilégiée afin qu'un programmeur puisse modifier le programme. Il est interdit de proposer un code source rendu volontairement difficile à comprendre. Il est également interdit de soumettre des formes intermédiaires, comme le résultat d'un préprocesseur ou d'un traducteur automatique.

*   **Les œuvres dérivées**

La licence doit autoriser les modifications et les applications dérivées, et elle doit permettre leur distribution sous les mêmes termes que ceux de la licence du logiciel original.

*   **L'intégrité du code source de l'auteur**

La licence ne peut restreindre la redistribution d'un code source sous forme modifiée que si elle permet la distribution de fichiers de correction (patch) avec le code source, dans le but de modifier le programme au moment du développement. La licence doit explicitement permettre la distribution de logiciels développés à partir de codes sources modifiés. La licence peut exiger que les applications dérivées portent un nom différent ou un numéro de version distinct de ceux du logiciel original.

*   **La non-discrimination contre des personnes ou groupes**

La licence ne doit pas discriminer des personnes ou des groupes de personnes.

*   **La non-discrimination contre des champs d'application**

La licence ne doit pas limiter l'utilisation du logiciel à un champ d'application particulier. Par exemple, elle ne doit pas interdire l'utilisation du logiciel dans le cadre d'une entreprise ou pour la recherche génétique.

*   **La distribution de licence**

Les droits attachés au programme doivent s'appliquer à tous ceux à qui il est redistribué, sans obligation pour ces parties d'obtenir une licence supplémentaire.

*   **La licence ne doit pas être spécifique à un produit**

Les droits attachés au programme ne doivent pas dépendre du fait qu'il fasse partie d'une quelconque distribution de logiciels. Si le programme est extrait de cette distribution et est utilisé ou distribué sous les termes de sa propre licence, toutes les parties auxquelles il est redistribué doivent bénéficier des mêmes droits que ceux accordés par la distribution originelle de logiciels.

*   **La licence ne doit pas restreindre d'autres logiciels**

La licence ne doit pas imposer de restrictions sur d'autres logiciels distribués avec le logiciel licencié. Par exemple, la licence ne doit pas exiger que tous les programmes distribués sur le même support soient des logiciels open source.

*   **La licence doit être neutre sur le plan technologique**

Aucune disposition de la licence ne peut aller à l'encontre d'une quelconque technologie ou style d'interface. source : [http://fr.wikipedia.org/wiki/Open\_Source\_Definition](http://fr.wikipedia.org/wiki/Open_Source_Definition)

### Critères de la FSF (Free Software Foundation) :

1.  la liberté d'exécuter le programme (Liberté 0), numéroté « liberté 0 » car ce n'est pas une liberté propre aux logiciels libres.
2.  la liberté d'étudier le fonctionnement du programme, et de l'adapter à ses besoins (liberté 1)
3.  liberté de redistribuer des copies, donc d'aider son voisin (liberté 2)
4.  la liberté d'améliorer le programme et de publier des améliorations pour en faire profiter toute la communauté (liberté 3).


**Utilités des deux organisations :**

Ces deux organisations, permettent la promotion et la défense des logiciels libres. Elles instaurent des critères auxquels doivent répondre les licences pour être qualifiées de « libres ». Cependant, certaines licences répondent à tous les critères mais ne sont pas obligatoirement validées par les deux organisations ou simplement l'une d'elle (exemple de la licence CeCILLv2 reconnue par la FSF mais pas par l'OSI). La reconnaissance par ces organisations est comparable à un label du « libre », elle n'est pas obligatoire, mais peut être un gage de sécurité pour l'utilisateur.
