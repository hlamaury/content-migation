+++
title = "La protection des logiciels par le droit d\"auteur"
description = "La protection des logiciels par le droit d\"auteur"
tags = [ "Licence libre", "Droit d'auteur" ]
date = "2014-04-03"
+++

Depuis la loi du 3 juillet 1985 relative aux droits d'auteur, la notion d’œuvre de l'esprit a été étendue aux logiciels. Si ces derniers répondent au critère de l'originalité, ils pourront être protégés par le droit d'auteur. Sur la tête de l'auteur vont ainsi naître des droits moraux et patrimoniaux, lesquels lui permettront de contrôler l'exploitation futur de son œuvre.

En matière de logiciel dit « libre », Le droit est utilisé à contre-emploi, il autorise ce que le droit d'auteur entend d'habitude interdire. Les droits ainsi concédés au licencié seront formalisés dans une licence, dont son organisation contractuelle déterminera la nature de la licence, licence libre, licence propriétaire, free ware, shareware.

### 1\. L'accession à la protection par le droit d'auteur : condition d'originalité

Depuis la loi du 3 juillet 1985, la notion d’œuvre de l'esprit a été étendue aux logiciels. Donc désormais, s'ils répondent à la condition d'originalité, ils seront protégés par le droit d'auteur.

**Définition de l'originalité d'un logiciel**

L'originalité n'étant pas définit par le code de la propriété intellectuelle, la jurisprudence par l'arrêt PACHOT (Cass. Ass. Plén., 7 mars 1986) a mis en avant un critère objectif pour déterminer l'originalité : si le logiciel porte l'apport intellectuel de l'auteur, alors il est considéré original. Cet arrêt relevait que l'originalité d'un logiciel consiste dans un effort personnalisé allant au-delà de la simple mise en œuvre d'une logique automatique et contraignante. Ce critère a été rappelé par l'arrêt Cass. 1ère civ., 17 octobre 2012, n°11-21641.

**Preuve de l'originalité du logiciel**

L'originalité d'un logiciel est présumée, puisque ce n'est uniquement qu'en cas de litige mettant en cause l'originalité du logiciel que l'auteur devra la démontrer. Généralement, dans une affaire de contrefaçon, l'auteur aura tout intérêt de démontrer l'originalité de son œuvre afin de poursuivre pour contrefaçon toute personne qui aurait une utilisation non autorisée du logiciel (car il y a de forte chance que le présumé contrefacteur essaie de remettre en cause l'originalité du logiciel pour se réfugier derrière en espérant rendre irrecevable l'action en contrefaçon s'il s'avère que le logiciel ne peut être protégé par le droit d'auteur). Si l’œuvre n'est pas originale alors, par conséquent, elle ne sera pas protégeable par le droit d'auteur et donc il ne pourra y avoir contrefaçon.

Il est a signaler qu'une contrefaçon n'est pas automatiquement une copie servile, elle est jugée vis à vis des ressemblances et non discordances entre l'original et la présumée contrefaçon. Un exemple a été récemment donné par l'arrêt Cass 1ère civ., 14 novembre 2013, n°12-20687.

### 2\. Qu'est-ce qui est protégé par le droit d'auteur ?

Les logiciels sont qualifiés d’œuvres de l'esprit et sont à ce titre protégeables par le droit d'auteur (article L.112-2 13°). Sont protégés le logiciel ainsi que les documents auxiliaires, s'ils sont originaux c'est à dire s'il y a un apport intellectuel.

Les idées sont de libres parcours par Henri Desbois. Cela traduit le fait que ne sont pas protégeables les idées ou principes qui sous-tendent le logiciel, ni les algorithmes. La jurisprudence a bien précisé que les fonctionnalités d'un logiciel sont également exclues du droit d'auteur, ce dernier ne protège que la forme d'expression.

### 3\. L'attribution de la qualité d'auteur :

Principe :

L'article L.113-4 du code de la propriété intellectuelle (=CPI) pose le principe d'attribution de la qualité d'auteur à celui qui crée l’œuvre.

Tempérament :

L'article L.113-9 du CPI instaure une exception concernant l'attribution des droits patrimoniaux, qui joue uniquement pour les auteurs de logiciels salariés qui ont créé des logiciels « dans l'exercice de leurs fonctions ou d'après les instructions de leur employeur sont dévolus à l'employeur ». L'auteur salarié dispose toujours de ses droits moraux (enfin seul subsiste véritablement son droit de paternité), mais est soumis à une cession légale automatique au profit de son employeur concernant ses droits patrimoniaux (distribution, représentation, reproduction).

L'auteur de l’œuvre modifiée :

Le logiciel modifié créé à partir d'un logiciel existant, s'il est original sera également protégé par le droit d'auteur. Cependant, il sera nécessaire avant toute modification d'obtenir l'autorisation de l'auteur du logiciel initial afin de ne pas devenir contrefacteur, pour cela un contrat est le moyen le plus sûr de formaliser l'autorisation et les conditions de la modification, c'est ici que ressort l'utilité des contrats de licence. (Sur la qualité et les droits de l'auteur : le changement de licence ).

### 4\. Contenu des droits du créateur :

1\. Des droits moraux limités

En matière de logiciels, il n'y a pas de droit de repentir, ni de retrait.

Le droit au respect de l’œuvre est restreint à deux cas (L121-7 1° CPI) :

soit le cessionnaire modifie le logiciel en portant atteinte à l'honneur ou à la réputation de l'auteur (disposition permettant les adaptations par l'utilisateur) ; soit un tiers porte atteinte d'une quelconque manière au logiciel, quelle qu'en soit la conséquence. Enfin l'exercice du droit de divulgation lorsque l'auteur est salarié est limité en raison de la dévolution des droits à l'employeur. Seul reste véritablement entier le droit de paternité.

2\. Les droits patrimoniaux

Il faut se référer à l'article L.122-6 CPI pour avoir connaissance de ce qui est compris dans les droits patrimoniaux de l'auteur. Le droit patrimonial correspond au droit de reproduction, d'adaptation et de mise sur le marché par le titulaire de ces droits.

Cependant il existe une exception au droit patrimonial de l'auteur à l'article L.122-6-1 I. CPI qui dispose : « I. Les actes prévus aux 1° et 2° de l'article L. 122-6 ne sont pas soumis à l'autorisation de l'auteur lorsqu'ils sont nécessaires pour permettre l'utilisation du logiciel, conformément à sa destination, par la personne ayant le droit de l'utiliser, y compris pour corriger des erreurs.

Toutefois, l'auteur est habilité à se réserver par contrat le droit de corriger les erreurs et de déterminer les modalités particulières auxquelles seront soumises les actes prévus aux 1° et 2° de l'article L. 122-6, nécessaires pour permettre l'utilisation du logiciel, conformément à sa destination, par la personne ayant le droit de l'utiliser. » C'est au vu des droits patrimoniaux accordés à l'utilisateur que se différencient les licences libres des licences propriétaires.

### 5\. Durée de la protection par le droit d'auteur :

Principe :

L.123-1 CPI, les logiciels sont protégés pendant 70 ans après le décès de l'auteur pour les droits patrimoniaux, quant au droit moral il est inaliénable et imprescriptible.

Exception :

L.123-3 CPI, la durée de protection pour les œuvres collectives court à compter du 1er janvier de l'année suivant celle où l’œuvre a été publiée. Ces durées de protection ne jouent que pour les droits patrimoniaux, puisque la protection des droit moraux est perpétuelle.