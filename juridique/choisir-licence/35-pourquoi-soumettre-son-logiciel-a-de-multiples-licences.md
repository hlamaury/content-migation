+++
title = "Pourquoi soumettre son logiciel à de multiples licences ?"
description = "Pourquoi soumettre son logiciel à de multiples licences ?"
tags = [ "Juridique", "Licence libre" ]
date = "2014-04-03"
+++

Soumettre un logiciel à plusieurs licences est tout à fait possible. Plusieurs raisons peuvent justifier ce choix. Il s'agit avant tout de raisons d'ordre juridique et marketing.

Distribuer un logiciel sous une certaine licence est un choix laissé à l'auteur comme nous l'avons vu dans le billet Qui effectue le choix de la licence Par conséquent, il peut également choisir de distribuer son logiciel sous plusieurs licences.

**Les avantages de distribuer un logiciel sous plusieurs licences (multilicences) sont :**

D'une part, d'apporter une réponse aux problèmes de compatibilité entre les licences. En effet, il est ainsi possible de cumuler les avantages des licences et aussi des compatibilités. Par exemple un logiciel sous AGPLv3 et sous EUPL, sera compatible GPLv2, GPLv3, CeCILLv2 et CeCILLv2.1 et il est avantageux de soumettre un logiciel à deux licences initialement incompatibles mais tendant au même but, cela élargit les possibilités.

De plus, la technique des multilicences permet de cumuler les atouts de chaque licence.

Cela peut d'autre part, être une stratégie marketing, en utilisant une licence pour sa renommée, afin d'attirer une certaine communauté et/ou faire connaître une licence moins connue. Par exemple, un logiciel sous licence GPLv2 et CeCILLv2.1, permet de profiter de la renommée de la GPLv2 qui est la licence libre la plus utilisée et ainsi attirer la communauté GPLv2. Cela permet aussi de faire connaître d'avantage la licence CeCILLv2.1 par une communauté plus internationale.

Et enfin, distribuer un logiciel sous plusieurs licences est un gage de sécurité supplémentaire au cas où une des licences serait annulée par une juridiction nationale.

Conclusion : cette stratégie de multiple licences permet d’optimiser la diffusion des logiciels et faciliter les contributions.