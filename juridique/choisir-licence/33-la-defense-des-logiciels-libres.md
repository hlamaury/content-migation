+++
title = "La défense des logiciels libres"
description = "Défense des logiciels libres"
tags = [ "Juridique", "Licence libre" ]
date = "2014-03-31"
+++

**Que se passe-t-il en cas de non-respect des termes de la licence ?**

Souvent une clause résolutoire est insérée dans les contrats des licences libres, permettant ainsi de mettre fin au contrat de licence si le licencié n'en respecte pas les termes. Cela simplifie la résolution du contrat puisqu'il n'est pas nécessaire de saisir le juge pour obtenir cette résiliation.

**Une licence libre peut-elle encourir la nullité ?**

Comme tous les autres types de contrats, si une licence libre ne respecte pas les conditions de validité d'un contrat (le consentement, la capacité, l'objet, la cause), une des parties au contrat peut invoquer l'exception de nullité afin de ne pas exécuter le contrat et y mettre un terme.De même la licence peut être annulée si elle est en contradiction avec une disposition réglementaire ou législative impérative, dans ce cas la nullité encourue est absolue, mais certaines licences contiennent une clause de survie permettant de ne pas invalider tout le contrat si une clause est nulle. 

**Comment le titulaire des droits peut-il défendre son logiciel libre ?**

Le logiciel s'il est original est considéré comme une œuvre protégeable au titre du droit d'auteur. Donc si quelqu'un porte atteinte à un des droits d'auteur, le titulaire pourra l'assigner en contrefaçon. De plus, une action en concurrence déloyale peut être jointe à l'action en contrefaçon à titre principal ou subsidiaire. Préalablement à la lecture de ce billet, il est conseillé de prendre connaissance du billet "La protection des logiciels par le droit d'auteur, l'utilité de soumettre son logiciel à une licence libre"

### La conclusion d'un contrat - le cadre légal par le droit des contrats (règles communes à tous les contrats) :

**1\. Validité d'un contrat**

Comme tous les contrats, la licence libre doit remplir quatre conditions pour être valide (article 1108 du Code civil = cc) : le consentement (sur l'intégrité du consentement (article 1109cc, il existe trois vices pouvant mettre en cause la validité du consentement (l'erreur article 1110cc ; le dol 1116cc et la violence 1111 à 1115cc)). la capacité (c'est le fait qu'une personne puisse légalement passer un contrat, qu'elle soit donc majeure et qu'elle ne soit pas frappée d'incapacité d'exercice de ses droits). l'objet (c'est sur quoi porte le contrat) la cause (pour quelles raisons les parties se sont engagées)

**2\. Le contenu d'un contrat**

La licence est un contrat qui à force de loi entre les parties, c'est ce qui est appelé l'effet relatif des contrats. Le droit commun des contrats est applicable aux licences libres et les parties à ce contrat peuvent donc avant tout litige, s'accorder sur les conséquences engendrées par l’inexécution des obligations contractuelles.

Pour cela : soit les parties peuvent accroître les charges qui pèsent sur le débiteur. soit au contraire elles les diminuent souvent par une clause de non-responsabilité, mais attention les clause léonines sont interdites. Dans le cas des licences libres, c'est la deuxième solution qui est choisie en utilisant les clauses de non-responsabilité qui permettent l'exonération quasi-totale de responsabilité du concédant de la licence.

Lorsque le contrat comporte une obligation de résultat (comme les contrats de licences libres), le créancier bénéficie d'une présomption de faute qui ressort du simple constat du défaut d'exécution de l'obligation contractuelle. Le débiteur est autorisé à faire échec à cette présomption par une clause de non-responsabilité. Aux termes d'une telle clause, le créancier devra démontrer une faute du débiteur pour obtenir l'octroi de dommages-intérêts et non se contenter de dresser le constat matériel du défaut d'exécution, comme il suffit en cas d'obligation de résultat non atteint. En revanche, une clause qui exclurait purement et simplement la responsabilité contractuelle du débiteur serait nulle.

Par ailleurs, la jurisprudence écarte les clauses de non-responsabilité lorsque le défaut d'exécution est la conséquence d'un comportement volontaire du débiteur. Ainsi, les fautes dolosives et intentionnelles, étant entendues plus généralement sous le terme de fautes lourdes du débiteur font échec à l'application des clauses de non-responsabilité. En résumé :les clauses de non-responsabilité ne s'appliquent pas en cas de faute lourde du débiteur.

Au sein du contrat une clause résolutoire peut être convenue entre les parties, celle-ci permettant en cas d'inexécution de mettre fin au contrat sans passer devant le juge. En effet normalement la résolution d'un contrat (article 1184 cc alinéa 3) est une sanction judiciaire uniquement autorisée par le juge.

Certaines licences libres contiennent des clauses résolutoires en prévention du cas où le licencié ne respecterait pas une des clauses du contrat qui serait alors résolue.

**3\. Quand les parties peuvent-elles agir en nullité ?**

(pour ne pas avoir à exécuter le contrat)

Le concédant peut utiliser la clause résolutoire (s'il y en a une dans le contrat de licence) si le licencié ne respecte pas les termes de la licence. Cette clause rendra caduc le contrat sans passer devant le juge. Les deux parties au contrat et parfois même des tiers peuvent invoquer l'exception de nullité pour mettre fin au contrat, pour ne pas avoir à l'exécuter.

Dans quels cas l'invoquer ? A chaque fois qu'il y a violation d'une condition de formation du contrat. La nullité peut être relative (c'est-à-dire lorsque seules les parties au contrat peuvent invoquer l'exception de nullité) ou bien la nullité peut être absolue (ce qui signifie que les parties ainsi que tous les tiers intéressés peuvent agir en nullité du contrat). La nullité est relative lorsqu'il s'agit d'une atteinte à une des règles qui tend à protéger l'intérêt particulier de l'un des cocontractants. La nullité est absolue lorsque l'intérêt général est atteint, l'ordre public est mis en cause. (article 6 cc : On ne peut déroger, par des conventions particulières, aux lois qui intéressent l'ordre public et les bonnes mœurs.).

Dans tous les cas, le juge n'a pas de pouvoir d'appréciation sur l'opportunité de la sanction lorsqu'il est saisi d'une action en exception de nullité, il doit prononcer la nullité, on dit qu'elle est de droit.

Il faut faire attention et vérifier que le contrat de licence libre ne contienne pas une clause de survie avant d'engager une action en exception de nullité en invoquant la contradiction d'une des clauses de la licence vis à vis du droit applicable, puisque la clause de survie permet de ne pas annuler toute la licence si une clause est invalide, le temps que cette dernière soit modifiée afin d'être conforme au droit.

**4\. Application aux logiciels libres**

La conséquence du droit des contrats est que certaines licences qui n'obéissent pas aux dispositions légales impératives (= celles que l'on ne peut déroger) encourent la nullité ce qui instaure une certaine insécurité juridique, néfaste dans la sphère des échanges. Exemple avec la licence GPLv3, pour l'instant jamais annulée mais certaines de ses dispositions pourraient être discutées et voir annulées par la suite, ce qui serait lourd de conséquence, c'est pour cela qu'en pratique elle n'a encore jamais été annulée, sa validité n'a pas été mise en cause.

Mais un risque plane sur les licences qui par certaines de leurs dispositions sont contraires à l'ordre public de certains États dans lesquelles elles encourent le risque d'être déclarées nulles et de laisser un logiciel sans cadre contractuel pour gérer les droits et obligations entre licencié et concédant, d'où l'utilité des multilicences, puisque si ce cas venait à se présenter, les droits sur le logiciel ne seraient pas sans cadre clair pour les organiser, il y aurait toujours une licence opérationnelle.

**5\. Les dispositions législatives les plus souvent mises à mal par les licences ?**

(cette liste de cas n'est pas exhaustive)

Article L131-1 CPI dispose : La cession globale des œuvres futures est nulle., il est souvent reproché aux licences GNU GPL d'être contraire à cette disposition, mais actuellement ces licences n'ont pas été annulées pour ce motif et ne le seront sûrement jamais les conséquences en seraient trop lourdes.

Article L131-3 CPI dispose : La transmission des droits de l'auteur est subordonnée à la condition que chacun des droits cédés fasse l'objet d'une mention distincte dans l'acte de cession et que le domaine d'exploitation des droits cédés soit délimité quant à son étendue et à sa destination, quant au lieu et quant à la durée." Un contrat de cession de droits est valable à condition que le domaine d'exploitation des droits cédés soit délimité conformément aux termes du premier alinéa du présent article. "Les cessions portant sur les droits d'adaptation audiovisuelle doivent faire l'objet d'un contrat écrit sur un document distinct du contrat relatif à l'édition proprement dite de l’œuvre imprimée." "Le bénéficiaire de la cession s'engage par ce contrat à rechercher une exploitation du droit cédé conformément aux usages de la profession et à verser à l'auteur, en cas d'adaptation, une rémunération proportionnelle aux recettes perçues." Il est donc nécessaire dans le contrat d'une licence, qu'il y ait la mention de chacun des droits concédés, ainsi que la précision quant à l'étendue, la durée, la destination, et le lieu.

Article L122-7 CPI dispose : Le droit de représentation et le droit de reproduction sont cessibles à titre gratuit ou à titre onéreux. La cession du droit de représentation n'emporte pas celle du droit de reproduction. La cession du droit de reproduction n'emporte pas celle du droit de représentation. Lorsqu'un contrat comporte cession totale de l'un des deux droits visés au présent article, la portée en est limitée aux modes d'exploitation prévus au contrat. Cet article rejoint la pensée de l'article L122-7CPI, chaque droit concédé doit être défini clairement, il n'y a pas de cession implicite.

### Défense sur les fondements de la responsabilité civile et du droit d'auteur cumulativement ou subsidiairement : (pour obtenir réparation d'un préjudice)

**1\. Incidence du droit d'auteur :**

La protection par le droit d'auteur permet de saisir la justice sur le fondement de la contrefaçon si un droit de l'auteur est atteint, afin de faire cesser l'atteinte et réparer le préjudice subit par le titulaire des droits.

\-Attention car le droit d'auteur ne protège que la forme des œuvres, c'est-à-dire en matière de logiciel il s'agit des éléments visibles par l'utilisateur comme les interfaces, les menus ou les écrans mais aussi les codes sources ou objets du logiciel. -Les moyens de rapporter la preuve de la contrefaçon : le constat d'huissier : Document établi par un huissier qui rend compte d'une situation donnée pour servir de preuve dans le cadre d'une procédure judiciaire en cours ou à venir. la saisie-contrefaçon : « La saisie-contrefaçon est un mode de preuve de la contrefaçon et, plus généralement, de toute atteinte à un droit de propriété intellectuelle. »

Ces deux modes de preuves peuvent être cumulés et utilisés pour une action en contrefaçon et/ou pour parasitisme ou de la concurrence déloyale.

\*L'action en contrefaçon est fondée sur le droit d'auteur, elle sanctionne l'atteinte à un droit réel.

\*L'action en concurrence déloyale ou parasitisme est fondée sur la responsabilité civile, cette action sanctionne l'atteinte à un droit personnel, l'objet du litige n'est pas nécessairement original pour que l'action en concurrence déloyale puisse être engagée contrairement à la contrefaçon, de plus la personne qui saisit la justice sur le fondement de la concurrence déloyale n'a pas besoin d'être titulaire des droits sur l'objet litigieux, cette personne doit simplement avoir un intérêt à agir, mais il faudra qu'elle démontre une faute, un préjudice et un lien de causalité.

Les deux actions peuvent se cumuler à condition qu'elles soient chacune fondées sur une faute distincte, sauf si l'action en concurrence déloyale est subsidiaire.

« subsidiaire » signifie secondaire. Dans une assignation en justice, il s'agit d'une prétention dont l'examen par le juge n'aura lieu que dans le cas où il aura rejeté la demande présentée au principal. Si le juge fait droit à la demande principale, il n'aborde pas les demandes subsidiaires.

Donc les deux actions peuvent exister et être présentées toutes deux à titre principal et donner lieu toutes les deux à des dédommagements car elles ne sanctionnent pas les mêmes choses dans ce cas, mais, pour que les deux actions conjointes puissent aboutir, elles doivent se fonder sur des faits distincts. Ces deux actions peuvent également être fondées sur les mêmes faits mais à conditions que les deux actions ne soient pas engagées à titre principal, une des deux sera l'action subsidiaire.

Concurrence déloyale vs parasitisme : Pour que l'action en concurrence déloyale aboutisse il faut démontrer l'existence d'une faute. Ce qui distingue l'action en concurrence déloyale classique du parasitisme, est le fait que pour qu'il y ait parasitisme, il n'est pas nécessaire qu'il y ait concurrence ni même confusion même si ce critère est souvent repris pour justifier une situation parasitaire.

Le parasitisme se définit comme le fait pour un agent économique de profiter à des fins commerciales de la réputation ou des investissements financiers, matériels ou intellectuels d’autrui, ce qui traduit un non-respect des usages en matière de commerce. Le parasitisme est initialement une forme de concurrence déloyale, mais la jurisprudence tend à l'en éloigner progressivement. Pour obtenir réparation du préjudice subit par le comportement parasitaire du défendeur, le demandeur doit simplement démontrer que le défendeur a profité de sa réputation ou son investissement peu importe qu'ils soient tous deux en concurrence ou non.

**2\. Pour faciliter l'action en contrefaçon, des présomptions ont été instaurées par le code de la propriété intellectuelle :**

L'article L.113-1du Code de la propriété intellectuelle (= CPI) instaure une présomption de la qualité d'auteur (fonctionne pour les personnes physiques). Cet article dispose : La qualité d'auteur appartient, sauf preuve contraire, à celui ou à ceux sous le nom de qui l’œuvre est divulguée . Ce qui signifie que le nom sous lequel est divulgué l’œuvre la première fois est réputé être celui de l'auteur de cette œuvre jusqu'à preuve du contraire. Preuve qui peut être rapportée par tout moyen, elle est libre puisqu'il s'agit d'une présomption simple. Mais attention, cette présomption vaut pour la qualité d'auteur et non pour la titularité des droits patrimoniaux qui peuvent avoir été cédés.

L'article L.113-5 CPI instaure une présomption de titularité des droits pour la personne morale et la personne physique sur une œuvre collective. Cet article dispose : L’œuvre collective est, sauf preuve contraire, la propriété de la personne physique ou morale sous le nom de laquelle elle est divulguée. Cette personne est investie des droits de l'auteur. Cette présomption a été instaurée pour empêcher les contrefacteurs de se réfugier derrière des arguments d'ordre probatoire, la cour de cassation fait bénéficier le demandeur d'une présomption de titularité lorsqu'il exploite commercialement l’œuvre en cause.

Donc pour bénéficier de cette présomption :

*   il faut une exploitation publique (le simple dépôt auprès de l'INPI sous enveloppe Soleau ne suffit pas à caractériser cette exploitation publique) ;
*   absence de revendication par quelqu'un qui dispose effectivement de la qualité d'auteur et qui montre l'absence de cession de droit.

**3\. Engagement d'une action en contrefaçon :**

L'engagement de l'action en concurrence déloyale ne sera pas abordée puisqu'elle est beaucoup moins restrictive que l'action en contrefaçon, si une personne peut agir en contrefaçon elle pourra également agir en concurrence déloyale. Pour engager une action en concurrence déloyale, il suffit d'avoir un intérêt à agir.

Article L.331-1alinéa 1 CPI les actions civiles et les demandes relatives à la propriété littéraire et artistique, y compris lorsqu'elle portent également sur une question connexe de concurrence déloyale, sont exclusivement portées devant des tribunaux de grande instance, déterminés par voie réglementaire .

Par principe, il peut y avoir contrefaçon uniquement si une œuvre est originale et donc protégeable par le droit d'auteur. En effet, la contrefaçon est l'atteinte à un droit d'auteur (dans le cas d'un logiciel) et pour bénéficier de la protection par le droit d'auteur l’œuvre doit être originale (voir « la protection des logiciels par le droit d'auteur »).

Au civil, « la contrefaçon existe indépendamment de toute faute ou mauvaise foi du contrefacteur », contrairement au pénal où il faut l'intention de commettre.

Qualité à agir en contrefaçon :

Concernant le demandeur, il doit prouver qu'il est bien titulaire des droits qu'il invoque, sauf à ce que les tribunaux se satisfassent des présomptions.

*   L'action en contrefaçon peut être exercée par l'auteur, s'il n'est pas dessaisi. L'auteur qui a cédé ses droits patrimoniaux n'a plus qualité pour les exercer, sauf pour les droits qui ne sont pas compris dans la cession. Bien entendu, il reste recevable à agir en défense de son droit moral.
*   Qu'il soit dénommé cessionnaire ou licencié, celui qui dispose d'un droit exclusif sur l’œuvre doit pouvoir exercer l'action en contrefaçon, alors que cette possibilité doit être refusée au bénéficiaire d'une « simple autorisation » non exclusive.

Concernant le défendeur (le présumé contrefacteur) :

*   ce peut être un tiers qui n'a jamais eu aucun droit sur l’œuvre contrefaite.
*   cela peut aussi être l'auteur ou un cessionnaire subséquent qui aurait poursuivit l’exploitation en dépit de la cession qu'il aurait consentie.
*   ce peut être un cessionnaire qui dépasse les limites de la cession.

Exemple appliqué aux logiciels libres :

Si par exemple, un licencié fait une copie du logiciel sous licence CeCILLv2.1 (ce qu'il est autorisé à faire avec un logiciel libre), mais qu'ensuite il redistribue le logiciel sous licence propriétaire et qu'il ne donne pas accès au code source, alors il aura outrepassé les termes de la licence et sera considéré comme contrefacteur.

Savoir qui est titulaire des droits sur le logiciel afin de pouvoir engager l'action en contrefaçon :

*   S'il s'agit d'un auteur personne physique : Si l'auteur d'un logiciel décide de le mettre sous licence libre (exemple avec la licence libre CeCILL v2.1), cette licence est non exclusive pour les licenciés successifs, donc l'auteur initial conserve le pouvoir d'agir en cas de contrefaçon si un de ses droits est atteint, il en est resté titulaire, de même pour les contributeurs sur leurs contributions. La présomption de l'article L.113-1 CPI pourra être invoquée pour démontrer la qualité d'auteur et donc de titulaire initial des droits qu'il n'a pas cédé par licence exclusive sur son logiciel.
*   S'il s'agit d'une œuvre collective : S'il s'agit d'une œuvre collective la personne physique ou morale sous le nom de laquelle est exploité commercialement/publiquement l’œuvre, bénéficiera de la présomption de l'article L.113-5CPI pour prouver sa titularité des droits sur l’œuvre collective et donc cette personne sera en capacité d'agir en justice pour contrefaçon et également pour concurrence déloyale (mais cette action peut être intentée par toute personne qui justifie d'un intérêt à agir donc champ plus large que pour l'action en contrefaçon).
*   Action en contrefaçon seule ou jumelée avec une action en concurrence déloyale ? Suivant les termes de la licence CeCILLv2.1, le fait pour le licencié de redistribuer le logiciel sans fournir le code source et de plus sous licence propriétaire sont des inexécutions de plusieurs obligations du licencié, une clause résolutoire est insérée dans la licence et va permettre au concédant de résoudre le contrat qui le lie au licencié. De plus, l'auteur pourra assigner le licencié contrefacteur en justice pour contrefaçon et également concurrence déloyale ou parasitisme.

D'un point de vue stratégique, il est préférable d'intenter l'action en contrefaçon au principal (fondée sur le droit d'auteur) et l'action en concurrence déloyale ou en parasitisme (les deux actions sont fondées sur l'article 1382 cc) à titre subsidiaire, puisqu'il sera plus aisé d'apporter la preuve pour les deux actions simultanément, les preuves accumulées pour l'action en contrefaçon peuvent servir pour l'action en concurrence déloyale si l'action en contrefaçon est rejetée. Si les deux actions sont cumulées conjointement, il faudra les aborder de façon indépendante, de plus si les deux actions connexes sont rejetées le demandeur se retrouve sans rien alors que si l'action en concurrence déloyale est subsidiaire, le demandeur pourra plus facilement voir sa demande aboutir, c'est une sorte de seconde chance, le titulaire des droits pourra obtenir une réparation mais sur le fondement de la responsabilité civile dans ce cas.