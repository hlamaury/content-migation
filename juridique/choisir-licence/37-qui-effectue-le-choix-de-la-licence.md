+++
title = "Qui effectue le choix de la licence ?"
description = "Qui effectue le choix de la licence ?"
tags = [ "Licence libre", "Droit d'auteur" ]
date = "2014-04-03"
+++

C'est le libre choix de l'auteur initial du logiciel. Les auteurs de logiciels modifiés devront préalablement avant toute modification et distribution obtenir l'autorisation de l'auteur initial personne physique, ou personne morale (dans le cas d'une œuvre collective) et ce le plus souvent par la conclusion d'un contrat de licence.

 Par exemple, avec une licence GNU GPLv3, l'auteur initial du logiciel qui a choisi de soumettre son logiciel a cette licence, autorise les licenciés à utiliser, modifier et distribuer ou redistribuer le logiciel modifié ou non librement, mais sous condition de soumettre le logiciel modifié ou non à cette même licence. Donc dans ce cas, le licencié auteur d'une œuvre dérivée n'aura pas le choix de la licence, donc de la manière dont il va gérer ses droits.

Principe : Le changement de licence est une prérogative de l'auteur.

{well}

Préalablement à la lecture de ce billet, il est conseillé de prendre connaissance du billet : [La protection des logiciels par le droit d'auteur](index.php/juridique/39-la-protection-des-logiciels-par-le-droit-d-auteur), l'utilité de soumettre son logiciel à une licence libre.

{/well}

### En cas d'auteur unique

Dans la situation la plus simple où l'auteur est le seul à détenir les prérogatives de droit d'auteur, il a la possibilité de soumettre son logiciel à une nouvelle licence de façon tout à fait arbitraire. Cependant, lors d'un changement de licence, cela n'a pas d'effet rétroactif, donc toutes les licences concédées auparavant restent valables, le changement n'a d'effet que pour l'avenir.

Si l’auteur peut donc concéder des licences différentes de la licence choisie à l'origine, il ne peut s’opposer à la circulation du logiciel sous l'ancienne licence. En ce sens, il faut comprendre que l’acte de diffusion d'un logiciel sous une certaine licence libre est irréversible.

### En cas de pluralité d'auteurs

Un logiciel est considéré comme une œuvre de l'esprit, donc il peut être une œuvre issue de plusieurs auteurs suivant la lettre de l'[article L113-2](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278882&cidTexte=LEGITEXT000006069414&dateTexte=20140331&oldAction=rechCodeArticle) CPI qui dispose :

Est dite de collaboration l’œuvre à la création de laquelle ont concouru plusieurs personnes physiques.

Est dite composite l’œuvre nouvelle à laquelle est incorporée une œuvre préexistante sans la collaboration de l'auteur de cette dernière.

Est dite collective l’œuvre créée sur l'initiative d'une personne physique ou morale qui l'édite, la publie et la divulgue sous sa direction et son nom et dans laquelle la contribution personnelle des divers auteurs participant à son élaboration se fond dans l'ensemble en vue duquel elle est conçue, sans qu'il soit possible d'attribuer à chacun d'eux un droit distinct sur l'ensemble réalisé.

Plusieurs situations peuvent se présenter :

*   **Le logiciel est une œuvre collective**, c'est à dire qu'une personne physique ou morale peut être titulaire des droits d'auteur sur l’œuvre collective et peut donc disposer librement des droits patrimoniaux dessus. En conséquence, cette personne peut choisir la licence sous laquelle l’œuvre sera distribuée et comment seront régis les droits patrimoniaux sur le logiciel. (Donc ici, la situation est semblable à celle de l'auteur unique, puisque certes plusieurs auteurs ont contribué à la réalisation du logiciel, mais une seule personne est titulaire des droits d'auteurs dessus, donc un décideur unique).
*   **Le logiciel est une œuvre de collaboration**, ici plusieurs auteurs personnes physiques ont contribué à la création de l’œuvre en même temps, de façon concertée par une inspiration commune.Ici les différents auteurs sont tous titulaires des droits d'auteurs de manière indivis, c'est une copropriété. Donc pour changer la licence sous laquelle est distribuée le logiciel, l'unanimité des consentements est requise et cela peut engendrer des situations de blocage.
*   **Le logiciel est une œuvre composite ou dérivée**, c'est-à-dire lorsqu'il est développé à partir d’un logiciel préexistant (dite œuvre première). Donc en ce qui concerne l’œuvre initiale (=préexistante) c'est le premier auteur qui conserve les prérogatives d'auteur dessus. En ce qui concerne l’œuvre seconde, c'est l'auteur de celle-ci qui en détient les droits d'auteurs mais sous contrainte de respecter les droits de l'auteur premier (L.113-4CPI) ce qui signifie que l'auteur second doit obtenir l'autorisation de l'auteur premier avant toute utilisation de l’œuvre, d'où l'utilité d'une licence qui contractualise tout ceci clairement.

**Remarque** : les cas présentés précédemment peuvent dans certaines situations se combiner.

### L'auteur salarié :

L'[article L113-9](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278890&cidTexte=LEGITEXT000006069414&dateTexte=20140331&oldAction=rechCodeArticle) CPI dispose : Sauf dispositions statutaires ou stipulations contraires, les droits patrimoniaux sur les logiciels et leur documentation créés par un ou plusieurs employés dans l'exercice de leurs fonctions ou d'après les instructions de leur employeur sont dévolus à l'employeur qui est seul habilité à les exercer.

Toute contestation sur l'application du présent article est soumise au tribunal de grande instance du siège social de l'employeur. Les dispositions du premier alinéa du présent article sont également applicables aux agents de l’État, des collectivités publiques et des établissements publics à caractère administratif.

Dans le cas où l'auteur du logiciel serait salarié, s'il crée dans le cadre de ses fonctions, alors les droits patrimoniaux du développeur reviennent à l'employeur, c'est une cession légale automatique. C'est alors à l'employeur qu'il revient de choisir la licence sous laquelle sera distribuée le logiciel, c'est-à-dire comment seront gérés les droits patrimoniaux sur le logiciel.

Dans le cas où l'employeur est titulaire des droits patrimoniaux cela ne change rien à la mécanique vu précédemment qui s'applique également dans ce cas.

*   Il peut s'agir du seul titulaire des droits donc situation dans laquelle "il fait ce qu'il veut" .
*   Il sera beaucoup moins fréquent de rencontrer une œuvre de collaboration car en général tous les auteurs sont employés et par conséquent, tous les droits patrimoniaux reviendront entre les mains du seul employeur, donc situation semblable à celle vu ci dessus.
*   Concernant les œuvres collectives, l'employeur sera celui qui prend l'initiative, qui organise et divulgue sous son nom donc aucune difficulté.
*   Enfin, en ce qui concerne les œuvres composites ou dérivées, le mécanisme sera le même que celui développé précédemment, le seul changement sera au niveau du titulaire des droits patrimoniaux qui sera non pas l'auteur mais l'employeur.
*   Par exemple, un premier développeur crée un logiciel qu'il soumet à une licence libre, puis un employeur ayant obtenu cette licence libre (donc il a l'autorisation d'apporter des modifications) sur ce logiciel décide de le modifier/l'adapter. Le résultat de cette modification ou adaptation du logiciel donne lieu à une « nouvelle œuvre » qui est la propriété de l'employeur (même si c'est son employé qui l'a créé), ce dernier devra simplement obtenir l'accord du ou des propriétaires du logiciel originaire pour exploiter le logiciel dérivé et déterminer ensemble les conditions d'exploitation de ce nouveau logiciel pour respecter les droits du premier propriétaire, mais cela est établi par avance dans le contrat de la licence libre.