+++
title = "Le mécanisme de la compatibilité entre licences"
description = "Le mécanisme de la compatibilité entre licences"
tags = [ "Juridique", "Licence libre" ]
date = "2014-04-07"
+++

En quelques mots, la compatibilité entre licence libre, est la possibilité de « mélanger » des composants de logiciels sous différentes licences entre eux, afin d'obtenir de nouveaux logiciels modifiés, puis par la suite de les soumettre à une certaine licence plutôt qu'à une autre.

Le principe : la licence d'un logiciel ne peut pas conférer plus de droits et moins d'obligations que les licences de chacun des composants qui sont intégrés au logiciel.

### Compatibilité supérieure :

C'est la capacité d'un logiciel sous une certaine licence d'être soumis aux termes d'une autre licence.

Cette compatibilité se décline en 3 mécanismes :

*   La compatibilité de la licence avec ses nouvelles versions : c'est la mise à jour du logiciel, la possibilité pour un logiciel sous ancienne version de la licence, d'être soumise à la nouvelle version de la licence. Cette compatibilité est repérable au sein du contrat de licence par les termes ou toute version ultérieure.
*   La compatibilité supérieure expresse : certaines licences incluent des clauses prévoyant une compatibilité expresse avec d'autres licences libres, aux profits desquelles elles acceptent que le logiciel soit soumis (sous différentes conditions : soit il faut une intégration de code sous cette autre licence, soit simplement le logiciel modifié sans intégration de code préexistant peut être redistribué sous une licence expressément compatible avec la licence du logiciel initial).
*   La compatibilité supérieure implicite : provient de la permissivité de la licence en composition et/ou en dérivation. Plus elle sera permissive plus elle sera implicitement compatible avec de nombreuses licences.

### Compatibilité inférieure :

C'est la capacité de la licence d'accueillir un logiciel ou des composants logiciels soumis à d'autres licences.

Cette compatibilité peut être vérifiée de 2 façons :

Cette compatibilité existe lorsque la licence du logiciel/ composant logiciel accueillit par la licence est expressément compatible avec cette dernière. Sous l'angle des facilités d'accueil expresses ou implicites (par la permissivité de la licence) dont dispose la licence « d'accueil ».

**Mise en application avec quelques licences à fort copyleft** (en se basant uniquement sur les compatibilités expresses) ; exemples avec les licences : [GPLv3](index.php/juridique/31-presentation-de-la-licence-gpl-v3) ; [AGPLv3](index.php/juridique/30-presentation-de-la-licence-agpl-v3) ; [CeCILL v2](index.php/juridique/29-presentation-de-la-licence-cecill-v2) ; [CeCILL v2.1](index.php/juridique/28-presentation-de-la-licence-cecill-v2-1) ; [EUPL](index.php/juridique/27-presentation-de-la-licence-eupl-v1-1).

*   Avec la GPLv3

Compatibilité supérieure : Aucune, sauf avec les versions ultérieures de la GPL s'il est précisé ou toute version ultérieure , alors le logiciel sous licence GPLv3 pourra être mis à jour et distribué sous la nouvelle version GPL.

Compatibilité inférieure : la licence GPLv3 permet d'accueillir des composants sous les licences : AGPLv3, CeCILLv2 ; CeCILLv2.1, qui se soumettront à la licence GPLv3. Mais attention, compatibilité avec l'AGPLv3 mais sous condition. Le logiciel dérivé issu de composants sous AGPLv3 et GPLv3, est soumis à la licence GPLv3 mais la clause de communication du code source modifié s'applique même lors de la distribution en réseau.

*   Avec l'AGPLv3

Compatibilité supérieure : avec la GPLv3, uniquement en cas de modification du logiciel par intégration de code soumis à la licence GPLv3, c'est ce que l'on appelle en matière de composition. Compatibilité inférieure : CeCILLv2.1 ; GPLv3

Rappel : c'est la capacité d'un logiciel sous licence AGPLv3 d'accueillir des composants soumis aux licences citées ci-dessus.

*   Avec la CeCILLv2

Compatibilité supérieure : GPLv3 ; GPLv2 ; ainsi que la CeCILLv2.1 la mise à jour est acceptée par la licence CeCILLv2 aux termes de son article 12.

Compatibilité inférieure : GPLv3 ; GPLv2 ; CeCILL v2.1 ; EUPL, du code sous CeCILLv2 peut accueillir du code sous les licences citées ci-avant.

*   Avec la CeCILLv2.1

Compatibilité supérieure : un logiciel sous licence CeCILL v2.1 peut être soumis en cas d'intégration de code sous une autre licence, aux termes de GPLv2 ; GPLv3 ; AGPLv3 ou EUPL. Par son article 12, la licence CeCILLv2.1 permet de soumettre le logiciel sous cette licence à la version ultérieure, c'est la mise à jour.

Compatibilité inférieure : un logiciel sous licence CeCILL v2.1 peut accueillir des composants sous les licences GPLv2 ; GPLv3; AGPLv3 ; EUPL ; CeCILLv2.

*   Avec l'EUPL

Compatibilité supérieure : si un composant est intégré à du code sous licence EUPL, le code modifié peut être par la suite soumis aux licences GPLv2 ; CeCILLv2. De plus, par son article 13 la licence EUPL permet de mettre à jour le logiciel et de le soumettre à toute version ultérieure de cette licence.

Compatibilité inférieure : un logiciel sous licence EUPL a la capacité d'accueillir du code sous licence GPLv2 ; CeCILLv2 ; CeCILLv2.1.

**Résumé**

Si on mélange :

AGPLv3 + GPLv3 = GPLv3 + clause rendant applicable l'obligation de communication du code source même en réseau

AGPLv3 + CeCILLv2.1 = AGPLv3

GPLv3 + CeCILLv2 = GPLv3

GPLv3 + CeCILLv2.1 = GPLv3

EUPL + GPLv2 = GPLv2

EUPL + CeCILLv2 = CeCILLv2

EUPL + CeCILLv2.1 = EUPL

CeCILLv2 + GPLv2 = GPLv2

CeCILLv2.1 + GPLv2 = GPLv2