+++
title = "Marchés publics vs. contrôle de légalité"
description = "Marchés publics vs. contrôle de légalité"
tags = [ "S²LOW", "Marchés publics", "Dématérialisation", "Signature électronique" ]
date = "2018-08-03"
+++

La date du **1er octobre 2018**, correspondant à l'obligation de dématérialisation des marchés publics, fait couler beaucoup d'encre. Certains points méritent d'être rappelés ou précisés pour aborder sereinement cette échéance.

1.  La **dématérialisation de la publication des marchés publics existe depuis 2005**, ce n'est donc pas une nouveauté. Même si la réforme de la commande publique entrée en vigueur le 1er avril 2016 vient préciser les choses, en particulier du côté du profil acheteur (cad la plateforme de publication des marchés publics !). La procédure est [simplifiée](https://www.boamp.fr/Espace-entreprises/Actualites/Reforme-des-marches-publics-ce-qui-change-pour-les-entreprises) pour les entreprises.  
    On notera en particulier que depuis 2016, la signature (électronique ou manuelle!) n'est plus obligatoiire pour les réponses des entreprises. Ceci a été [confirmé en juin 2016](https://www.boamp.fr/Espace-entreprises/Actualites/Marches-publics-la-signature-de-l-offre-et-de-la-candidature-n-est-plus-obligatoire "les entreprises peuvent ne pas être obligées de signer.") en réponse à une question de sénateur. La collectivité peut toutefois exiger une signature électronique.
2.  I**l est possible d'envoyer les marchés publics au contrôle de légalité** de manière dématérialisée depuis le début du projet ACTES. En 2006, la classification "marché public" existait déjà, même si à l'époque la limitation à 20 Mo limitait techniquement la transmission des pièces. Depuis, la limite technique est passée à 200 Mo par transaction: on a maintenant la place de dématérialiser un marché public. Seuls les plans sont à exclure de l'envoi.  
    Les services de l'État précisent la liste des pièce à envoyer au contrôle de légalité, telle la préfecture de l'Eure en a fait un article dédié :  
      
    [Pièces à fournir au contrôle de légalité (article R 2131-5 CGCT)](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité") 
    
    [« _La transmission au préfet ou au sous-préfet des marchés des communes et de leurs établissements publics autres que les établissements publics de santé comporte, les pièces suivantes :_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_1° La copie des pièces constitutives du marché, à l'exception des plans ;_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_2° La délibération autorisant le représentant légal de la commune ou de l'établissement à passer le marché ;_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_3° La copie de l'avis d'appel public à la concurrence ainsi que, s'il y a lieu, de la lettre de consultation ;_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_4° Le règlement de la consultation, lorsque l'établissement d'un tel document est obligatoire ;_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_5° Les procès-verbaux et rapports de la commission d'appel d'offres, de la commission de la procédure de dialogue compétitif et les avis du jury de concours, avec les noms et qualités des personnes qui y ont siégé, ainsi que le rapport de présentation de la personne responsable du marché prévu par l'article 75 du code des marchés publics ;_](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité")  
    [_6° Les renseignements, attestations et déclarations fournis en vertu des articles 45 et 46 du code des marchés publics._](http://www.eure.gouv.fr/Politiques-publiques/Collectivites-locales-Intercommunalite/Controle-de-legalite/Commande-publique/Transmission-des-marches-publics-au-controle-de-legalite "Pièces à fournir au contrôle de légalité") >>
    
3.  **La signature électronique ne devient pas obligatoire dès le 1er octobre 2018.**  
    La Direction des Affaires Juridiques (DAJ) nous fait bénéficier de ses lumières au travers d'un guide à destination des acheteurs publics, présenté sous la forme de Questions / Réponses qui le rend très simple à lire. Pour en savoir plus, consulter [notre article dédié](https://adullact.org/breves/67-actualite/actu-libre-france/776-dematerialisation-des-marches-publics-au-1er-octobre-2018 "Explications relatives à la signature des marches publics") et les [références au Ministère des Finances](https://www.economie.gouv.fr/daj/dematerialisation-commande-publique).
4.  **La publication des données essentielles ne passe pas par le flux ACTES**. Les flux PES (XML) contenant les données essentielles doivent être constitués à la charge de la collectivité (NB: ADULLACT se désolé de l'abando des collectivités par l'État qui devrait mieux les accompagner dans cette démarche !) puis envoyés au Ministère des finances pour publication en Open Data.
5.  **Pour transmettre les données essentielles**, il y a 2 possibilités:
    *   Soit votre éditeur de Gestion Financière vous fournit le nécessaire pour lui fournir les données essentiels de vos marchés (soit automatiquement en lien avec votre profil acheteur, soit via un formulaire à remplir manuellement).
    *   Soiti votre éditeur de Gestion Financière ne vous propose pas de solution, et dans ce cas il est possible d'envoyer les PJ en trésorerie via un flux "PES dépenses" lors de la transmission des mandats.

Charge ensuite au profil acheteur de récupérer ces données diffusées par ETALAB en Open Data, et de les republier sur sa propre plateforme d'une manière intelligible.  
Là encore, la DAJ nous éclaire grandement avec un article dédié à l'[ouverture des données des marches publics](https://www.economie.gouv.fr/daj/ouverture-des-donnees-commande-publique "opendata"), très explicatif (voir le schéma).

En conclusion, il convient au sein des collectivités de bien faire la différence entre ces différentes notions, pour éviter de sur-investir trop tôt dans des outils parfois très sophistiqués :

*   Dématérialisation des marchés publics = obligatoire pour les marchés publics de plus de 25 000 €.
*   Signature électronique des marchés publics = non obligatoire.
*   La publication des données essentielles de marchés publics en Open Data = obligatoire au 1er octobre 2018.

Notre conseil bonus: ouvrez le dialogue avec vos principales entreprises...