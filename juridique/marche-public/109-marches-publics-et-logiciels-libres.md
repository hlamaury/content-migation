+++
title = "Marchés publics et logiciels libres"
description = "Marchés publics et logiciels libres"
tags = [ "Marchés publics", "Juridique" ]
date = "2018-07-31"
+++

**Les collectivités territoriales et administrations publiques ont le droit de privilégier des outils sous licence libre au terme d'un appel d'offres de marchés publics. C'est même l'une des recommandations de la circulaire Ayrault de septembre 2012.** 

Mais comment passer un appel d'offres lorsque l'objet n'est pas le logiciel libre, déjà choisi par l'organisation, mais les services afférents à ce logiciel libre (hébergement, formation, maintenance, etc.), alors même qu'il est interdit de spécifier un outil lors de la passation d'un marché public ?

Le code des marchés publics est complexe. Il existe une jurisprudence -> Picardie contre Kosmos, dont voici quelques extraits :

_"il résulte en outre de l'instruction que la mention du logiciel Lilie , en raison du caractère de logiciel libre que celui-ci présente et qui le rend librement et gratuitement modifiable et adaptable aux besoins de la collectivité par toute entreprise spécialisée dans l'installation de logiciels supports d'espaces numériques de travail, ne peut être regardée ni comme ayant pour effet de favoriser la société X qui a participé à sa conception et en est copropriétaire ni comme ayant pour effet d'éliminer des entreprises telles que les sociétés requérantes \[...\]"_

Cette jurisprudence, par ailleurs publiée au recueil Lebon, ouvre les portes à tous les services publics qui souhaiteraient ouvrir un appel d'offres pour des services liés à un logiciel libre de leur choix. 

Pour vous aider dans la rédaction des clauses propres aux logiciels libres, l'Agence du Patrimoine Immatériel de l'État (APIE) a rédigé un guide destiné à offrir un dispositif contractuel aux acheteurs publics.

**Lire** : [Conseils à la rédaction de clauses de propriété intellectuelle pour les marchés de développement et maintenance de logiciels libres (PDF)](images/sampledata/FAQ/Juridique/CCAG-TIC_marches-publics-2014.pdf)

Autres ressources :

**Lire :** [Jurisprudence Conseil d’État, 30 septembre 2011, n° 350431, Région Picardie](http://www.marche-public.fr/Marches-publics/Textes/Jurisprudence/CE-350431-region-picardie-logiciels-libres.htm) via marche-public.fr 

**Lire** : [Un marché public peut-il exiger du logiciel libre ?](https://www.journaldunet.com/solutions/expert/50532/un-marche-public-peut-il-exiger-du-logiciel-libre.shtml) via Journal du Net, décembre 2011