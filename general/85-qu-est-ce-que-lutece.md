+++
title = "Qu'est-ce que Lutèce ?"
description = "Présentation de Lutèce"
tags = [ "Lutèce", "Mairie de Paris" ]
date = "2011-12-30"
+++

### Histoire

Lutèce voit le jour en 2002, développé par la Mairie de Paris. Sa création est partie d’un besoin de mettre à disposition des mairies d'arrondissement de Paris un outil de type CMS pour gérer leurs propres sites et confier la gestion à des webmestres qui n'ont pas forcément des compétences précises dans ce domaine.

Il devait permettre un fonctionnement en réseau pour que les informations publiées par les services de la capitale soient accessibles par les Parisiens. Dernier impératif, son architecture devait être bâtie sur des technologies libres.

### Description de l'outil

Lutèce est une plate-forme de co-publication sur Internet, donc un CMS (Content Management System, ou système de gestion de contenu) développé par la direction informatique de la Mairie de Paris qui permet de créer rapidement un site internet ou intranet dynamique basé sur du contenu HTML, XML... .

Lutèce fournit une interface d'administration simple qui peut être utilisée par des utilisateurs sans compétences techniques particulières. La palette de services offerts sur un site peut être facilement personnalisée, il suffit d'ajouter un ou des plugins pour accéder à de nouvelles fonctionnalités.

### Gratuit

Lutèce est gratuit au téléchargement et à l'utilisation, sans aucun frais de licence.

### Open source

**Site officiel**

[http://fr.lutece.paris.fr/fr/jsp/site/Portal.jsp](http://fr.lutece.paris.fr/fr/jsp/site/Portal.jsp)

**Site de démonstration**

[http://lutece.demonstrations.adullact.org/jsp/site/Portal.jsp](http://lutece.demonstrations.adullact.org/jsp/site/Portal.jsp)

**Forge ADULLACT**

[https://adullact.net/projects/lutece-core/](https://adullact.net/projects/lutece-core/)