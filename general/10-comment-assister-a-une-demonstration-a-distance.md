+++
title = "Comment assister à une démonstration à distance ?"
description = "Démonstration en OpenMeetings"
tags = [ "OpenMeetings", "Démo" ]
date = "2011-11-07"
+++

ADULLACT réalise des démonstrations et autres transferts de compétences à distance. Quels outils utiliser ? Quels pré-requis ?

Pour assister à une présentation à distance, il convient de pouvoir se connecter sur la machine de l'orateur.

Plusieurs possibilités s'offrent à vous :

### Teamviewer

Un des logiciels les plus répandus dans les collectivités pour permettre cela est [TeamViewer](http://www.teamviewer.com/fr/). Une fois installé le logiciel sur votre poste, l'opérateur ADULLACT vous communique un code et un mot de passe. Il suffit d’introduire ces données pour avoir la "vue" sur l''écran de l'opérateur.

![](/images/sampledata/FAQ/General/Demo-Teamviewer.png)

Celui-ci réalise sa présentation en ligne et commente ses actions par téléphone. Il est nécessaire d'ouvrir le port 80/TCP dans les deux sens. Pour télécharger gratuitement (version de test pour les utilisateurs Linux) TEAMVIEWER : [téléchargement](http://www.teamviewer.com/fr/download/linux.aspx).

Il est également possible de voir l'écran du présentateur sans rien télécharger, en allant sur [go.teamviewer.com](https://go.teamviewer.com/v10/fr/ "accès direct").

Il suffit alors de renseigner les informations de connexion fourni par votre interlocuteur ADULLACT (un code de 3 fois 3 chiffres séparés par un '-'  sauf le 1er caractère qui est toujours la lettre 'm').