+++
title = "Qu'est-ce qu'un logiciel libre ?"
description = "Définition d'un logiciel libre"
tags = [ "Logiciel libre" ]
date = "2014-03-24"
+++

**Définition** : « Un logiciel libre est un logiciel dont l'utilisation, l'étude, la modification et la duplication en vue de sa diffusion sont permises, techniquement et légalement. Ceci afin de garantir certaines libertés induites, dont le contrôle du programme par l'utilisateur et la possibilité de partage entre individus. Ces droits peuvent être simplement disponibles (cas du domaine public) ou bien établis par une licence, dite « libre », basée sur le droit d'auteur. Les « licences copyleft » garantissent le maintien de ces droits aux utilisateurs même pour les travaux dérivés »

La [Free Software Fondation](https://www.fsf.org/?set_language=fr) (organisme non lucratif fondé par Richard Stallman) définit un logiciel comme "libre" s’il confère à ses utilisateurs les libertés suivantes :

*   la liberté d’exécuter le programme, pour tous les usages ;
*   la liberté d’étudier le fonctionnement du programme et de l’adapter à ses besoins ;
*   la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
*   la liberté d’améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.

Voir les articles concernant les [licences libres](component/tags/tag/licence-libre).

**Plus d'informations** : [fr.wikipedia.org/wiki/Logiciel\_libre](http://fr.wikipedia.org/wiki/Logiciel_libre)
