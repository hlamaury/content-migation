+++
title = "Intervention à distance"
description = "Protocole pour intervenir à distance"
date = "2008-12-30"
+++

SSH la méthode de connexion sécurisée la plus sûre, la plus efficace et la plus simple à mettre en ouvre.

Pour ce qui concerne l'intervention et la maintenance à distance, les techniciens ADULLACT ont besoin d'un **canal SSH** pour se connecter aux serveurs distants . Nous rappelons que le port SSH est le port 22/tcp. Afin de visualiser les actions menées sur les serveurs nous avons aussi besoin que le port HTTP (80/tcp) soit ouvert.

L'équipe technique se tient à disposition pour fournir des éléments (tel que l'@ip gateway) afin d'affiner les paramètres de filtrage pour ces connexions sécurisées. Vous pouvez nous joindre par mail à [support@adullact.org](mailto:support@adullact.org) ou par téléphone au **04 67 65 05 88**.