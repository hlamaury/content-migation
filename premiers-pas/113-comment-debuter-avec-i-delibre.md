+++
title = "Premiers pas avec i-délibre"
description = "Premiers pas avec i-délibre"
tags = [ "i-delibRE", "Assistance", "premier pas" ]
date = "2020-03-26"
+++

Une fois que la collectivité ou l'organisme est adhérent à l'association, une personne de la collectivité ou de l'organisme doit être désignée pour gérer le service. Une fois le [formulaire de demande d'ouverture d'accès à i-délibRE](https://demarches.adullact.org/commencer/inscription-au-service-i-delibre-de-l-adullact) renseigné, nous lui créons un compte administrateur. Ce compte dispose de toutes les habilitations nécessaires pour faites toutes les autres actions pour la collectivité.

  
Quand tout est prêt, vous recevrez un mail produit par la procédure d'oublie de mot de passe. Ce message contient un lien pour pouvoir fixer le mot de passe. Le lien n'est valable que 24h.

Vous pouvez également tester I-DELIBRE avec notre [démonstration i-delibre.](logiciels/115-demonstration-i-delibre "démonstration i-delibre")

Et pour démarrer concrètement et en production avec I-DELIBRE, voir les [pré-requis techniques](pre-requis/73-pre-requis-a-l-utilisation-d-i-delibre-tablette "pré-requis").

Enfin, nous vous conseillons vivement (même si c'est optionnel) de consulter les prestataires déclarés pour bénéficier d'un accompagnement professionnel. Voir la [liste des prestataires](https://comptoir-du-libre.org/fr/softwares/servicesProviders/15 "liste des prestataires").