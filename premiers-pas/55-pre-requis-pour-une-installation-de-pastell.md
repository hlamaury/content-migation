+++
title = "Premiers pas pour une installation de PASTELL"
description = "Premiers pas pour une installation de PASTELL"
tags = [ "PASTELL", "premiers pas", "installation" ]
date = "2013-02-07"
+++

### Pré requis pour un déploiement de Pastell

Vous trouverez ci dessous la liste de tous les éléments permettant la mise en ouvre du logiciel Pastell.

### Liste des systèmes d'exploitation supportés

Seul les versions de systèmes d'exploitation présents ci dessous **sont supportés.**

Les versions **NON LTS** d'ubuntu ne sont **pas supportées**.
 
|           | Status | Commentaires |
|-----------|--------|--------------|
| Ubuntu 10.04 LTS x64 | Obsolète | Fin du support Avril 2015 |
| Ubuntu 12.04 LTS x64 | Supporté | Version **Pastell 1.3**| 
| Ubuntu 14.04 LTS x64 | Supporté | Os de référence fortement conseillé. Version **Pastell 1.4** |
| Debian 6 x64 | Obsolète | Fin de support Mai 2015 |
| Debian 7 | Non Supporté | PHP 5.4.4 |
| Centos /RHEL 5 x64 | Obsolète | Déprécié |
| Centos /RHEL 6 | Supporté 
| Centos /RHEL 7 | Supporté |  Version **Pastell 1.4** |
| SLES 11 SP2 | Non Qualifié |

### Liste des outils embarqués supportés

|           | CloudOoo | Liberhorodatage | Commentaires |
|-----------|----------|-----------------|--------------|
| Ubuntu 12.04 LTS x64 | Supporté | Supporté | Version **Pastell 1.3** |
| Ubuntu 14.04 LTS x64 | Supporté | Supporté | Os de référence fortement conseillé. Version **Pastell 1.4** |
| Debian 7 | Supporté | Non Supporté | PHP 5.4.4 |
| Centos /RHEL 6 | Supporté | Supporté | 
| Centos /RHEL 7 | Non Qualifié | Non Qualifié |
| SLES 11 SP2 | Supporté | Supporté |

### Dimensionnement et ressources

Le dimensionnement peut être effectué tout en une même partition.

Nous conseillons le formatage en **LVM** afin de pouvoir augmenter a chaud l'espace disque.

|           | Détail | Commentaires |
|-----------|--------|--------------|
| Espace disque système | ~20 Go | L'espace disque peut être réuni en une seule partition de 40 Go
| Espace disque données | ~30 Go | _Idem_ |
| CPU | 1 | Indicateurs de départ |
| RAM | 4 Go | Indicateur de départ |

### Communication réseau

Voici la liste des ports utilisés en entrée et sortie.

Certains applicatifs doivent etres visibles depuis internet.

|           | Entrée | Sortie | Commentaires |
|-----------|--------|--------|--------------|
| HTTP port 80 TCP | Oui, complet | crl.adullact.org | récupération des AC et CRL RGS |
| HTTPS port 443 TCP | Oui, complet | www.s2low.org | Envoi des flux vers le TDT |
| SMTP port 25 TCP | Non | Oui | Généralement paramétré vers le relais SMTP local |

**Schéma d'architecture**

![](/images/sampledata/FAQ/Prerequis/pastell-13.png)

### Briques techniques

Voici la liste des briques techniques.

Ces briques sont des pré requis, et seront déployés à l'installation, inutile de procéder à leur mise en place.

|           | Version | Commentaires |
|-----------|---------|--------------|
| PHP | 5.5.9 | 
| APACHE | 2.x | Modules rewrite, ssl |
| MYSQL | 5.5 |

Un certain nombre de modules PHP sont nécessaires au bon fonctionnement de Pastell.

*   curl,
*   mysql,
*   openssl,
*   simplexml,
*   imap,
*   apc
*   bcmath
*   ssh2
*   pdo
*   pdo\_mysql
*   zip
*   phar
*   soap.

**OpenSSL**

OpenSSL 1.0.0a est requis (ou version supérieure). Note: cette version est livrée dans la distribution Ubuntu 12.04 LTS.

si une autre distribution est choisie, il est nécessaire de compiler et installer la dernière version stable de la branche 1.0 d'OpenSSL (http://www.openssl.org) .

**Navigateurs compatibles**

PASTELL est développé dans le respect des standards du web et de l'accessibilité. Toutefois, PASTELL a été testé sur les navigateurs suivants :

*   Au niveau fonctionnel : Mozilla Firefox 35 +
*   Au niveau ergonomie : Mozilla Firefox 3, Internet Explorer 7, Chrome, Opera, Safari

### Poste client

Voici la liste des pré requis concernant les postes de travail utilisateurs.

L'utilisation de Pastell nécessite un navigateur compatible ainsi qu'une version de JAVA pour l'utilisation de l'applet de signature.

Nous recommandons l'utilisation des versions les plus récentes des navigateurs.

|           | Système d'exploitation | Navigateurs Natif | Navigateurs supportés | Utilitaires | Commentaires |
|-----------|------------------------|-------------------|-----------------------|-------------|--------------|
| XP | Non Supporté | Non Supporté (IE7 & IE8) | Firefox 36+, Chrome 41+ | Java 8 | Depuis le 8 avril 2014, Microsoft ne prend plus en charge Windows XP, cette plate-forme n'est par conséquent plus prise en charge officiellement. Nous recommandons vivement aux utilisateurs d'effectuer une mise à niveau vers une version plus récente de Windows qui est toujours prise en charge par Microsoft, afin de conserver un environnement stable et sécurisé. |
| Vista | Supporté | IE 9 | Firefox 36+, Chrome 41+ | Java 8 |
| 7, 8.x | Supporté | IE 10 & IE11 | Firefox 36+, Chrome 41+ | Java 8 |
| OSX 10.x | Supporté | Safari 8 | Firefox 36+, Chrome 41+ | Java 8 | La signature électronique sur poste de travail "Apple MacOS-X" n'est possible qu'avec des certificats logiciels à ce jour. Le support des certificats RGS\*\* sur Apple MacOS-X est en cours de finalisation mais pas encore opérationnel. |
| Linux (ubuntu, Debian) | Supporté | | Firefox 36+, Chrome 41+ | Java 8 | La signature électronique sur poste de travail "GNU/Linux" (Ubuntu, Debian,...) n'est possible qu'avec des certificats logiciels à ce jour. |