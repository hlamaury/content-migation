+++
title = "Premiers pas pour une installation de Web-RSA"
description = "Premiers pas pour une installation de Web-RSA"
tags = [ "Web-RSA", "premiers pas", "installation" ]
date = "2009-10-27"
+++

Vous trouverez ci-dessous la liste des Pré-requis pour une installation de Web-RSA.

**Pour le serveur :**

*   4Go de ram Recommandé
*   Un espace disque de 50Go évolutif (coté Serveur applicatif)
*   Un espace disque de 50Go évolutif pour la base de donnée Postgres
*   CPU dual core ou un quad core (2Ghz et +)

**Les pré-requis logiciels sont :**

*   Apache2.2.8, agrémenté des modules complémentaires mod\_rewrite, dav, dav\_fs, actions
*   PHP >= 5.2.4 avec les modules libcurl php-soap et php-curl
*   PostgresSQL 8.3
*   Sun JDK JAVA 6
*   Alfresco 3
*   Mysql 5.0.5
*   GEDOOo 0.3.4
*   OpenOffice.org 2.4
*   imagemagick 6.3.7

Lors de l'installation plusieurs points sont à vérifier. La ré-écriture (rewrite) d'url pour le framework CakePhp, les droits utilisateurs sur la base de données.

Pour plus d'information consultez la documentation : https://adullact.net/docman/index.php?group\_id=613&selected\_doc\_group\_id=1126&language\_id=1

Voici le lien pour les pré-requis d'une installation Gedooo : http://faq.adullact.org/index.php?2009/10/23/53-pre-requis-pour-une-installation-de-gedooo

**Pour les postes utilisateurs :**

*   Web-RSA nécessite l'utilisation exclusive d'un navigateur WEB.
*   L'utilisateur peut utiliser n'importe quel navigateur. L'ADULLACT préconise les navigateurs conformes W3C.

Pour plus d'information visiter la forge http://adullact.net/projects/webrsa/