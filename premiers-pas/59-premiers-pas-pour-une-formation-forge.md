+++
title = "Premiers pas pour une formation Forge"
description = "Premiers pas pour une formation Forge"
tags = [ "Forge", "Formation", "premiers pas" ]
date = "2005-12-20"
+++

Lors d'une formation FusionForge, certaines manipulations nécessitent des autorisations. Détails...

Liste de prérequis techniques:

*   accès au serveur **adullact.net**
*   avec le protocole HTTP (http://adullact.net:80)
*   avec le protocole HTTPS (http://adullact.net:443)
*   avec le protocole SSH, port 22

Ces accès peuvent nécessiter des réglages système (firewall).

Disposer d'une adresse email professionnelle fonctionnelle et accessible pendant la formation.

Il sera également nécessaire d'installer des logiciels sur les machines des stagiaires.

*   Si la machine utilisée est sous MS-Windows, on installera la suite logicielle PUTTY (obligatoire) et le logiciel TORTOISECVS ou TORTOISESVN (obligatoire).
*   Si la machine utilisée est sous LINUX, on installera les packages ssh et cvs ou subversion (obligatoire), ainsi que la JRE java de Sun (optionnel).
*   Pour les développeurs, l'IDE Eclipse est également le bienvenu, de façon optionnelle ([www.eclipse.org](http://www.eclipse.org/)).

 Toutes ces installations nécessitent des **droits d'administrateur** sur les machines des stagiaires.

Pour encore mieux se préparer, ne pas hésiter à consulter la documentation (également disponible sur la forge) :

*   En tant qu'[administrateur d'un projet](images/sampledata/FAQ/Prerequis/FORGE_Documentation_administrateurs.pdf)
*   En tant qu'[utilisateur d'un projet](images/sampledata/FAQ/Prerequis/FORGE_Documentation_utilisateurs.pdf)