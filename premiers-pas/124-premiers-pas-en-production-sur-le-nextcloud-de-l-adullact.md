+++
title = "Premiers pas en production sur le Nextcloud"
description = "Premiers pas en production sur le Nextcloud"
tags = [ "Nextcloud", "premier pas" ]
date = "2021-04-07"
+++

Le service [https://nextcloud.adullact.org](https://nextcloud.adullact.org "Service Nextcloud de l'ADULLACT") est une instance du logiciel libre Nextcloud dédiée aux collectivités et opérée par [l'ADULLACT](https://adullact.org "Site web de l'ADULLACT"). Les données sont hébergées en France.

Pré-requis
==========

Comme [tous les autres services](https://adullact.org/services-web "Présentations des services de l'ADULLACT"), le service Nextcloud Collabora de l'ADULLACT s'adresse aux **adhérents de l'association**.  
Pour demander la création d'un compte administrateur de votre collectivité adhérente pour son espace collaboratif, il convient de renseigner le [formulaire de demande d'accès au Nextcloud de l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-nextcloud-de-l-adullact) .

L'administrateur :

*   pourra créer d'autres comptes (utilisateurs non administrateurs) pour le compte de sa collectivité
*   pourra fixer les droits sur les espaces partagés à l'ensemble de la collectivité.
*   sera inscrit sur une liste de diffusion dédiée à Nextcloud. Cette liste est utilisée par l'ADULLACT pour informer des évolutions du service, informer nos utilisateurs en cas d'incident, échanger entres collectivités sur les usages autour du Nextcloud.

Fonctions et limitations
========================

Le service propose un espace collaboratif de 5Go pour l'ensemble de la collectivité, que l'administrateur pourra structurer librement. Chaque utilisateur dispose d'un espace de 100Mo qui lui est réservé et qu'il gère en toute autonomie.  
  
Plus d'informations sur les fonctionnalités : [https://adullact.org/service-en-ligne-nextcloud](https://adullact.org/service-en-ligne-nextcloud "Description Nextcloud de l'ADULLACT")  
  
Pour toutes questions sur les usages il est conseillé :

*   d'utiliser le manuel d'aide accessible directement dans le menu utilisateur en haut à gauche de l'écran Nextcloud appelé _" `Aide Nextcloud "`_
*   d'utiliser la liste de diffusion `nextcloud AROBASE listes.adullact.org`, sur laquelle vous pouvez échanger avec d'autres collectivités autour de Nextcloud .
*   de contractualiser un support avec un prestataire Nextcloud reconnu sur le [Comptoir du libre](https://comptoir-du-libre.org/fr/softwares/117 "Nextcloud sur le Comptoir du libre")