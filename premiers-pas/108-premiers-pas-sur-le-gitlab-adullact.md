+++
title = "Premiers pas sur le GitLab Adullact"
description = "Premiers pas sur le GitLab Adullact"
tags = [ "Logiciels libres", "Assistance", "premiers pas", "GitLab" ]
date = "2018-01-11"
+++

GitLab
------

GitLab est une forge logicielle, sous licence Libre. Il s'agit d'un gestionnaire de dépôts de code source sous Git, et offre entre autres fonctionnalités : le suivi d'évolutions (_issues_), les wikis, les demandes de fusion (_merge request_).

Se créer un compte sur le GitLab ADULLACT
-----------------------------------------

1.  Se [créer un compte sur Adullact.net](https://adullact.net/account/register.php).
2.  Confirmer la création du compte en cliquant sur le lien reçu par courriel et en saisissant identifiant et mot de passe.
3.  Se connecter à nouveau sur adullact.net. (La confirmation de création de compte donne l'impression d'être connecté, il n'en est rien :) Il est bel et bien nécessaire de bien se reconnecter après avoir confirmé son compte.)
4.  Se connecter sur [gitlab.adullact.net](http://gitlab.adullact.net/) avec les mêmes identifiants
5.  Confirmer son adresse électronique
6.  Bienvenue !

Caractéristiques du Gitlab ADULLACT
-----------------------------------

Le Gitlab ADULLACT est une instance de Gitlab CE. Il propose l'ensemble des fonctionnalités de l'outil. Les spécificités de configuration sont précisés ci-dessous :

*   Des projets privés peuvent être ouverts sur demande. Pour cela, il suffit d'adresser sa demande à [support AROBASE adullact.org](mailto:support AROBASE adullact.org).
*   La creation de ticket par mail, ainsi que la réponse à des tickets par mail n'est pas activée.
*   Les runners sont disponibles. Cependant face à la diversité des configurations de développement, il est conseillé d'utiliser son / ses propres runners.

Si vous avez des demandes spécifiques, n'hésitez pas à nous contacter sur [support AROBASE adullact.org](mailto:support AROBASE adullact.org). Nous serons très heureux de faire progresser notre Gitlab pour vos besoins.

Passer l'interface GitLab en français
-------------------------------------

La [traduction de GitLab](https://docs.gitlab.com/ee/development/i18n/) est en cours, plusieurs chaînes de caractères peuvent être encore en anglais.

Pour changer la langue de l'interface, il convient de :

1.  Se connecter au [GitLab Adullact](https://gitlab.adullact.net/) ;
2.  Se rendre sur son profil (icône en haut à droite) puis cliquer sur _Settings_ ;
3.  Sur la page affichée, dans la rubrique _Main settings_, choisir Français pour _Preferred Language_.

FusionForge (adullact.net) et Gitlab (gitlab.adullact.net)
----------------------------------------------------------

Le site [https://adullact.net/](https://adullact.net/) (motorisé par le [logiciel libre FusionForge](http://fusionforge.org/)) reste en place ; le GitLab arrivant en ajout de FusionForge.

*   Il n'est pas prévu de migration automatique depuis le FusionForge vers la GitLab.
*   Une migration manuelle est laissée à la discrétion de chacun des responsables de projets.
*   Les nouveaux projets Git sont invités à se placer sur le GitLab.
*   Nous proposons d'accompagner nos adhérents sur Git / GitLab / la création de communautés autour de leurs logiciels libres.

Donner accès à un contenu uniquement sur inscription
----------------------------------------------------

Voir [Donner accès aux fichiers binaires seulement sur inscription](logiciels/107-gitlab-donner-acces-seulement-sur-inscription).