+++
title = "Permiers pas pour une installation de S²LOW internalisée"
description = "Permiers pas pour une installation de S²LOW internalisée"
tags = [ "S²LOW", "premiers pas", "installation" ]
date = "2008-05-16"
+++

Vous trouverez ci-dessous la liste des Pré-requis pour une installation de S²LOW internalisée.

**Pour le serveur :**

*   1Go de Ram (2Go Recommandé)
*   Un espace disque de 40Go + Espace disque en fonction du nombre d'actes à traiter/stocker
*   CPU 2Ghz mono cœur pour moins de 20 utilisateurs,
*   Pour une utilisation supérieure à 20 utilisateurs augmenter sensiblement la RAM et le CPU

ADULLACT impose un serveur GNU/Linux Ubuntu 6.06 LTS pour l'internalisation de S²LOW. Pour l'installation, le serveur doit-être connecté à Internet afin de récupérer et installer les derniers paquets disponibles. Sur une base Ubuntu Server, les principales briques logicielles qui seront installées: PostgreSql Le JDK Sun JAVA Apache2, mod\_ssl Tomcat Postfix Snort Clamav Amavisd-new

Un certificat serveur SSL/Certinomis devra être commandé et disponible lors de l'installation de l'application. Commander un certificat SSl serveur.

**Pour les postes utilisateurs :**

L'application S²LOW nécessite l'utilisation exclusive d'un navigateur WEB. L'utilisateur peut utiliser n'importe quel navigateur. L'ADULLACT préconise les navigateurs conformes W3C. La JVM 1.6 (ou supérieur) doit être installée sur les postes clients afin de pouvoir télétransmettre des actes en traitement par lot. Voir aussi le billet sur le démarrage de S²LOW

Pour l'environnement système :

Pour l'utilisation, les ports 80/tcp (http) et 443/tcp (https) doivent être ouverts en sortie sur le firewall entre les postes clients et le serveur.

Pour l'installation, la configuration à distance et la maintenance, un accès SSH au serveur internalisé est fortement recommandée. Cela implique une ouverture du port 22/tcp en entrée sur le firewall. Il est recommandé de prendre contact avec l'équipe technique ADULLACT afin d'affiner les règles de filtrage.

Liste des flux réseaux autorisés :

*   22/tcp entrant (avec restriction) pour administration à distance
*   80/tcp entrant/sortant HTTP + Mise à jour système
*   443/tcp entrant/sortant
*   6030/tcp sortant Connexion vers le MIOCT (partie java)
*   25/tcp entant/sortant
*   53/udp DNS
*   123/udp NTP