+++
title = "Premiers pas pour une installation de ODFGEDOOo"
description = "Premiers pas pour une installation de ODFGEDOOo"
tags = [ "premiers pas", "installation", "serveur ODFGEDOOo" ]
date = "2009-10-23"
+++

Vous trouverez ci-dessous la liste des Pré-requis pour une installation d'un serveur ODFGEDOOo.

Pour le serveur, les pré-requis matériels sont :

*   2Go de Ram Recommandé
*   CPU 1Ghz mono coeur

ADULLACT recommande un serveur GNU/Linux. L'installation a été validée sur la plateforme Ubuntu 12.04 server LTS, ODFGEDOOo peut également être installé sous d'autres systèmes d'exploitation: Debian Etch, Fedora/CentOS/RedHat ES, Mandriva Server,... sous réserve que les pré-requis logiciels ci-dessous soient respectés, et sous réserve de validation par les équipes techniques de l'ADULLACT.

Pour l'installation, le serveur doit-être connecté à Internet afin de récupérer et installer les derniers paquets disponibles.

Les pré-requis logiciels sont : Sun JDK JAVA6

Pour plus d'information visiter la forge http://adullact.net/projects/gedooo/