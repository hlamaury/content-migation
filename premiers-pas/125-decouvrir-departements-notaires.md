+++
title = "Premier pas sur Départements & Notaires"
description = "Premier pas sur Départements & Notaires"
tags = [ "Logiciels libres", "premiers pas", "Départements & Notaires" ]
date = "2021-04-08"
author =  "mfaure"
banner = "/images/comptoir_departement_notaire.png"
+++

Départements & Notaires est un extranet permettant d'apporter une réponse en temps réel aux études notariales chargées d’une succession s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide sociale.

Communauté
----------

L'ADULLACT assure l'animation de la communauté des utilisateurs (conseils départemetaux). Les Groupes de Travail Collaboratif (GTC) offrent un lieu d'échange de bonnes pratiques et d'expériences. Ils sont aussi le lieu des prises de décision quant à la feuille de route de l'application. Les GTC sont organisés en moyenne tous les ans (hors période Covid). Les [comptes rendus des précédents GTC Départements & Notaires](https://adullact.org/departements-et-notaires) sont consultables en ligne.

La communauté Départements & Notaires échange grâce à la liste discussion "dep-notaire". L'inscription est réservée aux collectivités et se fait sur demande (merci de [nous contacter à cet égard](https://adullact.org/contact)).

La [fiche Départements & Notaires sur le Comptoir du Libre](https://comptoir-du-libre.org/fr/softwares/115) donne accès à la liste des collectivités déjà utilisatrices ainsi qu'aux prestataires déclarés sur l'application.

Informations techniques
-----------------------

*   L'application est disponible sous la [licence libre aGPL v3](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/LICENSE.txt).
*   Le [code source se trouve sur le Gitlab Adullact](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2).
*   Les pré-requis sont détaillés dans la [documentation d'installation](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/installation.md).