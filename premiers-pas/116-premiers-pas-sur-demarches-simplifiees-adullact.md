+++
title = "Premiers pas sur Démarches Simplifiées"
description = "Premiers pas sur Démarches Simplifiées"
tags = [ "Démarches Simplifiées (DS)", "DS", "premiers pas" ]
date = "2021-02-15"
+++

### Présentation

Le serveur **[demarches.adullact.org](https://demarches.adullact.org/ "Service Démarches Simplifiées de l'ADULLACT")** est l'instance du [logiciel libre **Démarches Simplifiées**](https://github.com/betagouv/demarches-simplifiees.fr "Code source du logiciel libre Démarches Simplifiées sur Github") (développé et maintenu par la [DINUM](https://www.numerique.gouv.fr/ "direction interministérielle du numérique (DINUM)")) dédiée aux collectivités et opérée par l'ADULLACT.

### Vocabulaire

*   **Démarche** : démarche administrative créée par une collectivité, et à destination de ses citoyens (exemple : inscription à la cantine, demande d'état-civil...)
*   **Dossier** : ensemble des informations saisies par un citoyen lors du remplissage d'une démarche.
*   **Instructeur** : agent de la collectivité chargé de traiter les dossiers d'une démarche.
*   **Administrateur** : agent de la collectivité ayant les habilitations pour créer de nouvelles démarches

### Pré-requis

Le service _Démarches Simplifiées_ de l'ADULLACT s'adresse aux [adhérents de l'association](https://adullact.org/association/adherer/formulaire-d-adhesion).

Un seul administrateur est créé **par collectivité**.  
Si plusieurs personnes doivent créer des démarches ou si vous souhaitez anticiper des changements d'organisation comme le départ d'un agent, nous vous invitons à utiliser un courriel non nominatif.

Pour demander la création du compte administrateur de votre collectivité adhérente, il convient de renseigner le [formulaire de demande d'accès au service _Démarches Simplifiées_ de l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-demarches-simplifiees-adullact).

### Questions

Nous serons très heureux de répondre à vos questions par mail sur `demarches` `AROBASE adullact.org`.

Ce service est mis à disposition des adhérents de l'ADULLACT. Pour des prestations complémentaires (formations, accompagnement...) nous vous invitons à contacter les [prestataires inscrits sur le _Comptoir du Libre_](https://comptoir-du-libre.org/fr/softwares/136).

### Limitations

Pour le moment, _Démarches Simplifiées_ de l'ADULLACT possède encore quelques différences par rapport à l'instance _Demarches-Simplifiees.fr_  :

*   FranceConnect n'est pas encore activé (des jetons multiples sont juridiquement nécessaires pour les collectivités, là où l'État n'en utilise qu'un seul).
*   Au sein de l'application, le _formulaire de demande d'aide_ (ou _contact_) n'est pas encore fonctionnel. Cependant, n'hésitez pas à nous joindre sur `demarches AROBASE adullact.org`
*   Pour les instructeurs, les récapitulatifs par mails (hebdomadaire et quotidien) ne sont pas envoyés. En revanche, les notifications par mail à chaque nouveau dossier et message d'un usager fonctionnent normalement.
*   Une démarche peut contenir des pièces jointes. Cependant lors du passage à la prochaine version s'appuyant sur une technologie de stockage différente (_ObjectStorage_), de telles démarches seront perdues ainsi que les contenus soumis par les citoyens. Les collectivités utilisatrices seront prévenues en amont de la mise à jour afin qu'elles puissent s'organiser. Les champs impactés sont les suivants :  
    *   _pièce justificative_,
    *   _logo de la démarche_,
    *   _import d'un texte de cadre juridique_,
    *   _notice explicative de la démarche_.

### Liste de discussions

Quand une collectivité demande un accès au service _Démarches-Simplifiées_ de l'ADULLACT, nous l'inscrivons sur une liste de diffusion dédiée à ce service. Il est possible d'y échanger entre pairs sur la création de démarches.

L'association ADULLACT utilise aussi cette liste pour informer les utilisateurs d'éventuels incidents et des évolutions fonctionnelles, ainsi que pour organiser des _Groupes de Travail Collaboratifs_ (GTC).

Si vous utilisez déjà ce service _Démarches-Simplifiées_ de l'ADULLACT, vous pouvez écrire à `demarches-simplifiees AROBASE listes.adullact.org` . Si ce n'est pas le cas contactez nous pour corriger cet oubli.

* * *

*   [Conditions Générales pour les Administrations](logiciels/123-cgu-pour-les-administrations-du-service-demarches-simplifiees-adullact "CGU pour les Administrations du service "Démarches Simplifiées" de l'ADULLACT")
*   [Conditions Générales d'Utilisation pour les Usagers](logiciels/121-cgu-pour-les-usagers-du-service-demarches-simplifiees-adullact "CGU pour les Usagers du service "Démarches Simplifiées" de l'ADULLACT")
*   [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi "Suivi d'audience et vie privée du service "Démarches Simplifiées" de l'ADULLACT")
*   [Mentions légales](logiciels/119-mentions-legales-demarches-simplifiees-adullact "Mentions légales du service "Démarches Simplifiées" de l'ADULLACT")