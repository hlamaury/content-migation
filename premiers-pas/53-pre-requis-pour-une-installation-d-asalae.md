+++
title = "Premiers pas pour une installation d\"as@lae"
description = "Premiers pas pour une installation d\"as@lae"
tags = [ "as@lae", "premiers pas", "installation" ]
date = "2013-02-07"
+++

Vous trouverez ci-dessous l'ensemble des pré-requis techniques nécessaires en préalable à l'installation d'une plateforme AS@LAE

**Introduction**

La plate-forme destinée à héberger l'application as@lae est composée de deux serveurs physiques ou virtuels. Ces deux instances distantes seront répliquées deux fois par jour.

La plate-forme peut être installée également sur un seul serveur, si une réplication instantanée au niveau de la machine virtuelle est mise en place.

La plate-forme type conseillée est basée sous hyperviseur de machine virtuelle KVM. La solution as@lae peut être mise en place également sur tout autre hyperviseur.

**Distribution Linux**

La distribution linux de référence est : Ubuntu 12.04 LTS x64.

**Format des Serveurs**

La plate-forme As@lae est composée de deux instances, ces instances peuvent être physiques ou virtuelles. Dans le cadre de l'utilisations d'un serveur virtuel, l'application As@lae peut être mise en place sur tout hyperviseur.

Les données générées sur le système de fichier ainsi qu'en base de donnés seront répliquées vers le second serveur distant de manière asynchrone. Le second serveur as@lae est disponible uniquement en dans le cadre d’un PRA (Plan de Reprise d'Activité)

**Préconisation Serveurs**

*   CPU = processeur 2/4 core, 1 ou 2 vCPU en cas de machine virtuelle
*   RAM = 4GO
*   Espace Disque: 50GO : pour le système d'exploitation, les sources de l'application et la base de données. pour les archives : Montage d'espace disque sécurisé (Raid 1 ou Raid5) et évolutif sur baie de stockage type SAN. (NFS, RDM, etc..)

**Architecture fonctionnelle**

La plate-forme as@lae comporte :

1 serveur applicatif comprenant :

Le serveur Web Apache

L'outil de signature électronique LiberSign

L'outil de validation de formats FormatValidator (CINES)

L'antivirus ClamaV

Le service de vérification de formats de fichiers relaxNG Jing

Le service de conversion de documents CloudOoo et openoffice/Libreoffice

Le service d'horodatage

Le service de génération et fusion de documents PDF (tomcat)

Le serveur de bases de données PostgreSQL

Les 4 derniers services pouvant être installés à distance (sur d'autres machines) et accessibles via WS.

**Prérequis logiciels**

Les versions de paquets utilisés, sont ceux disponibles dans les dépôts officiels de chaque distribution. Distribution préconisée: Ubuntu 12.04 LTS x64

Du coté serveur web : Apache2 cakephp (1.2.10) fourni avec les sources d'asalae PHP5 (5.3) php-soap php5-pgsql php-xsl php-curl curl rsync Oracle java JRE 1.6 clamav

Pour le service de convertion de formats de fichier CloudOoo 1.22 (nécessite python v2.6 minimum) et openoffice/Libreoffice 3.x.

Pour le service géneration et fusion de documents PDF Gedooo (tomcat).

Pour le service de validation de formats de fichier FormatValidator V2.3 développé par le CINES.

Pour le service de validation de formats de fichier RelaxNG Jing développé par le créateur du format de fichier RelaxNG.

Du coté base de données : PostgreSQL 9.x minimum (réplication native intégrée)

Pour le serveur d'horodatage Nous préconisons une application d'horodatage distante utilisant les technologies OpenSSL v1.0, accessible via Webservice.

Ergonomie:

Les navigateurs recommandés et compatibles avec l'application sont : Firefox 3.6 /18 et supérieurs IE 7 – 8 (si pb d'affichage, nous contacter)

**Système de réplication de données**

Respectant le cahier des charges, l’accent est mis sur l'intégrité des données. Une haute disponibilité n' est pas requise au niveau de la plate-forme As@lae. L' application nécessite deux serveurs qui doivent être respectivement éloignés géographiquement de plus de 3 km. Un mécanisme de réplication de données asynchrone est mis en place au sein du logiciel as@lae. Ce mécanisme effectue : Une copie différentielle des fichiers des répertoires de travail vers le second serveur via RSYNC. Une copie de la base de données vers le second serveur, via les outils de réplication PosgreSQL. Les réplications auront lieu deux fois par jour : 13h et 23h.

Dans le cadre d'une infrastructure hautement disponible existante comprenant des équipements de stockage et de virtualisation, une seule instance est nécessaire.

**Autorité d'horodatage homologuée**

Afin de garantir l'intégrité et la validité des données, certaines actions ont besoin d'être horodatées. Le projet libre LiberHorodatage fournit des jetons d'horodatage répondant à toutes les contraintes de la RFC 3161 et peut être utilisé dans le contexte d'AS@LAE. Un certificat x509 comportant l'extension “timestamping” devra être délivré pour insérer une signature serveur aux jetons d'horodatage. D'autres fournisseurs de jetons d'horodatages peuvent également être utilisés.