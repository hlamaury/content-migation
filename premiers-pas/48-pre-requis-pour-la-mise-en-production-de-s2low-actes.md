+++
title = "Premiers pas avec S²LOW (ACTES)"
description = "Premiers pas avec S²LOW (ACTES)"
tags = [ "S²LOW", "ACTES", "Assistance", "premiers pas" ]
date = "2007-03-01"
+++

Mode opératoire de démarrage et pré-requis demandés aux utilisateurs du module ACTES de la plate-forme nationale mutualisée S²LOW.

### **Pré-requis et informations à fournir avant de démarrer S²LOW**

La [convention type](https://www.collectivites-locales.gouv.fr/files/files/dgcl_v2/actes/convention-type_transmission_actes_v11.odt "convention type") requiert certaines informations précises (article 2):

*   Nom du dispositif: **S2LOW**.
*   Date de 1ere homologation **22 janvier 2007** (dernière ré-homologation: 3 septembre 2019).
*   Nom de l'opérateur: **ADULLACT** (Association des Développeurs et Utilisateurs de Logiciels Libres pour les Administrations et les Collectivités Territoriales).
*   Date du marché: les adhérents de l'association sont invités à renseigner ici la date de leur 1ere adhésion à l'association, renouvelée annuellement.

Avant de pouvoir télé-transmettre, une collectivité doit renseigner le [formulaire de demande d'inscription au service S2LOW](https://demarches.adullact.org/commencer/inscription-au-service-s2low-de-l-adullact). Tous les utilisateurs enregistrés sur S2LOW seront aptes à télétransmettre immédiatement après activation, et avec leur certificat RGS\*\*.

En option, vous pouvez commander une formation auprès d'une entreprise partenaire de l'association. Sur notre Comptoir du libre, vous pouvez trouvez [des prestataire à propos de S2LOW](https://www.comptoir-du-libre.org/softwares/servicesProviders/5).

### **Pour les postes utilisateurs**

S2LOW nécessite l'utilisation exclusive d'un navigateur WEB. L'ADULLACT préconise les navigateurs conformes W3C comme FIrefox par exemple.

![](/images/sampledata/FAQ/Prerequis/s2low_capture1.png) 

_Affichage de la page principale ACTES_

![](/images/sampledata/FAQ/Prerequis/s2low_capture2.png)

_Administration de l'entité collectivité_

### **Pour les certificats**

On notera que seuls les utilisateurs appelés à télétransmettre doivent impérativement disposer d'un certificat RGS\*\* ou RGS\*\*\* (cf paragraphe suivant). Un administrateur qui gère une collectivité (créer de nouveaux utilisateurs, etc... ) peut se contenter d'un certificat basique (classe 0) fourni gracieusement par l'association à ses membres.

Les certificats X.509 des agents appelés à télétransmettre et délivrés par une autorité de certification agréée(\*) sont obligatoire pour autoriser la connexion sur la plateforme S²LOW. Seuls les certificats de types RGS\*\* ou RGS\*\*\* (c'est à dire matériel USB, carte à puce...) sont acceptables pour télétransmettre via S²LOW.

Plus d'informations sur les certificats électroniques: [Certificat RGS\*\* : le cas des multi-mandats,](/faq/juridique/92-certificat-rgs-le-cas-des-multi-mandats) [Certificats électroniques et dispositifs de télétransmission](/faq/technique/16-certificats-electroniques-et-dispositifs-de-teletransmission)

### Plus d'informations...

Plus d'information sur la [page du ministère de l'intérieu](https://www.collectivites-locales.gouv.fr/actes-0 "convention")r à propos de la Dématérialisation de la transmission des actes.

(\*) Liste officielle des [organismes agréés](https://www.lsti-certification.fr/index.php/fr/certification/psce "lsti") pour délivrer les certificats RGS\*\* ou RGS\*\*\*:

Accès direct au service [S²LOW](http://s2low.org).