+++
title = "Premiers pas en production avec web-marché"
description = "Premiers pas en production avec web-marché"
tags = [ "web-marché", "Marchés publics", "premiers pas" ]
date = "2011-08-18"
+++

Cet article indique les pré-requis pour un démarrage sur la plateforme de dématérialisation des marchés publics de l'ADULLACT, nommée WEB-marché.

La collectivité adhérente souhaitant démarrer avec WEB-marché devra fournir certains renseignements afin de permettre à nos équipes de créer le compte. La plateforme de l'ADULLACT est accessible aux collectivités adhérentes :

*   pour les collectivités : [https://webmarche.adullact.org/agent](https://webmarche.adullact.org/?page=agent.AgentHome "Accès collectivités")
*   pour les entreprises : [webmarche.adullact.org](https://webmarche.adullact.org "Accès entreprise")

Pour demander la création d'un compte administrateur de votre collectivité adhérente pour son profil acheteur, il convient de renseigner le [formulaire de demande d'accès au Webmarché de l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-webmarche-de-l-adullact) .