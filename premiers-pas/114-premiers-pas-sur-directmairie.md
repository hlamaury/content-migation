+++
title = "Premiers pas sur DirectMairie"
description = "Premiers pas sur DirectMairie"
tags = [ "Assistance", "DirectMairie", "premier pas" ]
date = "2020-04-22"
+++

DirectMairie est un logiciel libre de remontée d'informations citoyennes.

### Ouverture de compte pour les adhérents Adullact

1.  En tant que gestionnaire de collectivité, merci de [vous créer un compte.](https://directmairie.adullact.org/registration)
2.  Puis de transmettre à `directmairie @ adullact.org` le courriel utilisé pour la création de ce compte.
3.  Nous transmettre également le nom exact, le logo (<256 pixels) et le n° SIRET de la collectivité. Ces informations apparaitront telles quelles sur l'application, aux yeux de tous.
4.  De là nous créons votre collectivité et revenons vers vous avec les informations complémentaires pour finaliser la configuration de votre collectivité.

### Configuration côté Agent

Par défaut, aucune configuration n'est requise. Toutefois, une fois l'administrateur créé (voir ci-dessus) il pourra re-configurer l'application pour sa collectivité. Il est possible de reconfigurer:

*   la liste des catégories affichés,
*   les courriels attachés à ces catégories (par défaut c'est le courriel de l'administrateur initial qui est utilisé),
*   créer d'autres administrateurs.

### Déroulé côté Utilisateur

La première fois, l'utilisateur commence par lancer un navigateur sur son périphérique (smartphone, tablette, ...) avec le chemin (url): [https://directmairie.adullact.org](https://directmairie.adullact.org "DirectMairie"). Selon le navigateur utilisé, il peut ajouter un raccourci sur son écran.

Il suffit ensuite de cliquer sur "remonter une information". Plusieurs opérations sont proposées:

1.  localiser le problème en cliquant sur "ma position" (sinon se situer manuellement sur la carte).
2.  Sélectionner la catégorie du problème, parmi la liste proposée (cette liste est configurable selon la collectivité).
3.  Décrire le problème, en quelques mots.
4.  Et enfin, éventuellement ajouter une photo prise avec l'appareil.

Puis envoyer ces informations à la collectivité correspondant à la géolocalisation (ci-dessus).

Si la commune géolocalisée n'a pas encore configuré DirectMairie pour son territoire, un message d'erreur apparait alors, dès l'étape n°2. L'utilisateur est ainsi prévenu qu'il ne peut pas utiliser DirectMairie sur ce territoire.

Sinon, la fiche ainsi créée est alors envoyée au service concerné de la mairie sélectionnée.