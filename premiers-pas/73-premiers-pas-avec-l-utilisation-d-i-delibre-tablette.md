+++
title = "Premiers pas pour l\"utilisation d\"i-delibRE (tablette)"
description = "Premiers pas pour l\"utilisation d\"i-delibRE (tablette)"
tags = [ "i-delibRE", "premiers pas", "tablette" ]
date = "2014-05-26"
+++

Quels sont les pré-requis techniques pour utiliser i-Delibre ?

### Côté tablette

Pré-requis techniques minimaux requis pour utiliser I-delibre sur une tablette:

| Type de tablette | Processeur requis | RAM minimale | Version OS |
|------------------|-------------------|--------------|------------|
| Tablette **ANDROID** | Dual-core 1.2GHz / Cortex A9 _**ou**_ supérieur | 1 Go | >= 4.4 |
| Tablette **IPAD** | A4 1GHz _**ou**_ supérieur | 256 Mo | >= iOS 9 |
| Tablette **WINDOWS** | Intel 1.20 GHz | 1 Go | >= Microsoft Windows 8.1 |
  
**Taille préconisée pour les tablettes >= 10"**  
  
Exemple de tablettes testées :

| Type de tablette | Modèles | 
|------------------|---------|
| Tablette **ANDROÏD** | Samsung Galaxy Tab 2 _**ou**_ Asus Transformer Book Trio TX201TA-CQ003H 11" |
| Tablette **IPAD** | iPad 2 _**ou**_ iPad 3 | 
| Tablette **WINDOWS** | Microsoft Surface Pro 10" _**ou**_ Microsoft Surface Pro 2 10" _**ou**_ Sony Vaio Tap 11" _**ou**_ HP 11-H060EF 11,6" |

  
Les technologies utilisées dans idelibre sont:
* Jquery 2.0.0 
* Bootstrap 2.2.2 
* Raphaël JS 2.1.0 
* HTML5 
* cache Manifest

  
L'application i-delibre st accessible selon votre tablette ici:
* ANDROID
  i-delibre est disponible sur plusieurs magasins android:
    * Google Play
      [![i-delibRE / Android](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Android_robot.svg/200px-Android_robot.svg.png "i-delibRE / Android")](https://play.google.com/store/apps/details?id=coop.adullact_projet.idelibre_cordova_client&hl=fr_FR)
*   FILEDIR.com
      ![i-delibRE / Android](http://img.filedir.com/i/getitblack.png "i-delibRE / Android")
*   IOS sur apple store.
*   WINDOWS sur microsoft store.

Un certificat compatible sera nécessaire côté serveur (voir chapitre ci-dessous) pour permettre la connexion de la tablette. La configuration de ce certificat est importante: l'application tablette affiche le champ "O" (Organisation name) du certificat. 

### Côté client WEB (navigateur)

Navigateurs requis pour utiliser I-delibre sur un PC en mode WEB:

| Navigateur | Compatibilité idelibre |
|------------|------------------------|
| Chromium 29+ | OK |
| Chrome 29+ | OK |
| Firefox 21+ | OK |
| IE 7 | KO |
| IE 8 | KO |
| IE 9 | KO |
| IE 10 | OK |
| Opéra Coast | OK |
| Safari 5.1.9+ | OK |

### Côté serveur

Pour le serveur, les pré-requis matériels sont :

*   2 Go de Ram
*   Un espace disque de 30Go
*   2 CPU >= 1Ghz

et pour la partie logicielle:

*   Ubuntu 14.04 LTS x64
*   Ouverture des ports 80 et 443
*   Apache 2
    *   mod_expires
    *   mod_rewrite
    *   mod_ssl

*   PHP 5.4
*   Postgresql 9.1
*   CakePHP v. 2.4.1
*   Ghostscript (>= 8.71)

De plus, un certificat HTTPS est impératif côté serveur afin de garantir la sécurité entre les tablettes et le serveur. Ce certificat est dédié à une collectivité ou un ensemble de collectivités exploitant ce serveur.