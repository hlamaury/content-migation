+++
title = "Comment libérer mon logiciel ?"
description = "Comment libérer mon logiciel ?"
tags = [ "Licence CeCILL", "Logiciel libre", "GNU" ]
date = "2005-11-30"
+++

Nous proposons d'énumérer les étapes à suivre pour libérer un logiciel. 

0) Préambule : qu'est ce qu'une licence logicielle ?
----------------------------------------------------

Un logiciel libre n'est pas forcément commercialisé, et ne fait donc pas forcément l'objet d'un contrat commercial liant un fournisseur et un utilisateur. Une licence logicielle est un contrat par défaut. Ce contrat explicite ce qu'il est possible de faire ou de ne pas faire avec le logiciel, ainsi que les engagements réciproques des 2 parties.

C'est l'auteur du logiciel qui décide de ces droits, et donc de la licence logicielle qui s'applique à son logiciel libre.

Dans le cas d'une collectivité, il conviendra certainement de faire voter une **délibération** autorisant que le logiciel, qu'il soit développé en interne ou sur commande auprès d'un prestataire, peut être déposé sous licence libre sur une forge publique.

1) Choisir sa licence :
-----------------------

Il existe en réalité de nombreuses licences qui ont été généralement créées pour satisfaire au mieux les préoccupations de tel ou tel secteur d'activité. Une "bonne licence", c'est d'abord une licence bien adaptée à l'usage que l'on veut en faire. Nous ne citerons que 2 licences: GPL & CeCILL. N'hésitez pas à prendre contact avec [ADULLACT](http://www.adullact.org/) pour en savoir plus ou découvrir d'autres licences logicielles mieux adaptées à vos besoins. Nous vous invitons également à consulter d'autres sites consacrés à ce sujet (exemple : l'[article de Framasoft](http://www.framasoft.net/article2185.html) consacré à ce sujet).

*   La licence [GNU/GPL](http://www.gnu.org/licenses/gpl.html) de la Free Software Foundation . Pensée et créée pour le logiciel, elle est très largement utilisée dans le monde (+90%) pour le logiciel libre.
*   La licence [CeCILLV2](http://www.cecill.info/). La license française du logiciel libre. La première licence qui définit les principes d'utilisation et de diffusion des logiciels libres rédigée d'origine en français et en conformité avec le droit français, reprenant les principes de la GNU/GPL. La version 2 de CeCILL est compatible avec la GPL

Pour en savoir plus sur comment déclarer un logiciel sous licence CeCILL, consultez également la [rubrique Juridique](index.php/juridique).

2) Renseigner le code :
-----------------------

La publication sous licence libre consiste concrètement à insérer en en-tête de chaque fichier les mentions de droits relatives à la licence choisie. La licence concerne chaque fichier source constituant le logiciel. Exemple: dans le cadre d'une publication sous licence GNU/GPL, l'en-tête des fichiers sources contiendra les lignes de commentaires suivants :

_Une ligne de commentaire definissant succintement le logiciel._

_Copyright (C) yyyy name of author This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA._

Il est également nécessaire de joindre un fichier texte nommé COPYING reprenant le texte intégral de la licence. Exemple: [www.gnu.org/licenses/gpl.txt](http://www.gnu.org/licenses/gpl.txt) ou [www.cecill.info/licences/Licence\_CeCILL\_V2-fr.html](http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)

3) Déposer son code sur une forge :
-----------------------------------

Une fois la licence choisie, vous pouvez déposer votre code sur une forge afin de le partager avec un maximum de personnes. Une forge est une plateforme WEB offrant des outils techniques permettant le développement collaboratif, et en particulier contient un gestionnaire de sources destiner à gérer et versionner vos fichiers(CVS). Le choix de la forge est important. Une forge neutre et indépendante peut également servir de "preuve de dépôt" du code source, pour prouver, par exemple, la paternité d'un code source, mais aussi la primauté du dépôt.

Exemple : Vous trouverez à l'adresse suivante comment déposer librement votre application sur la Forge de l'ADULLACT ([gitlab.adullact.net](https://gitlab.adullact.net "Forge ADULLACT")).

Pour en savoir plus
-------------------

D'autres articles sont à lire pour approfondir le sujet:

*   [Quelle licence choisir](juridique/36-vers-quelle-licence-se-tourner) ?
*   [À qui appartient la décision](juridique/37-qui-effectue-le-choix-de-la-licence) ?
*   [Le cas des licences multiples](juridique/35-pourquoi-soumettre-son-logiciel-a-de-multiples-licences).