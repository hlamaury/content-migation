+++
title = "Comment aborder une migration bureautique ?"
description = "Comment aborder une migration bureautique ?"
tags = [ "OpenOffice", "Migration", "Bureautique libre" ]
date = "2005-09-08"
+++

Quels conseils avant d'entreprendre une étude d’opportunité et de faisabilité de migration d’un parc de postes de travail, de la suite bureautique Microsoft vers une suite bureautique libre.

**Technique**

toute migration commence par un audit de l'état des lieux. on est parfois(toujours!) surpris par nos utilisateurs et par la manière qu'ils ont d'exploiter tel ou tel logiciel!

Par exemple, utilisez vous des macros avec votre traitement de texte actuel? si oui, il faudra considérer le travail de ré-écriture-migration de ces macros.

**Humain**

Selon le public (profils et nombres) concerné, il faudra prendre plus ou moins de précautions pour aboutir... les solutions sont diverses (de la gendarmerie nationale qui migre un peu brutalement! aux grandes entreprises qui le font en douceur...).

N'hésitez pas à vous référer au document IDA traitants plutot bien de la question "psychologique". L'audit devra egalement aborder ce sujet qui n'est pas à traiter à la legere.

**Expériences**

on citera en vrac les expériences suivantes:

*   Gendarmerie nationale : 70000 poste bureautiques en cours de migration de microsoft-office -> OpenOffice.org http://www.zdnet.fr/actualites/informatique/0,39040745,39203431,00.htm
*   Hôpital d'Avranches : une des première migration "bureautique complexe" à grande echelle (+400postes, contexte hétérogene, precurseur: 2003, ...). http://oootools.free.fr/memoire\_cnam/
*   Hôpital de Tourcoing : +600postes dans un contexte hétérogène. Marc Ledauphin (DSI) a lourdement appuyé la formation des utilisateurs. Pour la petit histoire: son equipe de formation fonctionne tellement bien qu'il arrive meme à revendre de la formation aux établissements environnants! http://www.aful.org/solutions/tourcoing.html/view
*   L'IUFM de Montpellier : dimension plus réduite, mais les problèmes humains n'en sont que plus sensibles: là encore, la priorité est donnée à l'explication et à la formation. migration en douceur, du type "bureau par bureau" et accompagnement quasi-individuel.

**Mutualiser**

Dans l'optique de partager les efforts (vous n'etes certainement pas le seul à vous poser exactement la meme question) et les solutions/reponses, il est bon, afin d'elargir le debat (meme sujet, memes questions...) de poster au sein de notre mailinglist dédiée: bureautique@listes.adullact.org deja de nombreux resp. de collectivités y sont abonnés...

Quelques exemples concrets de collectivités ayant migré, classées par année (il est intéressant de connaître la pérennité de la migration!), en précisant le nombre de postes de travail concernés :

Depuis 2002 :

*   Ville de Bischeim: ~120 postes
*   Ville de Pessac

Depuis 2005 :

*   Ville de Mions: ~200 postes
*   Ville d'Albi: ~550 postes
*   Ville de Bezons: ~280 postes

Depuis 2008 :

*   Ville d'Annecy: ~450 postes
*   Département des Pyrénées-Atlantique: ~1500 postes
*   Ville d'Echirolles: ~1100 postes

Depuis 2010 :

*   Communauté de communes d'Alès: ~600 postes
*   Communauté urbaine de Bordeaux: ~1700 postes

Depuis 2011 :

*   Département de Seine Saint-Denis: ~5500 poste