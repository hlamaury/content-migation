+++
title = "Comment rédiger de manière accessible sur Dotclear ?"
description = "Comment rédiger de manière accessible sur Dotclear ?"
tags = [ "Accessibilité", "Dotclear" ]
date = "2006-03-02"
+++

Ce petit guide a pour but d'énoncer quelques bonnes pratiques à suivre afin de maintenir le niveau d'accessibilité d'un blog sur DotClear (bien que certains conseils s'appliquent de manière plus générale).

### Guide de bonnes pratiques pour la rédaction de billets

#### Titres

**Les titres de billets ne doivent pas dépasser 80 caractères**

Si le titre est trop long, il perd de sa pertinence. Ainsi les utilisateurs de plage braille (malvoyants ou aveugles), qui ne présente pas plus de 80 caractères à la fois, auront du mal à lire le titre, voire ne le comprendront pas. De plus, un titre trop long perd de son importance dans le référencement des moteurs de recherche.

#### Liens

**Ne pas faire de liens "cliquez ici" ou "en savoir plus"**

Ce type de lien n'est pas explicite. Des utilisateur d'aide technique (plage braille, synthèse vocales...) peuvent demander une liste des liens de la page. Dans ce cas, une fenêtre présente tous les liens de la page. Le texte avant et après chaque lien n'est plus visible, on ne peut donc pas comprendre où mène le "cliquez ici".

Pour éviter ce type de liens, il convient généralement de reformuler légèrement sa phrase. Voici un exemple fictif :

*   mauvais : `Pour lire le rapport 2005, [cliquez ici](mon-rapport-2005.html)`
*   bon : `Vous pouvez lire le [rapport 2005](mon-rapport-2005.html)`

Par ailleurs, utiliser un intitulé de lien significatif apporte une amélioration sensible pour le référencement.

Deux intitulés identiques doivent mener vers une cible identique.

Voyons de suite un mauvais exemple:

*   `le [rapport](mon-rapport-2004.html) de 2004`
*   `le [rapport](mon-rapport-2005.html) de 2005`

Ici les deux liens ont le même intitulé ("rapport") alors que l'un pointe vers le rapport 2004 et l'autre vers le rapport 2005. Quand l'utilisateur d'aide technique demandera la liste des liens, il verra deux fois "rapport" et ne saura pas qu'ils ciblent deux destinations différentes.![](/images/images/p.png)

Reprenons le même exemple et rendons-le correct :

*   `le [rapport de 2004](mon-rapport-2004.html)`
*   `le [rapport de 2005](mon-rapport-2005.html)`

#### Structuration de textes

**Les titres**

Il est possible d'utiliser plusieurs niveaux de titres dans les billets (jusqu'à trois niveaux dans Dotclear). Ceci permet de mieux structurer les idées, rend la navigation plus facile, notamment aux personnes aveugles, qui peuvent "sauter" de titre en titre. Si les titres ne sont pas présents, elles doivent écouter tout le texte depuis le début pour savoir si un point peut les intéresser par la suite.

Dans Dotclear, les titres s'utilisent avec des points d'exclamation. Plus il y en a, plus le titre est important. On commence donc avec 3 points d'exclamations devant le titre de plus haute importance, puis deux, puis un. Exemple de syntaxe dotclear:

`!!! Titre de plus haute importance: 3 points d'exclamation devant` `!! Titre inférieur: 2 points d'exclamation devant` `! Titre encore inférieur: un seul point d'exclamation devant` Les acronymes

Les acronmyes permettent d'expliciter les acronymes et abréviations utilisées. Placer un acronyme sur le sigle OGM, permettra de voir "Organisme Génétiquement Modifié" au passage de la souris. Exemple de syntaxe Dotclear :

`? ? OGM|Organisme Génétiquement Modifié ? ?`

(il ne doit **pas** y avoir d'espace entre les points d'interrogation "?")

#### Images

Les images peuvent être utilisées de différentes manières et dans différents contexte. Prenons les cas un par un.

**Images de décoration**

Une image de décoration ne véhicule aucun sens. Elle ne doit pas contenir de texte, et ne pas porter de message. Son absence ne doit pas être génante à la compréhension du billet. Par défaut, une image ajoutée dans dotclear est considérée comme image de décoration (aucune alternative textuelle). Exemple de syntaxe Dotclear :

`( (ma-belle-image.png) )`

(il ne doit **pas** y avoir d'espace entre les parenthèses)

**Image avec du sens**

Les images véhiculant un sens doivent posséder une "alternative textuelle" (ou ALT). Le ALT est le texte qui est affiché pour les internautes désactivant les images. Pour une personne aveugle qui ne voit pas l'image, c'est le ALT qui sera lu par la synthèse vocale en lieu et place de l'image. Exemple de syntaxe Dotclear :

`( (ma-belle-image.png|mon texte alternatif) )`

(il ne doit **pas** y avoir d'espace entre les parenthèses)

Le texte alternatif doit respecter les contraintes suivantes:

*   décrire de manière simple et factuelle l'image
*   faire moins de 80 caractères

**Image-lien**

Une image-lien est une image qui sert aussi de lien (exemple classique du logo du site qui sert de retour à l’accueil). Le ALT d'une image-lien ne doit pas être la description de l'image mais **la fonction du lien** (où mène ce lien, à quoi sert ce lien).

Reprenons l'illustration du logo du site qui sert de retour à l’accueil :

*   mauvais exemple: `( (mon-logo.png|logo de mon-site) )`
*   bon exemple: `( (mon-logo.png|retour à l'accueil de mon-site) )`

(il ne doit **pas** y avoir d'espace entre les parenthèses)

#### Remerciements

Les bonnes pratiques ci-dessus ont été fournies par la société [Open-S](http://www.open-s.com)