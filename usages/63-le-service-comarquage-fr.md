+++
title = "Le service Comarquage.fr"
description = "Le service Comarquage.fr"
tags = [ "Comarquage" ]
date = "2010-07-20"
+++

La [Dila](http://www.dila.premier-ministre.gouv.fr/) (Direction de l'Information Légale et Administrative) permet aux collectivités d’intégrer dans leur site internet les 3 guides des droits et démarches publiés sur www.service-public.fr, à destination des particuliers, des associations et des professionnels.

Easter-eggs, spécialiste GNU/Linux depuis 1997, a développé une solution libre de co-marquage, [Comarquage.fr](http://www.comarquage.fr/), qui est à ce jour la seule solution libre permettant le co-marquage de ces 3 guides, quel que soit le type de CMS utilisé par la collectivité.

Dotée d’une interface d’édition, elle permet aux agents d’enrichir l’information administrative avec des compléments locaux et de mutualiser toutes les informations locales partagées avec les collectivités d’un même territoire.

Les intérêts du co-marquage sont multiples :

*   la navigation de l’internaute se fait au sein du site de la collectivité,
*   les informations fournies sont toujours à jour,
*   le contenu du site est enrichi sans intervention,
*   le référencement du site est amélioré.
*   les démarches sont localisées sur le territoire.

L’outil sert à la fois à l’internaute qui recherche une information locale sur une démarche précise et à la collectivité, qui peut l’utiliser comme une **source d’information légale** non opposable aux guichets d’accueil.

L’intégration des guides des droits et démarches est très simple, il s’agit de copier et coller quelques lignes de code dans le site internet de la collectivité.

Pour permettre de tester Comarquage.fr, un **essai gratuit** pendant 3 mois sans engagement est proposé aux collectivités.

Si la collectivité souhaite adopter la solution, un abonnement annuel lui sera demandé (d’un montant égal à racine carrée de 30 fois la population) en contrepartie de la fourniture du service et du support, quel que soit le nombre de composants choisis.

**Pour plus d’information :**

Contacter [Easter-eggs](http://www.easter-eggs.com/index.php) au 01 43 35 00 37 ou à l’adresse [info@comarquage.fr](mailto:info@comarquage.fr)

**Voir en ligne :**

[Comarquage.fr](http://www.comarquage.fr/) est un service clé en main de diffusion d’informations publiques pour les sites web des collectivités territoriales et des services de l’État.

Ou télécharger la [plaquette de présentation](images/sampledata/FAQ/Usages/2013-Comarquage-fr-plaquette.pdf) (pdf).