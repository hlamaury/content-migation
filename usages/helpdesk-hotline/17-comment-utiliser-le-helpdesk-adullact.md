+++
title = "Comment utiliser le HelpDesk ADULLACT ?"
description = "Comment utiliser le HelpDesk ADULLACT ?"
tags = [ "HelpDesk", "Maintenance" ]
date = "2007-01-12"
+++

La plateforme de HELPDESK d'ADULLACT est accessible à l'adresse [magasin.adullact.org](http://magasin.adullact.org/)

Le HELPDESK d'ADULLACT est accessible avec un login. Les login sont issus d'un contrat de maintenance avec un partenaire ADULLACT habilité à maintenir tel ou tel libriciel.

Une fois connecté sur la plateforme avec son login, un nouveau menu apparaît dans la partie gauche : helpdesk

Une fois ce menu sélectionné (clique), la fenêtre centrale fait apparaître l'interface de gestion des tickets.

L'utilisateur peut alors visualiser et administrer ses propres tickets, et peut créer de nouveaux tickets. Un utilisateur est habilité à créer des tickets pour un (ou plusieurs) produit pour lequel il est autorisé. Chaque utilisateur ne voit que ses propres tickets.