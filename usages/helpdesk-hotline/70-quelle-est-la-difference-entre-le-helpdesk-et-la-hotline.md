+++
title = "Quelle est la différence entre le HelpDesk et la Hotline ?"
description = "Hotline et HelpDesk"
tags = [ "hotline", "HelpDesk" ]
date = "2009-10-09"
+++

Aujourd'hui; ADULLACT-Projet propose deux moyens de communication afin de répondre le plus rapidement et le plus efficacement possible à vos attentes...

Depuis le 4 Septembre 2009, ADULLACT Projet a ouvert sa ligne HOTLINE (0811-03-03-93).  
  
Cette ligne est destinée aux clients ayant besoin d'une aide ou d'un renseignement sur l'un des libriciels suivants : S²LOW, Web-delib, I-Parapheur, LetterBox, Publicatur et Lutèce.  
Un technicien vous aidera à distance et répondra à vos questions.  
  
Nous proposons toujours, la plateforme de HelpDesk ([http://helpdesk.adullact.org](http://helpdesk.adullact.org)) pour les enregistrements de Bugs ou de fonctionnalités manquantes.

Vos identifiants vous seront fournis durant votre formation.