+++
title = "Comment enregistrer un nouveau projet sur la forge ADULLACT ?"
description = "Comment enregistrer un nouveau projet sur la forge ADULLACT ?"
tags = [ "Forge" ]
date = "2005-10-28"
+++

Étape après étape : comment créer un nouveau projet libre sur la forge ADULLACT (fusionforge).

**1ère étape: s'inscrire sur la forge**

N'hésitez pas à consulter notre billet "[Comment s'inscrire sur la forge ADULLACT](general/20-comment-s-inscrire-sur-la-forge-adullact)".

**2ème étape: trouver un nom de projet**

À vrai dire, le projet aura 2 noms : un nom long, visible par tous et sur lequel se fera la communication ; et un nom court (ou nom unix) uniquement composé de caractères en minuscules et de chiffres et tiret, sans espace, ni ponctuation. Ce nom devra faire entre 3 et 15 caractères de long. 

Pour des raisons évidentes de référencement, nous vous invitons à choisir un nom original ! Cela permettra de reconnaitre et surtout de permettre au plus grand nombre de trouver votre projet au milieu de la jungle google ! Exemple : pour un projet de générateur de site WEB, éviter un nom du style "Web Site Generator".

**3ème étape: enregistrer le nouveau projet**

Une fois connecté, à partir de votre page personnelle, suivre le lien enregistrer un projet, et remplir le formulaire.

À noter que le résumé du projet est là pour expliquer le but et les raisons du projet à l'équipe ADULLACT. Celle-ci se réserve le droit de ne sélectionner que les projets en rapport avec ses objectifs : offrir des logiciels métiers aux administrations, collectivités, ainsi qu'aux monde de la santé ou de l'éducation. Ce texte contiendra moins de 1200 caractères. 2 choses importantes à considérer avec le plus grand soin : 

*   Le choix de la licence libre: la forge ADULLACT propose un certain nombre de licences libres OSI : vous aurez à choisir celle qui concerne votre projet avec la plus grande attention. L'équipe ADULLACT reste à votre disposition pour vous aider dans ce choix.
*   La description publique du projet : ce texte est celui qui décrit votre projet aux publics de la forge ADULLACT. L'outil de recherche de la forge cherche dans le nom du projet et dans ce texte de description. Prenez le plus grand soin à décrire votre projet au mieux en moins de 255 caractères !

Vous noterez en fin de formulaire, un choix cornélien entre CVS et SVN (SUBVERSION). Il s'agit d'outils de gestion des sources. Si vous n'avez aucune préférence, nous vous invitons à conserver le choix par défaut.

**4ème étape: acceptation du projet**

Une fois le formulaire rempli et envoyé, l'équipe ADULLACT est informée de l'arrivée de ce nouveau projet et délibère sur la pertinence du projet. Si le projet est accepté, le projet est définitivement créer, et vous recevez un mail de confirmation. En cas de refus, vous êtes également informé du pourquoi du refus !

**Pour en savoir plus**

N'hésitez pas à consulter la [documentation en ligne](http://adullact.net/docman/index.php?group_id=134&selected_doc_group_id=153&language_id=7).