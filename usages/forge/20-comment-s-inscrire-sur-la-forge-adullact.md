+++
title = "Comment s\"inscrire sur la forge ADULLACT ?"
description = "Comment s\"inscrire sur la forge ADULLACT ?"
tags = [ "Forge" ]
date = "2005-10-28"
+++

Étape après étape : comment s'inscrire sur la forge ADULLACT (fusionforge).

Le but d'une forge logicielle est de favoriser le développement collaboratif : plusieurs intelligences au service d'un projet commun. La forge de l'ADULLACT utilise le logiciel libre FusionForge.

La première chose à faire consiste donc à s'inscrire sur la forge pour profiter au mieux de toutes ses possibilités, et à commencer par créer un nouveau projet, mais aussi de le faire vivre au quotidien (évolution du code source, diffusion d'information, réponses aux questions des utilisateurs...).

## Pour s'inscrire

Sur la page d'accueil de la [forge ADULLACT](https://adullact.net/), en haut à droite, se trouve un lien nouveau compte. Remplir le formulaire ne pose pas de problème particulier sinon de faire attention au mot de passe à fournir, qui, pour des raisons de sécurité, impose de contenir au moins 1 lettre, au moins 1 chiffre, et au moins 1 caractère de ponctuation. Les 3 critères sont requis.

Attention également au thème choisi : nous recommandons vivement de conserver le thème ADULLACT. Choisir un autre thème risque de masquer certaines fonctionnalités spécifiques à la forge ADULLACT.

Enfin, nous vous invitons à autoriser la réception des mails informatifs concernant la forge ADULLACT : ce canal de communication n'est utilisé que par l'équipe ADULLACT de maintenance de la forge et lui permet d'informer tous les utilisateurs des opérations de maintenance (arrêts, perturbations...). Il y a, en moyenne, 1 mail / an ! Une fois le formulaire rempli et envoyé, la forge envoie un mail de confirmation. En lisant ce mail, il suffit de suivre l'URL de confirmation fournie dans le mail pour entériner l'inscription sur la forge ADULLACT.

## Pour se connecter

Une fois inscrit, pour se connecter, il y a un lien en haut à droite de la page d'accueil de la forge ADULLACT. À noter la nécessité d'accepter les cookies sur le navigateur.

En cas de perte de mot de passe, il suffit de cliquer sur le lien prévu à cet effet : un mail est alors envoyé pour vous permettre de redéfinir un nouveau mot de passe.

## Et ensuite...

Une fois inscrit et connecté sur la forge ADULLACT, toutes ses fonctionnalités vous sont accessibles et en particulier la possibilité de créer un nouveau projet.

Il est également courant de devoir s'inscrire et participer à un projet déjà existant. Pour se faire, le plus simple est d'aller sur la page du projet auquel on souhaite participer, puis sur la page d'accueil (onglet "résumé"), cliquer sur le bouton "Demander à rejoindre le projet" (sous la liste des membres du projet, sur la droite de l'écran). Les administrateurs du projet sont automatiquement informés de votre demande et sont en mesure d'accepter (ou non) votre demande d'inscription à leur projet.

Une [documentation complète](http://adullact.net/docman/index.php?group_id=134&selected_doc_group_id=153&language_id=1) est disponible en ligne.