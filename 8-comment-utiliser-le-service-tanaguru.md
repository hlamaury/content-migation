+++
title = "Comment utiliser le service Tanaguru ?"
description = "Utilisation du servie Tanaguru"
tags = [ "Tanaguru", "Accessibilité" ]

date = "2012-03-09"
+++

Comment évaluer et mesurer le **niveau d'accessibilité** de votre site web ?

Le service [TANAGURU](http://www.tanaguru.com/fr/) est un service utile à toute personne (morale ou physique) souhaitant évaluer le niveau d'accessibilité de son (ou ses) site WEB. L'avantage de cet outil est qu'il est adapté aussi bien pour les spécialistes de l'accessibilité que pour les novices.

Dans le 1er cas, l'outil saura les aider techniquement et précisément à optimiser l'**accessibilité du site** ; dans le second cas, l'outil affiche une simple note permettant de se faire une idée générale du niveau de l'accessibilité du site.

L'outil complet est accessible via abonnement.

ADULLACT offre à ses adhérent un accès à l'outil pour un seul de leurs sites. En général, le site de référence de la collectivité, mais ça peut être n'importe quel site accessible de l'extérieur.

Pour profiter de ce service, il suffit aux **adhérents ADULLACT** de se rendre sur le service [TANAGURU pour ADULLACT](https://my.tanaguru.com/) et de cliquer sur s'inscrire. Après vérification et validation de votre inscription, vous recevrez un mail dans les 48H vous confirmant l'accès au service "TANAGURU pour ADULLACT". Vous pourrez rejouer le test autant de fois que vous le souhaitez.

#### Accéder au [service](https://my.tanaguru.com/)