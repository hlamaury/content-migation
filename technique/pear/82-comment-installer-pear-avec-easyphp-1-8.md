+++
title = "Comment installer Pear avec EasyPHP 1-8 ?"
description = "Voici la procédure à suivre pour installer PEAR lorsque l'on utilise le pack EasyPHP1-8"
tags = [ "Pear" ]
date = "2006-04-18"
+++
Voici la procédure à suivre pour installer PEAR lorsque l'on utilise le pack EasyPHP1-8 :

*   Ouvrir une fenêtre de commande DOS.

Exécuter : `cmd`

*   Aller dans le répertoire extensions :

`cd c:\Program Files\EasyPHP1-8\php\extensions`

*   Copier le fichier php\_gd2.dll dans le répertoire parent : 

`copy php_gd2.dll ..`

*   Retourner dans le répertoire parent : 

`cd ..`

*   Lancer le script d'installation :

`go-pear.bat`

*   Suivre les instructions d'installation....
*   Une fois pear installé, exécuter la commande permettant d'installer des packages PEAR non-stables :

`pear config-set preferred_state alpha`

*   Installer les packages souhaités : 

Exemple : `pear install Calendar`