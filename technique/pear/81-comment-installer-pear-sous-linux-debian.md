+++
title = "Comment installer Pear sous Linux/Debian ?"
description = "Comment installer PEAR sous une Debian sarge ?"
tags = [ "Linux", "Pear" ]
date = "2006-09-20"
+++
Comment installer PEAR sous une Debian sarge ?

Nous allons décrire deux méthodes d'installation de PEAR. La première méthode s'appuie sur les packages .deb de debian, nous utiliserons les commandes apt-get et dpkg. La deuxième méthode d'installation se fait via les sources de PEAR que nous récuperons depuis le site web.

#### Première méthode

Avant de se lancer dans l'installation de PEAR, nous devons connaître la version de php installé sur le serveur.

`# dpkg -l|grep php`

Cette commande donne une liste de paquets installés, un paquet php4 est affiché si nous sommes en version 4 de php.  
Pour installer PEAR nous devons exécuter la commande en root :

`# apt-get install php4-pear`

Le package PEAR est installé nous pouvons lister les paquets pear.

`# pear list`

Ou installer de nouveaux paquets :

`pear install |nom_du_paquet|`

Une aide est fournie :

`pear --help`

#### Deuxième méthode

Nous allons installer le package PEAR depuis les sources du site web, nous avons aussi besoin du paquet php-cli qui comprend la commande php.  
Nous listons tout comme dans la première méthode les paquets php afin de connaitre la version de php installé sur le sysytème.

`dpkg -l|grep php`

Pour un php en version 4 nous installons le paquet php4-cli :

`apt-get install php4-cli`

Nous pouvons maintenant récuperer les sources depuis le site web et lancer l'installtion de PEAR.

``` `` `lynx -source http://pear.php.net/go-pear | php -q` `` ```

Répondez aux questions et le paquet PEAR est installé.  
  
Vous pouvez lister les paquets :

``` `` `pear list` `` ```

Ou installer de nouveaux paquets :

``` `` `pear install |nom_du_paquet|` `` ```

Une aide est fournie :

`` `pear --help` ``