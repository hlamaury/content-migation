+++
title = "Add-in ODF pour Word"
description = "Guide d'installation du plugin Add-in ODF pour Microsoft Office 2003 à XP."
tags = [ "Word", "OpenOffice" ]
date = "2011-11-18"
+++
Guide d'installation du plugin "Add-in ODF" pour Microsoft Office 2003 à XP.

Add-in ODF pour Word est un petit logiciel permettant de transformer, d'ouvrir et d'enregistrer des fichiers provenant de la suite OpenOffice sur Word, Excel, Powerpoint, améliorant grandement la compatibilité entre OpenOffice (suite bureautique libre) et Microsoft Office (suite bureautique Microsoft). Il ne fait qu'ajouter un menu contextuel dans le menu "Fichier" des logiciels de la suite Office.

**Prérequis d'installation** : Office 2007, 2003 et XP Pour Office 2003 : Net Framework 2.0 ou superieur

Téléchargement du Plugin lien de téléchargement : [www.commentcamarche.net/download/telecharger-34065429-add-in-odf-pour-word](http://www.commentcamarche.net/download/telecharger-34065429-add-in-odf-pour-word)

**Installation du Plugin**

1\. Exécuter le programme télécharger

2\. Cliquer sur INSTALLER

3\. Suivre les indications

4\. Cliquer sur « Change »

![](/images/sampledata/FAQ/General/Addin-ODF-capture2.jpg)

5\. Choisir le plugin à installer (default « Add in ODF Word)

6\. Choisir le type de format ( .odt obligatoire )

![](/images/sampledata/FAQ/General/Addin-ODF-capture3.jpg)

7\. Cliquer sur Installer

8\. Add-in ODF est maintenant installé sur votre machine

![](/images/sampledata/FAQ/General/Addin-ODF-capture5.jpg)

9\. Vous avez maintenant la possibilité d'enregistrer vos .doc en .odt en cliquant sur « Fichier – Save as ODF »

10\. Vous pouvez également ouvrir des .odt « Fichier – Open ODF »