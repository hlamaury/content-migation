+++
title = "Comment ouvrir un fichier ODT dans mon application web ?"
description = "L'objectif principal de ce billet est d'utiliser le protocole WebDAV pour rendre possible la lecture et l'écriture de fichiers de type bureautique ou autre, à travers le web, sans avoir à télécharger le fichier sur son disque dur."
tags = [ "ODT", "OpenOffice" ]
date = "2010-01-20"
+++
L'objectif principal de ce billet est d'utiliser le protocole WebDAV pour rendre possible la lecture et l'écriture de fichiers de type bureautique ou autre, à travers le web, sans avoir à télécharger le fichier sur son disque dur.

Exemple : cela permet en cliquant simplement sur un fichier au format OpenDocument dans ma page web (ex: monfichier.odt), de directement lancer OOo (ou ms-Word2010), de modifier le fichier à la volée, puis de sauvegarder ce fichier en cliquant simplement sur le bouton "enregistrer". Vous trouverez ci-dessous les manipulations à effectuer afin d'ouvrir en mode lecture-écriture un fichier ODT sur un serveur distant.

### Préparation du serveur :

*   **Configuration Apache**

En supposant que le module APACHE2 est installé sur un serveur linux/debian, les pages web sont stockées dans le répertoire /var/www/monappli/ et accessibles sur ce serveur via une url du type http://monserveur/monappli/  
  
1) Activer le module Webdav  
  
en utilisant les commandes issues du package apache2 : 

`# a2enmod dav  
# a2enmod dav_fs`

2) Enrichir la Balise Directory de l'application:  
  
Dans la configuration appache dédiée à l'application web:

`<Directory /var/www/monappli/*>  
DAV On  
Options FollowSymlinks Indexes  
order allow,deny  
allow from all  
</Directory>`

3) Déposer le fichier ODT dans le répertoire de mon site web:  
  
Écrire un fichier `monfichier.odt` dans le répertoire `/var/www/monappli/`

*   **Adresse du fichier odt** :

Pour lancer OpenOffice.org (ou LibreOffice), le protocole utilisé est le suivant : `**vnd.sun.star.webdav://**`(\*)  
Pour lancer msword2010, le protocole utilisé est le suivant : `**http://**`  
  
_Exemple d'adresse web_ :  
`vnd.sun.star.webdav://monserveur.local/monappli/monfichier.odt`  
ou  
`http://monserveur.local/monappli/monfichier.odt`  
  
Concrètement, sur un exemple d'application comme WebDelib, on a une ligne à modifier dans le fichier de configuration `app/config/webdelib.inc` :  
Pour OpenOffice.org/LibreOffice :  
`Configure::write ('PROTOCOLE_DL', 'vnd.sun.star.webdav');`  
Pour ms-Word2010 :  
`Configure::write ('PROTOCOLE_DL', 'http');`

### Préparation du client :

*   **Sur un poste client Microsoft Windows** :
*   Pour ouvrir directement le fichier `odt` à partir de n'importe quel navigateur, il faudra inscrire le fichier de base de registre ci-dessous ("Clic Droit" > "Enregistrer le lien sous..." ) suivant dans votre système d'exploitation :
    *   Avec OpenOffice.org V3
    *   Windows XP : [OOo\_vnd.sun.star.webdav.reg](http://ftp.adullact.org/public/webdelib/OOo_vnd.sun.star.webdav.reg)
    *   Windows Vista / Seven / 8 : [wd-ooo-vista-seven.reg](http://ftp.adullact.org/public/webdelib/wd-ooo-vista-seven.reg)
    *   Avec OpenOffice.org V4
    *   Windows Vista / Seven / 8 : [wd-ooo4-vista-seven.reg](http://ftp.adullact.org/public/webdelib/wd-ooo4-vista-seven.reg)
    *   Avec LibreOffice V4
    *   Windows Vista / Seven / 8 : [wd-lo4-vista-seven.reg](http://ftp.adullact.org/public/webdelib/wd-lo4-vista-seven.reg)
    *   Avec LibreOffice V5
    *   Windows 7 /8 / 10 : [wd-lo5-7-8-10.reg](http://ftp.adullact.org/public/webdelib/wd-lo5-7-8-10.reg)

**Attention : de bien saisir le chemin vers votre OpenOffice (installé sur le poste client).**

*   **Sur un poste client Microsoft Windows, avec msWord** :
*   Pour ouvrir directement le fichier `odt` avec ms-Word(2010, 2013), à partir de Internet Explorer 7, 8 ou 9, sous MS-Windows Vista ou Seven, il faudra inscrire le fichier de base de registre ci-dessous ("Clic Droit" > "Enregistrer le lien sous..." ) suivant dans votre système d'exploitation :
    *   Windows XP : _Non opérationnel_
    *   Windows Vista / Seven (32 ou 64 bits) :
    *   msword2010 : [wd-ms-office2010.reg](http://ftp.adullact.org/public/webdelib/wd-ms-office2010.reg)
    *   msword2013 : [wd-ms-office2013.reg](http://ftp.adullact.org/public/webdelib/wd-ms-office2013.reg)

**Attention :** Ce protocole n'est valable que sous MS-Windows Vista ou Seven.  
**Attention :** Seul les navigateurs Internet Explorer version 7, 8 ou 9 supportent ce protocole.

**Attention: ces 2 configurations de registres (OpenOffice.org vs. MS-word) sont incompatibles ! Autrement dit, tous les utilisateurs d'un même Web-Delib devront tous être équipés de OpenOffice/LibreOffice _ou (exclusif!)_ MS-word.**  
  

*   **Sur linux** :

Avec firefox il vous suffira d'associer une action au protocole `vnd.sun.star.webdav`.

*   **Pour les versions de Firefox 3.5 et suivantes :**
    *   \* Taper about:config dans la barre d'adresse de votre navigateur,
    *   \* Faites un clic droit -> Nouvelle -> Booléenne -> Saisissez le nom : network.protocol-handler.expose.vnd.sun.star.webdav -> Value -> false
    *   \* La prochaine fois que vous cliquerez sur un lien qui appelle ce protocole, firefox vous demandera avec quelle application l'associer.

*   **Pour les versions de Firefox 3.0 :**
    *   Taper about:config dans la barre d'adresse de votre navigateur,
    *   Faites un clic droit -> Nouvelle -> Booléenne -> Saisissez le nom : network.protocol-handler.external.vnd.sun.star.webdav -> Value -> true
    *   Faites un clic droit -> Nouvelle -> Booléenne -> Saisissez le nom : network.protocol-handler.external.vnd.sun.star.webdav -> /usr/bin/soffice
    *   Assurrez vous que la valeur network.protocol-handler.expose-all soit réglée à true.

*   **Pour Opera v10.0**
    *   Vous pouvez ajouter une action pour le protocole _vnd.sun.star.webdav_ allant dans les préférences du navigateur.
    *   Cliquez sur Avancé > Programmes
    *   Ajouter le protocole : `vnd.sun.star.webdav` et associez lui l'action d'ouvrir OpenOffice (`/usr/bin/soffice`)

### Vérification

Une fois votre navigateur correctement configuré, vous pouvez tester à l'aide de l'exemple fourni pour OpenOffice.org/Libreoffice: monfichier.odt. Vous modifiez le fichier, le sauvegardez, sortez de OpenOffice.org, recliquez sur le fichier pour l'ouvrir à nouveau et vérifier que vos modifications ont bien été enregistrées.  
Note: comme pour toutes les démonstrations ADULLACT, le contenu de ce fichier est réinitialisé chaque nuit !  
\---  
(\*) le protocole utilisé dans notre démonstration (`vnd.sun.star.webdav`) est inspiré du protocole utilisé dans **Alfresco** pour réaliser cela. Il est possible de remplacer ce texte par n'importe quel texte dédié à votre application web. Exemple: pour WebDelib, nous utilisons le protocole `webdelib.webdav` en lieu et place du `vnd.sun.star.webdav` décrit ici. Pour cela, il suffit de remplacer la chaîne de caractère `vnd.sun.star.webdav` par `webdelib.webdav`, partout dans cet article !