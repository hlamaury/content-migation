+++
title = "Comment résoudre les problèmes d'installation d'applications basées sur CakePHP ?"
description = "Vous trouverez ci-dessous comment éviter les pièges souvent rencontrés..."
tags = [ "CakePHP" ]
date = "2008-02-21"
+++
L'installation des applications comme GTT, Web-Delib, WebSubvention peut parfois s'avérer difficile.

Vous trouverez ci-dessous comment éviter les pièges souvent rencontrés...

#### 1) La configuration de la base de données : 

La configuration de la base de données se fait dans le fichier suivant :

`{Répertoire_installation}/{nom_application}/app/config/database.php`

#### 2) Le répertoire tmp/ :

Penser à donner les droits d'écriture à ce répertoire sans quoi les caches de pages ne pourront pas se générer.  
Il y a deux façon de faire cela :

*   `chmod -R 777 {Répertoire_installation}/{nom_application}/tmp`

Ou bien, on donne les droits d'écriture uniquement à apache. Cela pourrait ressembler sur une debian à :

*   `chown -R www-data: {Répertoire_installa}/{nom_appli}/tmp` 

#### 3) La ré-écriture d'url :

Pour permettre le framework cakePhp de faire de la redirection d'url, il est indispensable que le module rewrite soit activé dans votre configuration apache.  
Décommentez (ou ajouter si elle n'existe pas) la ligne suivante :  
  
LoadModule rewrite\_module `{répertoire_modules_apache}/mod_rewrite.so`  
  
Il faut également vérifier la directive `AllowOverride` soit égale à `All` dans la balise directory de votre DocumentRoot.  
Sous debian, cela pourrait ressembler à quelque chose comme cela :  
`<Directory /var/www/>  
     Options Indexes FollowSymLinks MultiViews  
    AllowOverride All  
    Order allow,deny  
     allow from all  
</Directory>`

#### 4) short\_open\_tag :

`short_open_tag` est une directive définie dans le php.ini  
Définit si les balises courtes d'ouverture de PHP ( <? ?> ) sont autorisées ou non.  
Dans le cas de nos applications, nous vous conseillons de passer cette directive à ON.

#### 5) Configuration de l'envoi des mails :

Dans certains cas, les applications peuvent envoyer des mails.  
Les variables à configurer comme l'host SMTP, le login de connexion au serveur smtp, etc... se situent dans le fichier :  
`{Répertoire_installation}/{nom_application}/app/controllers/components/email.php`

#### 6) Maccro DEBUG :

Par défaut, les applications sont livrés en mode production.  
Cela signifie que, pour des raisons de sécurité, les erreurs seront systématiquement cachées.  
Si le problème persiste, vous pouvez les afficher en modifiant le fichier :  
`{Répertoire_installation}/{nom_application}/app/config/core.php  
Il suffira de remplacer la ligne :  
      _define('DEBUG', 0);_`  
par :  
      `define('DEBUG', 1);`

#### 7) Le fichier de préférence :

Pour certaines applications, un fichier de préférence peut exister.  
C'est dans ce fichier ou les configurations locales propre à l'installation seront enregistrées. il se trouve dans le répertoire : `{Répertoire_installation}/{nom_application}/app/config/`  
Lors de l'installation, il faudra renommer le fichier .inc.default en .inc  
  
**exemple** : webdelib.inc.default -> webdelib.inc  
ou : webrsa.inc.default -> webrsa.inc