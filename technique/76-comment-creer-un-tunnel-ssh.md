+++
title = "Comment créer un tunnel SSH ?"
description = "Comment accéder à une application Web (port 80 par défaut) via le port 22 ?"
tags = [  ]
date = "2010-06-16"
+++

Comment accéder à une application Web (port 80 par défaut) via le port 22 ?

Pour des raisons de sécurité, il arrive que les administrateurs réseaux n'ouvrent que le port 22 pour accéder à un serveur :

Tout d'abord, il faut ouvrir une connexion entre le serveur distant et notre machine. 

Nous allons ouvrir un tunnel entre notre port 8080 en local et le port 22 distant.

![](/images/sampledata/FAQ/Technique/Ecran_tunnel-SSH-1.png)  
  
Après vérification, nous constatons que nous avons bien un port en écoute sur le port 8080 avec l'application ssh :

![](/images/sampledata/FAQ/Technique/Ecran_tunnel-SSH-2.png)  
  
Il faut ensuite configurer notre navigateur web pour lui spécifier sur quel port il peut sortir :

![](/images/sampledata/FAQ/Technique/Ecran_tunnel-SSH-3.png)  
  
Renseigner dans votre fichier /etc/hosts l'adresse IP et le ServerName inscrits dans le virtualhost du serveur distant :  
  
![](/images/sampledata/FAQ/Technique/Ecran_tunnel-SSH-4.png)