+++
title = "Comment utiliser les APIs S²LOW en PHP"
description = "Définit les pré-requis nécessaires."
tags = [ "S²LOW" ]
date = "2010-01-21"
+++

Les pré-requis techniques pour l'envoi de document à S²LOW sont les suivants :

*   PHP > 5.2.x
*   le module php\_curl

  
La documentation des API disponibles pour S²LOW est accessible sur la forge adullact : [Documentations APIs](https://adullact.net/docman/?group_id=222)

<?php   
   
    // Url vers la plateforme s2low que vous voulez atteindre
    $host  = 'https://demo-s2low.extranet.adullact.org'; 
    // l'api que vous voulez utiliser
    $api = $url."/modules/actes/actes\_transac\_submit.php"
    // Enveloppe à envoyer à s2low 
    $file = "/var/www/s2low/adl-TACT--123456725--20080624-1.tar.gz"; 
   
    // la partie x509 du certificat : openssl pkcs12 -in certificat.p12 -out client.pem -clcerts -nokeys 
    define('PEM',      './key/client.pem');
    //  la clé privée du certificat :   openssl pkcs12 -in certificat.p12 -out key.pem -nocerts
    define('SSLKEY',   './key/key.pem');   
    //le certificat du CA :           openssl pkcs12 -in certificat.p12 -out ca.pem -cacerts -nokeys
    define('CA\_PATH',  './key/'); 
    define('PASSWORD', 'passwd'); 

    // En fonction de l'api que l'on appelle, on doit renseigner le tableau suivant : 
    $data = array('api'           => '1', 
                    'enveloppe' => "@$file" );

    $ch = curl\_init(); 

    // Paramétrage des options curl 
    curl\_setopt($ch, CURLOPT\_URL, $url);
    // En cas d'utilisation d'un proxy, on renseigne ici son adresse 
    // curl\_setopt($ch, CURLOPT\_PROXY, 'x.x.x.x:8080'); 
    curl\_setopt($ch, CURLOPT\_POST, TRUE);
    curl\_setopt($ch, CURLOPT\_POSTFIELDS, $data );
    curl\_setopt($ch, CURLOPT\_SSL\_VERIFYPEER, FALSE); 
    curl\_setopt($ch, CURLOPT\_CAPATH, CA\_PATH);
    curl\_setopt($ch, CURLOPT\_SSLCERT, PEM);
    curl\_setopt($ch, CURLOPT\_SSLCERTPASSWD, PASSWORD); 
    curl\_setopt($ch, CURLOPT\_SSLKEY,  SSLKEY); 
    curl\_setopt($ch, CURLOPT\_SSL\_VERIFYHOST, FALSE); 
    curl\_setopt($ch, CURLOPT\_VERBOSE, true); 
    curl\_setopt($ch, CURLOPT\_RETURNTRANSFER, TRUE);   
    $curl\_return = curl\_exec($ch); 

    if ($curl\_return === false) {
        echo 'KO\\nErreur dans le module curl.' . '<br /gt;';
        echo 'curl\_errno() = ' . curl\_errno($ch) . '<br /gt;';
        echo 'curl\_error() = ' . curl\_error($ch) . '<br /gt;';
    } else {
       echo 'Document envoyé<br >'; 
    }

    curl\_close($ch); 
  
?>