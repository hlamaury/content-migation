+++
title = "Comment utiliser les APIs S²LOW en Java ?"
description = "Les applications i-Parapheur et S²LOW proposent des API (interfaces de programmation) pour les applications tierces qui souhaitent se connecter et échanger des données."
tags = [ "S²LOW" ]
date = "2010-02-25"
+++

De plus en plus de collectivités souhaitent automatiser les échanges dématérialisés, et cela passe naturellement par la mise en place de dialogue inter-application.

Les applications i-Parapheur et S²LOW proposent des API (interfaces de programmation) pour les applications tierces qui souhaitent se connecter et échanger des données.

Par exemple : automatiser la télé-transmission d'un ACTE administratif via S²LOW, depuis son application de gestion RH favorite...

Les spécifications d'API de S²LOW sont publiées sur la Forge ADULLACT, et les connexions se font simplement grâce à l'utilisation d'un certificat électronique PKCS#12.

Voici un **exemple de code JAVA** (extrait des sources de l'application i-Parapheur, logiciel libre publié sous licence CeCILLv2), qui récupère le statut d'un ACTE précédemment télétransmis. Il montre la cinématique d'ouverture de connexion par certificat (package Apache EasySSLProtocolSocketFactory), d'utilisation de l'API, et d'exploitation du résultat.

 1 import java.io.IOException;  
 2 import java.security.KeyManagementException;  
 3 import java.security.KeyStoreException;  
 4 import java.security.NoSuchAlgorithmException;  
 5 import java.security.UnrecoverableKeyException;  
 6 import java.security.cert.CertificateException;  
 7 import org.apache.commons.httpclient.HttpClient;  
 8 import org.apache.commons.httpclient.HttpStatus;  
 9 import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;  
 10 import org.apache.commons.httpclient.methods.GetMethod;  
 11 import org.apache.commons.httpclient.protocol.Protocol;  
 12 import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;  
 13 import org.apache.commons.ssl.KeyMaterial;  
 14   
 15 class ConnecteurS2low {  
 16 private int getInfosS2lowActes(String TRANSACTION\_ID)   
 throws IOException, NoSuchAlgorithmException,   
 KeyStoreException, KeyManagementException,   
 CertificateException, UnrecoverableKeyException {  
 17 //Map propertiesActes = getPropertiesActes();  
 18 String passwordCertificat = "password";  
 19 String serverAddress = "demo-s2low.demonstrations.adullact.org";  
 20 String port = "443";  
 21 int codeRetour = -1;  
 22   
 23 EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();  
 24 KeyMaterial km = new KeyMaterial(readFile("/etc/certificats/monCertificat.p12"),  
 25 passwordCertificat.toCharArray());  
 26 easy.setKeyMaterial(km);  
 27 Protocol easyhttps = new Protocol("https",   
 (ProtocolSocketFactory) easy,  
 Integer.parseInt(port));  
 28 Protocol.registerProtocol("https", easyhttps);  
 29 HttpClient client = new HttpClient();  
 30 GetMethod get = new GetMethod("https://" + serverAddress + ":" +  
 31 port +  
 32 "/modules/actes/actes\_transac\_get\_status.php?transaction=" +  
 33 TRANSACTION\_ID);  
 34 try {  
 35 int status = client.executeMethod(get);  
 36 if (HttpStatus.SC\_OK == status) {  
 37 String reponse = get.getResponseBodyAsString();  
 38 String\[\] tab = reponse.split("\\n");  
 39 if (tab.length == 1 || "KO".equals(tab\[0\])) {  
 40 String error = "Erreur retournee par la plate-forme s2low: ";  
 41 for (int i = 1; i < tab.length; i++) {  
 42 error += tab\[i\];  
 43 }  
 44 throw new RuntimeException(error);  
 45 }  
 46 // La transaction s'est bien passee, on renvoie le statut (ligne 2)  
 47 codeRetour = Integer.parseInt(tab\[1\]);  
 48 } else {  
 49 throw new RuntimeException(  
 "Echec de la recuperation de la connexion: statut = "  
 + status);  
 50 }  
 51 } finally {  
 52 get.releaseConnection();  
 53 }  
 54 return codeRetour;  
 55 }  
 56 }