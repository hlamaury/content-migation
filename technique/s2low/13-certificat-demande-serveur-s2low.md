+++
title = "Comment faire une demande de certificat serveur pour S²LOW ?"
description = "S²LOW utilise un certificat serveur SSL X509 pour sécuriser les informations qui transitent sur le réseau. Nous allons détailler à travers ce billet comment générer un certificat SSL signé par une autorité de certification reconnue par le MINEFI."
tags = [ "S²LOW", "Certificat" ]
date = "2008-05-16"
+++

S²LOW utilise un certificat serveur SSL X509 pour sécuriser les informations qui transitent sur le réseau. Nous allons détailler à travers ce billet comment générer un certificat SSL signé par une autorité de certification reconnue par le MINEFI.

Informations nécessaires pour fabriquer un CSR
----------------------------------------------

*   Le pays ou se trouve le serveur (ex: Fr)
    
*   La région ou le département où se trouve le serveur (ex: LR ou Herault)
    
*   La ville où se trouve le serveur (ex: Montpellier)
    
*   L'organisation qui émet la demande (ex: entreprise, association, collectivité...)
    
*   Le service dont le serveur dépend (ex: Service informatique)
    
*   L'URL du site (Common Name, ex: mon\_nom.mon\_site.fr)
    
*   L'@mail du contact 
    

Générer le fichier CSR (Certificat Sign Request)
------------------------------------------------

*   **Pour Linux**

Vous devez avoir installé au préalable le paquet openssl.

`#openssl genrsa -des3 2048 > nom_mon_site.key`

Cette commande génère la clé privée de votre certificat, ne jamais publier cette clé. Si votre clé privée est perdue ou endommagée, vous ne pourrez plus utiliser votre certificat SSL. Une pass\_phrase vous est demandée, choisissez un mot de passe, il protège votre clé. Mémorisez le, il vous sera demandé lors de la configuration de S²LOW pour Apache2.

`#openssl req -new -key nom_mon_site.key > nom_mon_site.csr`

Cette commande génère une demande de certificat, elle est obtenue à partir de votre clé privée. Vous devez fournir les informations relatives à l'identité de votre site web. C'est le contenu de ce fichier que nous allons faire signer par [l'autorité de certification Certinomis](http://www.certinomis.fr/). Nous vous invitons à lire les informations livrées par Certinomis pour vous aider à remplir votre CSR.

Une fois le CSR créé, nous vous invitons à suivre le formulaire d'inscription de notre partenaire Certinomis. Le nom du certificat est de type SSL.