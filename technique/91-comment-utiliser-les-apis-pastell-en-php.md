+++
title = "Comment utiliser les APIs PASTELL en PHP ?"
description = "Vous trouverez ci-dessous un exemple d'utilisation des APIs PASTELL en PHP..."
tags = [ "PASTELL" ]
date = "2011-12-19"
+++

Vous trouverez ci-dessous un exemple d'utilisation des APIs PASTELL en PHP...

Les pré-requis techniques pour l'envoi de document à [PASTELL](https://adullact.net/projects/pastell/) sont les suivants :

*   PHP > 5.2.x
*   le module php\_curl

  
  
La documentation des API disponibles pour PASTELL est accessible [ici](https://pastell.test.adullact.org/web/api).

**Exemple de code PHP :**

ini\_set('display\_errors', 1);

/////////////////////////////////////
// Définition des variables
/////////////////////////////////////
define('login',    'col1');
define('password', 'col1');
define('host',     'http://pastell.test.adullact.org');     
$id\_e         = 3;
$circuit\_id   = 2; 

$delib\['Deliberation'\]\['nature\_id'\]   = 1;
$delib\['Deliberation'\]\['objet\_delib'\] = 'Deliberation de test'; 
$delib\['Seance'\]\['date'\]              = '11/07/1981';
$delib\['Deliberation'\]\['num\_delib'\]   = time();
$delib\['Deliberation'\]\['code'\]        =  '1.1.3'; 
$annexes\[\]                            = "/tmp/annexe\_01.pdf";
$annexes\[\]                            = "/tmp/annexe\_02.pdf";

/////////////////////////////////////
// Actions envoyées à PASTELL
/////////////////////////////////////
$id\_d   = createDocument($id\_e);
$result = modifyDocument($id\_e, $id\_d, $delib, $annexes); 
insertInParapheur($id\_e, $id\_d);
insertInCircuit($id\_e, $id\_d, $circuit\_id);
action($id\_e, $id\_d, 'send-iparapheur');

/////////////////////////////////////
// Définition des fonctions
/////////////////////////////////////

    function \_initCurl ($api, $data=array()) {
        $curl = curl\_init();
        curl\_setopt($curl, CURLOPT\_HTTPAUTH, CURLAUTH\_BASIC) ;
        curl\_setopt($curl, CURLOPT\_USERPWD, login.":".password);
        curl\_setopt($curl, CURLOPT\_RETURNTRANSFER, true);
        curl\_setopt($curl, CURLOPT\_URL, host."/web/api/$api");
        if (!empty($data)) {
            curl\_setopt($curl, CURLOPT\_POST, TRUE);
            curl\_setopt($curl, CURLOPT\_POSTFIELDS, $data );
        }
        return $curl;
    }

    function createDocument($id\_e, $type='actes') {
        $infos = array();
        $curl = \_initCurl("create-document.php?id\_e=$id\_e&type=$type");
        $result = curl\_exec($curl);
        curl\_close($curl);
        $infos = json\_decode($result);
        $infos = (array) $infos;
        return($infos\['id\_d'\]);
    }

    function modifyDocument($id\_e, $id\_d, $delib=array(), $annexes=array() ) {
        $file ="/tmp/delib.pdf";

        $acte = array('id\_e'                    => $id\_e,
                      'id\_d'                    => $id\_d,
                      'objet'                   => $delib\['Deliberation'\]\['objet\_delib'\],
                      'date\_de\_lacte'           => $delib\['Seance'\]\['date'\],
                      'numero\_de\_lacte'         => $delib\['Deliberation'\]\['num\_delib'\],
                      'type'                    => $delib\['Nomenclature'\]\['code'\],
                      'arrete'                  => "@$file",
                      'acte\_nature'             => $delib\['Deliberation'\]\['nature\_id'\],
                     );
        $curl = \_initCurl('modif-document.php', $acte);
        $result = curl\_exec($curl);
        curl\_close($curl);
        foreach ($annexes as $annex)
            sendAnnex($id\_e, $id\_d,  $annex);
    }

    function insertInParapheur($id\_e, $id\_d, $sous\_type = null) {
        $curl = \_initCurl("modif-document.php?id\_e=$id\_e&id\_d=$id\_d&envoi\_iparapheur=true");
        $result = curl\_exec($curl);
        curl\_close($curl);
    }

    function insertInCircuit($id\_e, $id\_d, $sous\_type) {
        $infos = array('id\_e'                    => $id\_e,
                      'id\_d'                    => $id\_d,
                      'iparapheur\_sous\_type'    => $sous\_type);
        $curl = \_initCurl("modif-document.php", $infos);
        $result = curl\_exec($curl);
        curl\_close($curl);
    }

    function action($id\_e, $id\_d, $action) {
        $acte = array('id\_e'                    => $id\_e,
                      'id\_d'                    => $id\_d,
                      'action'                  => $action
                    );
        $curl = \_initCurl('action.php', $acte);
        $result = curl\_exec($curl);
        curl\_close($curl);
        return json\_decode($result);
    }

    function sendAnnex($id\_e, $id\_d, $annex) {
        $acte = array('id\_e'                    => $id\_e,
                      'id\_d'                    => $id\_d,
                      'autre\_document\_attache'  => "@$annex"
                     );
        $curl = \_initCurl('modif-document.php', $acte);
        $result = curl\_exec($curl);
        curl\_close($curl);
    }