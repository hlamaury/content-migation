+++
title = "Jeu de caractères et encodage pour une application web"
description = "Jeu de caractères et encodage pour une application web"
tags = [ "Application web" ]
date = "2006-11-27"
+++

L'encodage d'une application WEB est un sujet relativement complexe et étendu, car il fait appel à différents paramétrages au niveau de la chaîne de production d'une application WEB. Cela fait appel aussi à différents intervenants, à savoir l'administrateur de la pate-forme d'hébergement, les développeurs de l'application WEB ainsi que l'utilisateur final.

Nous allons à travers ce document décrire les différents points où l'encodage peut être réglé.

*   Le serveur Apache (administrateur de la plate-forme)

Le serveur WEB qui héberge l'application peut forcer le nom du jeu de caratères qui sera ajouté à toute requete HTTP, on fixe ainsi un jeu de caratères par défaut via la directive **AddDefaultCharset**, cette directive est inscrite dans le fichier pricipal de configuration d'Apache. Elle surchage aussi un éventuel jeu de caratères qui aurait pu être spécifié dans le corps du code de l'application hébergé à l'aide d'une balise META.  
**C'est le degré de réglage d'encodage le plus fort**, si l'administarteur de la plate-forme décide de fixer un encodage par défaut (ex: UTF-8), tout le code hébergé par Apache va être délivré avec cette encodage.  
Cette méthode peut convenir pour un hébergement dedié, elle devient au contratire beucoup plus difficille à maintenir pour un hébergement mutualisé. Dans le cas d'un hébergement mutualisé nous définirons la variable à **off** dans le fichier de configuration d'Apache ( **AddDefaultCharset off**).

*   Le code source de l'application (les developpeurs de l'application)

Dans le cas ou la directive **AddDefaultCharset** est commentée dans le fichier de configuration d'Apache et un jeu de caractères est déclaré dans les META balises du code de l'application via **content='text/html; charset=nom\_charset'** , c'est ce même encodage écrit dans chaque META balise de chaque page qui sera alors l'encodage délivré dans la requête HTTP. C'est la méthode la plus utilisée et qui convient le mieux pour un hébergement mutualisé, le choix du jeu de caratères est anisi délégué aux développeurs de l'application, ce n'est plus imposé par l'administrateur de la plate-forme.

*   L'encodage du navigateur (l'utilisateur)

C'est le dernier maillon de la chaîne, il est utilisé seulement et seulement si l'encodage n'est déclaré ni dans Apache, ni dans le code source de l'application. C'est assez hasardeux de laisser l'utilisateur choisir l'encodage pour afficher le contenu de votre page WEB.

*   La base de données (administrateur de la plate-forme ou les developpeurs de l'application)

"La chaîne d'encodage" d'une application WEB continue avec la partie base de données, à la création d'une base ou à l'import de données on doit déterminier l' encodage qui est utilisé. Lors de l'ajout d'une base de données Mysql on peut définir depuis la version 4.1 de Mysql **l'interclassement** d'une base de données.  
C'est grâce à ce paramètre qu'on fixe un jeu de caratères par défaut ainsi qu'une **collation** associée à celui-ci. Il existe plusieurs collations pour un même jeu de caractères, la collation est utilisée pour les requêtes de recherche dans la base de données, cela correspond à un classement des caratères, l'encodage des caractères ne change pas, seul la hiérachie des caratères varie en fonction des différentes collations.  
Si on créer une base de donnée standard, on utilise l'encodage UTF-8 ainsi qu'une collation utf8\_unicode\_ci, la requête pour créer cette base est **CREATE DATABASE \`nom\_base\` DEFAULT CHARACTER SET utf8 COLLATE utf8\_unicode\_ci ;**.  
  
Si l'on souhaite initialiser une base de données avec un fichier de dump de base par exemple, il faut savoir en premier lieu sous quel encodage le dump de cette base est écrit, c'est primordial. Pour définir l'encodage du fichier \*.sql on peut aplliquer la commande **file** qui nous donnera des informations sur ledit fichier. Lors de l'initialisation de la base de données on choisira bien le type d'encodage retourné par la commande file.  
Mysql s'occupant de transformer les données dans le jeu de caractères correspondant à celui de la base de données.