+++
title = "Certificats électroniques et dispositifs de télétransmission"
description = "Tout ce que vous avez toujours voulu savoir sur les certificats électroniques et les tiers de télétransmission (TDT). ou de la nécessité d'acquérir un certificat compatible RGS"
tags = [ "S²LOW", "Certificat", "Certificat RGS" ]
date = "2007-04-12"
+++

Tout ce que vous avez toujours voulu savoir sur les certificats électroniques et les tiers de télétransmission (TDT). ou de la nécessité d\\"acquérir un certificat compatible RGS\*\*.

Tout ce que vous avez toujours voulu savoir sur les certificats électroniques et les tiers de télétransmission (TDT). ou de la nécessité d'acquérir un certificat compatible RGS\*\*.

L'association [ADULLAC](http://www.adullact.org/)T s'est penchée sur ce problème afin d'aider ses adhérents (et toutes les collectivités) à s'y retrouver.

L'ADULLACT a étudié plusieurs options pour permettre aux collectivités de diminuer le coût à investir afin de permettre à leurs agents de télétransmettre, vers ACTES, HELIOS, etc.

*   les agents télétransmetteurs peuvent-ils se connecter sur un TDT avec un certificat logiciel ?
*   la collectivité est elle autorisée à fabriquer elle-même les certificats pour ses agents (hors AED) ?
*   l'opérateur du dispositif de télétransmission est-il habilité à fabriquer lui même des certificats dédiés à son dispositif, pour les collectivités utilisant son dispositif ?
*   existe-t-il une solution gratuite pour obtenir des certificats certifiés ?

Pour répondre à ces questions, l'ADULLACT a rencontré les responsables du [projet ACTES](http://www.collectivites-locales.gouv.fr/actes-0?nocache=1216887254.81) au MIAT et à la DGCL. Après concertations avec d'autres acteurs officiels (MINEFI et projet Helios, DGME...) la réponse du MIAT a été très claire : non à toutes les questions ! Et rappelle que chaque agent télétransmetteur doit être équipé d'un **certificat de type RGS\*\***

Et de rappeler que TOUS les opérateurs offrant une plateforme habilitée à télétransmettre avec le ministère se sont engagés contractuellement sur un protocole d'authentification très précis. Ce protocole est abordé dans le cahier des charges du MIAT (annexe2, exigence 3.6, paragraphe 5.2) puis confirmé et précisé dans la convention type de raccordement que chaque opérateur signe avec le MIAT. Cette convention précise (paragraphe 5) :

_L'opérateur devra veiller à ce que l'authentification des collectivités repose sur l'utilisation de certificats conformes aux spécifications du paragraphe5.2 de l'annexe2 du cahier des charges d'homologation. Tout autre certificat ne doit pas être accepté. L'opérateur met à jour le paramétrage de son dispositif afin de s'en assurer._

Et dans cette annexe 2, paragraphe 5.2, on lit :

_5.2. Certificats acceptables pour l’authentification des collectivités auprès des dispositifs_

_Les certificats que pourra accepter un dispositif pour l’authentification des collectivités sont :_

1.  _Les certificats émis par une autorité de certificat référencée au niveau de sécurité fort (\*\*) de la PRIS v1 (Politique de Référencement Intersectorielle de Sécurité version 1), et ce durant toute la validité juridique de la PRIS v1.\[obsolète depuis 2010\]_
2.  _Les certificats délivrés par une autorité de certification agréée pour les téléservices du gouvernement. La liste de ces autorités de certifications est disponible à l’adresse :_ _[https://www.lsti-certification.fr/index.php/fr/certification/psce](https://www.lsti-certification.fr/index.php/fr/certification/psce)._
3.  _Les certificats émis par une autorité de certification référencée au niveau de sécurité fort (RGS\*\*) ou « 3 étoiles » (RGS\*\*\*) de la PRIS v2 (réglementation en vigueur) : - certificat d’authentification « agent » si la transmission est effectuée directement par un agent de la collectivité, - certificat serveur-client si la transmission est effectuée par un système (serveur d’une infrastructure)_

Le ministère affiche le cahier des charges relatif aux TDT.

#### En résumé :

Quelque soit la solution technique retenue (S²LOW ou autres...), un agent appelé à télétransmettre doit impérativement se connecter au dispositif avec un certificat matériel à identification forte et conforme au RGS en vigueur de la publication de l'arrêté du 18/05/2010 (on parle de certificat PRISv2, RGS 2 étoiles(\*\*) c'est à dire un certificat matériel délivré par une des entités certifiées par le ministère des finances et conformes à l'ETSI TS 102 042 : BNP, CERTINOMIS, CFF, CHAMBERSIGN, DHIMYOTIS,...).

On notera la possibilité de télétransmettre vers un dispositif de télétransmission via une autre application. Par exemple, le [parapheur électronique](http://adullact.net/projects/paraphelec/) ou bien la plateforme multi-flux [PASTELL](https://adullact.net/projects/pastell/) permet de se connecter via un simple login/motdepasse ou bien combiné avec un certificat, et le dossier est automatiquement envoyé au dispositif via un seul et unique certificat serveur certifié. Dans ce dernier cas, il est impératif d'utiliser des certificats serveur-client RGS \*. Ces certificats prennent différents noms commerciaux selon les autorités qui les commercialisent. Nous avons eu l'occasion de valider les fournisseurs suivants:

*   Certigna: Serveur Client RGS\*
*   Certinomis: Serveur Client RGS\*
*   Chambersign: Certiserv DT RGS serveur client

Cette mécanique est décrite dans le texte du ministère datant de l'été 2013 : [ACTES et le Référentiel Général de Sécurité](http://www.collectivites-locales.gouv.fr/actes-et-referentiel-general-securite-rgs-message-aux-emetteurs-sur-systeme-dinformations-actes-au-2).

Pour en savoir plus sur le service @CTES: [ACTES - Dématérialisation de la transmission des actes](https://www.collectivites-locales.gouv.fr/actes-0 "@actes")

Pour en savoir plus sur les [certificats certifiés RGS](https://clubpsco.fr/presentation-des-membres/ "lsti").

Voir aussi: [Certificat RGS\*\* : le cas des multi-mandats](/faq/juridique/92-certificat-rgs-le-cas-des-multi-mandats)